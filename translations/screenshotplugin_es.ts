<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>NPlugin::ScreenshotPlugin</name>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="72"/>
        <source>Screenshot Plugin</source>
        <translation>Plugin para obtener capturas de pantalla</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="77"/>
        <source>Displays screenshots of the packages to be installed</source>
        <translation>Muestra capturas de pantalla de los paquetes a instalar</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="82"/>
        <source>This plugin displays screenshots of the packages to be installed. It fetches the screenshots from http://screenshots.debian.net so a working internet connection is required.</source>
        <translation>Este plugin muestra capturas (si las hay) de los paquetes a instalar. Estás imágenes se obtienen de http://screenshots.debian.net. Por lo tanto es necesaria conexión a Internet para acceder a éstas.</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="106"/>
        <source>Loading screenshot
Establishing connection, please wait...</source>
        <translation>Cargar imagen captura de pantalla
Estableciendo la conexión, por favor espere...</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="157"/>
        <source>The screenshot size exceeds 10 MB. Something seems to be wrong here!
 Aborting Download.</source>
        <translation>El tamaño de la imagen es superior a 10 MB. Algo parece estar mal aquí! 
Cancelando la descarga.</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="164"/>
        <source>Loading screenshot - Progress: </source>
        <translation>Cargando imagen - Progreso: </translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="172"/>
        <source>No screenshot available for </source>
        <translation>Imagen no disponible</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="181"/>
        <source>An error occured while trying to download the screenshot:
</source>
        <translation>Ha ocurrido un error al intentar descargar la imagen:
</translation>
    </message>
</context>
<context>
    <name>NPlugin::ScreenshotPluginContainer</name>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugincontainer.cpp" line="86"/>
        <location filename="../src/plugins/screenshotplugin/screenshotplugincontainer.cpp" line="87"/>
        <source>Screenshots not supported</source>
        <translation>Capturador de pantalla no soportado</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugincontainer.h" line="51"/>
        <source>Screenshot Plugins</source>
        <translation>Plugin Capturador de pantalla</translation>
    </message>
</context>
</TS>
