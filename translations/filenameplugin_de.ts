<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>FilenameFeedbackWidget</name>
    <message>
        <source>Search for filename</source>
        <translation type="obsolete">Suche nach Dateinamen</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>Shows the filename that was searched for</source>
        <translation type="obsolete">Der Dateiname nach dem gesucht wurde</translation>
    </message>
</context>
<context>
    <name>FilenameSearchInput</name>
    <message>
        <source>Search packages with files containing</source>
        <translation type="obsolete">Duchsuche Pakete nach Datei</translation>
    </message>
    <message>
        <source>search installed packages only</source>
        <translation type="obsolete">nur in installierten Paketen suchen</translation>
    </message>
    <message>
        <source>Search packages containing a file whose filename matches the pattern</source>
        <translation type="obsolete">Suche nach Pakete mit einer Datei deren Name die angegebene Zeichenkette enthält</translation>
    </message>
    <message>
        <source>Check this if you want to search only the installed packages (usually much faster)</source>
        <translation type="obsolete">Nur in installierten Paketen suchen (normalerweise deutlich schneller)</translation>
    </message>
</context>
<context>
    <name>FilenameView</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="56"/>
        <source> not available</source>
        <translation>nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="57"/>
        <source>The &lt;tt&gt;</source>
        <translation>Das Kommando &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="57"/>
        <source>&lt;/tt&gt; command is not available.
Please make sure that the &lt;tt&gt;mime-support&lt;/tt&gt; package is installed and that you have permission to execute &lt;tt&gt;</source>
        <translation>&lt;/tt&gt; ist nicht verfügbar. Bitte stellen Sie sicher, dass das Paket  &lt;tt&gt;mime-support&lt;/tt&gt;  installiert ist und dass Sie die Rechte haben &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="71"/>
        <source>Unable to launch </source>
        <translation>Konnte folgendes Programm nicht ausführen: </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="72"/>
        <source>Launching &lt;tt&gt;</source>
        <translation>Die Ausführung von &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="72"/>
        <source>&lt;/tt&gt; failed due to an unknown reason.</source>
        <translation>&lt;/tt&gt; ist aus unbekanntem Grund fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="95"/>
        <source>Error viewing file</source>
        <translation>Fehler beim Betrachten einer Datei</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="95"/>
        <source>Unable to view file </source>
        <translation>Kann Datei nicht anzeigen: </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="96"/>
        <source>
Tried &lt;tt&gt;</source>
        <translation>Versucht mit &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="96"/>
        <source>&lt;/tt&gt; and
&lt;tt&gt;</source>
        <translation>&lt;/tt&gt; und &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="153"/>
        <source>Copy to clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="154"/>
        <source>Copy all filenames to clipboard</source>
        <translation>Alle Dateinamen in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="155"/>
        <source>View file (depends on settings in /etc/mailcap)</source>
        <translation>Datei ansehen (Abhängig von Einstellungen in /etc/mailcap)</translation>
    </message>
    <message>
        <source>Show the filelist for the selected package</source>
        <translation type="obsolete">Zeigt Dateiliste für ausgewähltes Paket an</translation>
    </message>
    <message>
        <source>Shows a list of the files which are included in the package. If the list is already shown it will be updated.&lt;br&gt;
For installed packages the list is shown by default because it is quite fast. For not installed package it is only shown if this button is clicked as it takes a considerable amount of time.</source>
        <translation type="obsolete">Zeigt die Liste der Dateien in dem aktuellen Paket an. Wenn die Liste bereits angezeigt ist, so wird diese aktualisiert.&lt;br&gt;
Für installierte Pakete wird die Dateiliste standardmäßig angezeigt da dies schnell ist. Für nicht-installierte Pakete wird diese nur angezeigt, wenn der Knopf angeklickt wird da diese eine zeitlang dauert.</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="obsolete">Anzeigen</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="59"/>
        <source>&lt;/tt&gt;.</source>
        <translation>&lt;/tt&gt; auszuführen.</translation>
    </message>
    <message>
        <source>Filter files to be shown</source>
        <translation type="obsolete">Filter für angezeigte Dateien</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="51"/>
        <source>Trying to view </source>
        <translation>Versuche Datei anzuzeigen: </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="103"/>
        <source>Retrying to view </source>
        <translation>Versuche Datei </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="103"/>
        <source> with mimetype text/plain</source>
        <translation> erneut mit dem MIME-Type text/plain anzuzeigen</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="114"/>
        <source>Finished viewing </source>
        <translation>Ansehen von Datei beendet</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="130"/>
        <source>Can&apos;t view file </source>
        <translation>Kann Datei nicht anzeigen </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="130"/>
        <source>, it is not viewable</source>
        <translation>, die Datei ist nicht anzeigbar</translation>
    </message>
</context>
<context>
    <name>NPlugin::FilenameActionPlugin</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameactionplugin.cpp" line="29"/>
        <source>Calls &quot;apt-file update&quot; updating the file database</source>
        <translation>Ruft &quot;apt-file update&quot; um die Dateidatenbank zu aktualisieren</translation>
    </message>
</context>
<context>
    <name>NPlugin::FilenamePlugin</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="66"/>
        <source>Filename Plugin</source>
        <translation>Plugin für Dateinamen</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="71"/>
        <source>Plugin to search by files and diplay files in packages.</source>
        <translation>Der Plugin erlaubt es nach Dateien zu suchen und Dateien in Paketen anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="76"/>
        <source>This plugin allows to search the packages for files, and to display the files included in the packages
For information about not installed packages, it needs the &lt;tt&gt;apt-file&lt;/tt&gt; tool.</source>
        <translation>Der Plugin erlaubt es nach Dateien zu suchen und Dateien in Paketen anzuzeigen.
Zum Anzeigen der Dateien in nicht installierten Pakete wird das Paket &lt;tt&gt;apt-file&lt;/tt&gt; benötigt.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="115"/>
        <source>Files</source>
        <translation>Dateien</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="278"/>
        <source>There is no file information for the current package available.</source>
        <translation>Es sind keine Informationen über die Dateien für das aktuelle Paket verfügbar.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="308"/>
        <source>delayed evaluation - waiting for further input</source>
        <translation>Verzögerte Auswertung - warte auf weitere Eingaben</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="326"/>
        <source>Apt file search not availabe</source>
        <translation>Dateisucht nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="327"/>
        <source>You need the &lt;tt&gt;apt-file&lt;/tt&gt; utility to search files in packages not installed.&lt;br&gt;To get apt-file fetch it via &lt;tt&gt;apt-get install apt-file&lt;/tt&gt; and run &lt;tt&gt;apt-file update&lt;/tt&gt; afterwards.</source>
        <translation>Sie benötigen das Paket &lt;tt&gt;apt-file&lt;/tt&gt; um in nicht installierten Paketen nach Dateien zu suchen oder die Dateien anzuzeigen.&lt;br&gt;
Installieren Sie apt-file mittels  &lt;tt&gt;apt-get install apt-file&lt;/tt&gt;. Führen Sie anschließend &lt;tt&gt;apt-file update&lt;/tt&gt; aus.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="340"/>
        <source>Performing search for filenames, this might take a while</source>
        <translation>Suche nach Dateinamen, dies kann einige Zeit in Anspruch nehmen</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="178"/>
        <source>Filenames</source>
        <translation>Dateinamen</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="237"/>
        <source>Querying database for file list</source>
        <translation>Abfragen der Dateidatenbank</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="417"/>
        <source>&lt;font color=#606060&gt;&lt;p&gt;File list for &lt;b&gt;</source>
        <translation>&lt;font color=#606060&gt;&lt;p&gt;Die Dateiliste für Paket &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="417"/>
        <source>&lt;/b&gt; not available.&lt;/p&gt;&lt;p&gt;For displaying the file list for not-installed packages, the &quot;apt-file&quot; package is required. Please install &quot;apt-file&quot; and retrieve the latest file database by running  &lt;tt&gt;apt-file update&lt;/tt&gt;.&lt;/p&gt;&lt;/font&gt;</source>
        <translation>&lt;/b&gt; ist nicht verfügbar.&lt;/p&gt;&lt;p&gt;Um die Dateien in nicht installierten Paketen anzuzeigen wird das Paket &lt;tt&gt;apt-file&lt;/tt&gt; benötigt. Bitte installieren Sie apt-file und führen Sie anschließend &lt;tt&gt;apt-file update&lt;/tt&gt; aus um die aktuellste Dateidatenbank herunterzuladen.&lt;/p&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="136"/>
        <source>&lt;font color=#606060&gt;For packages &lt;b&gt;not installed&lt;/b&gt;, the file list is not shown by default. This is because retreiving the file list will take some time.&lt;br&gt;Please click the &lt;b&gt;&amp;quot;Show&amp;quot;&lt;/b&gt; button to show the filelist for the selected package.&lt;/font&gt;</source>
        <translation>&lt;font color=#606060&gt;Für &lt;b&gt;nicht installierte &lt;/b&gt; Pakete wird die Dateiliste standardmäßig nicht angezeigt, da dies etwas Zeit in Anspruch nimmt.&lt;br&gt;Bitte klicken Sie den Knopf &lt;b&gt;&amp;quot;Anzeigen&amp;quot;&lt;/b&gt; um die Dateiliste für das ausgewählte Paket anzuzeigen.&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>NPlugin::FilenamePluginContainer</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="103"/>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="112"/>
        <source>Command not executed</source>
        <translation>Programm wurde nicht ausgeführt</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="103"/>
        <source>For an unknwon reason, the command could not be executed.</source>
        <translation>Aus unbekanntem Grund wurde das Programm nicht ausgeführt.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="124"/>
        <source>Update not successfully completed</source>
        <translation>Update wurde nicht erfolgreich beendet</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="125"/>
        <source>The apt-file update was not completed successfully.&lt;br&gt;The database might be broken, rerun &lt;tt&gt;apt-file update&lt;/tt&gt; to fix this.</source>
        <translation>Das Update der Dateidatenbank wurde nicht erfolgreich beendet.&lt;br&gt;Die Datenbank könnte beschädigt sein. Bitte führen Sie &lt;tt&gt;apt-file update&lt;/tt&gt;  erneut aus um dies zu beheben.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameactionplugin.cpp" line="28"/>
        <source>Update File Database</source>
        <translation>Dateidatenbank aktualiseren</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="83"/>
        <source>Filename Plugins</source>
        <translation>Plugins für Dateinamen</translation>
    </message>
</context>
</TS>
