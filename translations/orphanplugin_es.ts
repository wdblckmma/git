<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>NPlugin::OrphanPlugin</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="60"/>
        <source>Orphan Plugin</source>
        <translation>Plugin huérfanos</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="65"/>
        <source>Plugin to search for orphaned packages.</source>
        <translation>Plugin para búsqueda de paquetes huérfanos.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="70"/>
        <source>This plugin supports to search for packages which are orphaned.
It uses deborphan to implement the search</source>
        <translation>Este plugin soporta la búsqueda de paquetes que han quedado huérfanos.
Utiliza deborphan para implementar la búsqueda</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="153"/>
        <source>Searching orphans</source>
        <translation>Búsqueda de huérfanos</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="170"/>
        <source>Error running deborphan</source>
        <translation>Error al ejecutar deborphan</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="170"/>
        <source>An error occured running &lt;tt&gt;</source>
        <translation>Se produjo un error en ejecución &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="131"/>
        <source>Orphans</source>
        <translation>Huérfanos</translation>
    </message>
</context>
<context>
    <name>NPlugin::OrphanPluginContainer</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugincontainer.cpp" line="63"/>
        <source>deborphan not available</source>
        <translation>deborphan no disponible</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugincontainer.cpp" line="64"/>
        <source>The &lt;tt&gt;deborphan&lt;/tt&gt; application, needed by the orphan plugin, was not found. The orphan plugin was disabled. To use the orphan plugin install the deborphan package via&lt;br&gt;&lt;tt&gt;apt-get install deborphan&lt;/tt&gt;&lt;br&gt;and reenable the plugin using &lt;i&gt;Packagesearch -&gt; Control Plugins&lt;/i&gt; afterwards.</source>
        <translation>&lt;tt&gt;deborphan&lt;/tt&gt; la aplicación, que necesita el plugin de paquetes huérfanos, no se ha encontrado. El plugin huérfano se ha desactivado. Para usar el complemento de paquetes huérfanos, debe instalar el paquete deborphan a través de &lt;br&gt;&lt;tt&gt;apt-get install deborphan&lt;/tt&gt;&lt;br&gt;y luego volver a habilitar el plugin en &lt;i&gt;Packagesearch  -&gt; Control de Plugins&lt;/i&gt;.</translation>
    </message>
</context>
<context>
    <name>OrphanFeedbackWidget</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="16"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="28"/>
        <source>Displays the deborphan commandline</source>
        <translation>Muestra la línea de comandos deborphan</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="31"/>
        <source>This displays the command line which is used to search for orphans.</source>
        <translation>Esto muestra la línea de comandos que se utiliza para buscar los paquetes huérfanos.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="41"/>
        <source>Clears the orphan search</source>
        <translation>Borra la búsqueda de paquetes huérfanos</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="44"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
</context>
<context>
    <name>OrphanSearchInput</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="13"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="25"/>
        <source>Search for ophaned package</source>
        <translation>Búsquedas de paquetes huérfanos</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="28"/>
        <source>&lt;p&gt;Enable this option to search for orphaned packages. Orphaned packages are packages which no other package depends on. By default only the &lt;i&gt;libs&lt;/i&gt; and &lt;i&gt;oldlibs&lt;/i&gt; sections are searched.&lt;/p&gt;
&lt;p&gt;This search uses the &lt;tt&gt;deborphan&lt;/tt&gt; tool as backend so make sure it is installed.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Marque esta casilla para buscar paquetes huérfanos. Paquetes huérfanos son los paquetes que no dependen de ningún otro paquete. Por defecto, sólo se busca en las secciones &lt;i&gt;libs&lt;/i&gt; y &lt;i&gt;oldlibs&lt;/i&gt; &lt;/p&gt;
&lt;p&gt;La búsqueda utiliza  la herramienta &lt;tt&gt;deborphan&lt;/tt&gt; asegúrese de que está instalada.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="32"/>
        <source>Search orphaned</source>
        <translation>Buscar huérfanos</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="42"/>
        <source>Options</source>
        <translation>Opcciones</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="54"/>
        <source>Search in section lib and oldlib only</source>
        <translation>Busca sólo en las secciones lib y oldlib</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="57"/>
        <source>Searches only the lib and oldlib section for orphans.</source>
        <translation>Busca huérfanos sólo la sección lib y oldlib.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="60"/>
        <source>Search lib and oldlib</source>
        <translation>Buscar en lib y oldlib</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="70"/>
        <source>Search in section lib, oldlib and libdevel</source>
        <translation>Busca en la sección lib, oldlib y libdevel</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="73"/>
        <source>Search in the section libdevel in addition to libs and oldlibs.</source>
        <translation>Busca en la sección libdevel, además de libs y oldlibs.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="76"/>
        <source>Search lib, oldlib and libdevel</source>
        <translation>Buscar en lib, oldlib y libdevel</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="83"/>
        <source>Search all the packages, instead of only those in the libs section</source>
        <translation>Buscar todos los paquetes, en lugar de sólo en la sección libs</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="86"/>
        <source>Search all sections</source>
        <translation>Buscar en todas las secciones</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="93"/>
        <source>Searches packages that are possibly useless</source>
        <translation>Busca paquetes que ya no son útiles</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="96"/>
        <source>&lt;p&gt;Search for packages where deborphan guesses them not to be of much use to you by examining the package&apos;s name and/or description. It will pretend the package  is  in  the main/libs section, and report it as if it were a library.
&lt;/p&gt;
&lt;p&gt;This method is in no way perfect or even reliable, so  beware  when using this!
&lt;/p&gt;</source>
        <translation>&lt;p&gt;La búsqueda de paquetes que ya no son probablemente útiles (se basa en el nombre del paquete o en la descripción de éste).
&lt;/p&gt;
&lt;p&gt;Este método no es perfecto ni fiable, así que por favor tenga cuidado cuando lo use.
&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="102"/>
        <source>Guess useless</source>
        <translation>Buscar los supuestamente inútiles</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="109"/>
        <source>Searches packages with residual configuration</source>
        <translation>Busca los paquetes por las configuraciones restantes</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="117"/>
        <source>Search residual configuration</source>
        <translation>Buscar por la configuración restante</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="112"/>
        <source>&lt;p&gt;This option searches for uninstalled packages which still have  configuration files on the system.
&lt;/p&gt;
&lt;p&gt;It implies searching in all sections.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Esta opción busca paquetes desinstalados que aún tienen los archivos de configuración en el sistema.
&lt;/p&gt;
&lt;p&gt;Esto implica la búsqueda en todas las secciones.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugincontainer.cpp" line="79"/>
        <source>Orphan Plugins</source>
        <translation>Plugin huérfanos</translation>
    </message>
</context>
</TS>
