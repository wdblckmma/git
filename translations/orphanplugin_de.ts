<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>NPlugin::OrphanPlugin</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="60"/>
        <source>Orphan Plugin</source>
        <translation>Waisen Plugin</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="65"/>
        <source>Plugin to search for orphaned packages.</source>
        <translation>Plugin um verwaiste Pakete zu suchen.</translation>
    </message>
    <message>
        <source>This plugin supports to search for packages which are orphaned.
It uses deborphan to implement the search</source>
        <translation type="obsolete">Dieser Plugin ermöglicht die Suche nach verwaisten Paketen.
Zur Realisierung der Suche wird deborphan verwendet.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="70"/>
        <source>This plugin supports to search for packages which are orphaned.
It uses deborphan to implement the search.</source>
        <translation>Dieser Plugin ermöglicht die Suche nach verwaisten Paketen.
Zur Realisierung der Suche wird deborphan verwendet.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="153"/>
        <source>Searching orphans</source>
        <translation>Suche nach verwaisten Paketen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="170"/>
        <source>Error running deborphan</source>
        <translation>Fehler beim Ausführen von deborphan</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="170"/>
        <source>An error occured running &lt;tt&gt;</source>
        <translation>Ein Fehler trat auf, beim ausführen von &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugin.cpp" line="131"/>
        <source>Orphans</source>
        <translation>Waisen</translation>
    </message>
</context>
<context>
    <name>NPlugin::OrphanPluginContainer</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugincontainer.cpp" line="63"/>
        <source>deborphan not available</source>
        <translation>deborphan ist nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugincontainer.cpp" line="64"/>
        <source>The &lt;tt&gt;deborphan&lt;/tt&gt; application, needed by the orphan plugin, was not found. The orphan plugin was disabled. To use the orphan plugin install the deborphan package via&lt;br&gt;&lt;tt&gt;apt-get install deborphan&lt;/tt&gt;&lt;br&gt;and reenable the plugin using &lt;i&gt;Packagesearch -&gt; Control Plugins&lt;/i&gt; afterwards.</source>
        <translation>Die Anwendung &lt;tt&gt;deborphan&lt;/tt&gt; wird vom Waisen-Plugin benötigt aber wurde nicht gefunden. Der Waisen-Plugin wurde deaktiviert. Zum installieren von deborphan  führen Sie bitte&lt;br&gt;&lt;tt&gt;aptitude install deborphan&lt;/tt&gt;&lt;br&gt;aus und aktivieren Sie anschließend den Waisen-Plugin über &lt;i&gt;Packagesearch -&gt; Plugins verwalten&lt;/i&gt;.</translation>
    </message>
</context>
<context>
    <name>OrphanFeedbackWidget</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="16"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="28"/>
        <source>Displays the deborphan commandline</source>
        <translation>Die deborpham Kommandozeile</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="31"/>
        <source>This displays the command line which is used to search for orphans.</source>
        <translation>Dies zeigt die deborphan-Kommandozeile an, die für die Ermittlung der verwaisten Pakete verwendet wird.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="41"/>
        <source>Clears the orphan search</source>
        <translation>Löscht die Suche nach verwaisten Paketen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanfeedbackwidget.ui" line="44"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>OrphanSearchInput</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="13"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="25"/>
        <source>Search for ophaned package</source>
        <translation>Durchsucht nach verwaisten Paketen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="28"/>
        <source>&lt;p&gt;Enable this option to search for orphaned packages. Orphaned packages are packages which no other package depends on. By default only the &lt;i&gt;libs&lt;/i&gt; and &lt;i&gt;oldlibs&lt;/i&gt; sections are searched.&lt;/p&gt;
&lt;p&gt;This search uses the &lt;tt&gt;deborphan&lt;/tt&gt; tool as backend so make sure it is installed.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Aktivieren Sie dieses Kästchen um nach verwaisten Paketen zu suchen. Verwaiste Pakete sind Pakete, von denen kein anderes Paket mehr abhängt. Standardmäßig werden nur die Sektionen &lt;i&gt;libs&lt;/i&gt; und &lt;i&gt;oldlibs&lt;/i&gt; durchsucht.&lt;/p&gt;&lt;p&gt;Die Suche verwendet &lt;tt&gt;deborphan&lt;/tt&gt; als Backend, welches installiert sein sollte.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="32"/>
        <source>Search orphaned</source>
        <translation>Verweiste Pakete suchen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="42"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="54"/>
        <source>Search in section lib and oldlib only</source>
        <translation>Suche nur in Sektionen lib und oldlib</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="57"/>
        <source>Searches only the lib and oldlib section for orphans.</source>
        <translation>Sucht nur in den Sektionen lib und oldlib nach verwaisten Paketen.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="60"/>
        <source>Search lib and oldlib</source>
        <translation>Druchsuche lib und oldlib</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="70"/>
        <source>Search in section lib, oldlib and libdevel</source>
        <translation>Suche nur in Sektionen lib, oldlib und libdevel</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="73"/>
        <source>Search in the section libdevel in addition to libs and oldlibs.</source>
        <translation>Sucht nur in den Sektionen lib, oldlib und libdevel nach verwaisten Paketen.</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="76"/>
        <source>Search lib, oldlib and libdevel</source>
        <translation>Durchsuche lib, oldlib und libdevel</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="83"/>
        <source>Search all the packages, instead of only those in the libs section</source>
        <translation>Durchsucht alle Pakete anstatt nur in den libs Sektionen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="86"/>
        <source>Search all sections</source>
        <translation>Alle Sektionen durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="93"/>
        <source>Searches packages that are possibly useless</source>
        <translation>Sucht nach wahrscheinlich unbenötigten Paketen</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="96"/>
        <source>&lt;p&gt;Search for packages where deborphan guesses them not to be of much use to you by examining the package&apos;s name and/or description. It will pretend the package  is  in  the main/libs section, and report it as if it were a library.
&lt;/p&gt;
&lt;p&gt;This method is in no way perfect or even reliable, so  beware  when using this!
&lt;/p&gt;</source>
        <translation>Sucht nach Paketen für die vermutlich nicht mehr benötigt werden (basierend auf dem Paketnamen und/oder der Beschreibung). 
&lt;p&gt;Diese Methode ist nicht perfekt und nicht zuverlässig, daher seien Sie bitte vorsichtig bei deren Nutzung!&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="102"/>
        <source>Guess useless</source>
        <translation>Vermutlich unbenötigt</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="109"/>
        <source>Searches packages with residual configuration</source>
        <translation>Sucht Pakete mit verbleibender Konfiguration</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="117"/>
        <source>Search residual configuration</source>
        <translation>Suche Pakete mit verbleibender Konfiguration</translation>
    </message>
    <message>
        <location filename="../src/plugins/orphanplugin/orphansearchinput.ui" line="112"/>
        <source>&lt;p&gt;This option searches for uninstalled packages which still have  configuration files on the system.
&lt;/p&gt;
&lt;p&gt;It implies searching in all sections.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Mit dieser Option wird nach Paketen gesucht für die deinistalliert sind aber die noch Konfigurationsdateien auf der Festplatte haben.￼&lt;/p&gt;￼&lt;p&gt;Es wird in allen Sektionen gesucht.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/orphanplugin/orphanplugincontainer.cpp" line="79"/>
        <source>Orphan Plugins</source>
        <translation>Waisen-Plugin</translation>
    </message>
</context>
</TS>
