<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>ColumnControlDlg</name>
    <message>
        <location filename="../src/columncontroldlg.ui" line="13"/>
        <source>Control Columns</source>
        <translation>Ajuste de Columnas</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="74"/>
        <source>Shown</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="109"/>
        <source>&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="116"/>
        <source>&lt;&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="148"/>
        <source>Hidden</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="195"/>
        <source>&amp;OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="198"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="205"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="208"/>
        <source>Alt+C</source>
        <translation>Alt+A</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <location filename="../src/main.cpp" line="69"/>
        <source>Usage: packagesearch [options]
	-h, --help     prints this message
	-v, --version  prints the version information
If started without options the UI is launched
</source>
        <translation>Uso: packagesearch [opciones]
	-h, --help     imprime el mensage
	-v, --imprime la información de la versión
Si se inicia sin opciones, la interfaz de usuario se pondrá en marcha</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="112"/>
        <source>Unknown option </source>
        <translation>Opción desconocida </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="118"/>
        <source>Debian Package Search - Version </source>
        <translation>Búsqueda de paquetes Debian - Versión </translation>
    </message>
</context>
<context>
    <name>NPackageSearch::PackageDisplayWidget</name>
    <message>
        <location filename="../src/packagedisplaywidget.cpp" line="56"/>
        <source>Customize Columns</source>
        <translation>Personalizar columnas</translation>
    </message>
    <message>
        <location filename="../src/packagedisplaywidget.cpp" line="212"/>
        <source>Hide Column</source>
        <translation>Ocultar columna</translation>
    </message>
</context>
<context>
    <name>NPlugin::PackageNamePlugin</name>
    <message>
        <location filename="../src/packagenameplugin.cpp" line="23"/>
        <source>Package Name Plugin</source>
        <translation>Nombre del plugin del paquete</translation>
    </message>
    <message>
        <location filename="../src/packagenameplugin.cpp" line="24"/>
        <location filename="../src/packagenameplugin.cpp" line="25"/>
        <source>Displays the package names</source>
        <translation>Muestra los nombres de los paquetes</translation>
    </message>
    <message>
        <location filename="../src/packagenameplugin.h" line="63"/>
        <source>Name</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NPlugin::PluginManager</name>
    <message>
        <location filename="../src/pluginmanager.cpp" line="232"/>
        <source>No translation for plugin </source>
        <translation>No hay traducción para el plugin</translation>
    </message>
    <message>
        <location filename="../src/pluginmanager.cpp" line="233"/>
        <source> found for current locale </source>
        <translation> encontrada localización actual </translation>
    </message>
</context>
<context>
    <name>NPlugin::ScoreDisplayPlugin</name>
    <message>
        <location filename="../src/scoredisplayplugin.cpp" line="30"/>
        <source>Score Display Plugin</source>
        <translation>Plugin de Calificación</translation>
    </message>
    <message>
        <location filename="../src/scoredisplayplugin.cpp" line="31"/>
        <source>Displays the package scores</source>
        <translation>Muestra las calificaciones de los paquetes</translation>
    </message>
    <message>
        <location filename="../src/scoredisplayplugin.cpp" line="32"/>
        <source>Displays the search scores of the packages returned by the current search.&lt;br&gt;
The higher the scores, the better the packages matches to the search.</source>
        <translation>Indica el valor de la puntuación de los paquetes en la búsqueda actual.&lt;br&gt;
Cuanto mayor se la puntuación, mejor será el paquete buscado.</translation>
    </message>
    <message>
        <location filename="../src/scoredisplayplugin.h" line="77"/>
        <source>Score</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PackageSearch</name>
    <message>
        <location filename="../src/packagesearch.ui" line="13"/>
        <source>Debian Package Search</source>
        <translation>Búsqueda de paquetes Debian</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="53"/>
        <source>&lt;b&gt;Search&lt;/b&gt;</source>
        <translation>&lt;b&gt;Búsqueda&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="131"/>
        <source>Clear Search</source>
        <translation>Borrar búsqueda</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="166"/>
        <source>Dummy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="217"/>
        <source>&lt;b&gt;Result&lt;/b&gt;</source>
        <translation>&lt;b&gt;Resultado de la búsqueda&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="271"/>
        <source>&lt;font size=&quot;-1&quot;&gt;No search active&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;-1&quot;&gt;no hay búsqueda activa&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="302"/>
        <source>Choose a package</source>
        <translation>Escoja un paquete</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="305"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Nimbus Sans L; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Here you can choose to display information about a package, from all  packages available.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Nimbus Sans L; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Aquí puede elegir mostrar la información sobre un paquete en concreto, de todos los paquetes disponibles.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="334"/>
        <source>Back</source>
        <translation>Atrás</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="344"/>
        <source>Forward</source>
        <translation>Adelante</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="365"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="410"/>
        <source>&amp;Packagesearch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="426"/>
        <source>&amp;System</source>
        <translation>&amp;Sistema</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="439"/>
        <source>&amp;Help</source>
        <translation>A&amp;yuda</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="447"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="452"/>
        <source>Pa&amp;ckages</source>
        <translation>Pa&amp;quetes</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="464"/>
        <source>&amp;Contents...</source>
        <translation>&amp;Contenido...</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="467"/>
        <source>Contents</source>
        <translation>Contenido</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="475"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="478"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="486"/>
        <location filename="../src/packagesearch.ui" line="489"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="494"/>
        <location filename="../src/packagesearch.ui" line="497"/>
        <source>Control Plugins</source>
        <translation>Control de Plugins</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="500"/>
        <source>Control plugins</source>
        <translation>Control de Plugins</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="503"/>
        <source>Enable and disable plugins</source>
        <translation>Activar o desactivar plugins</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="508"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
</context>
<context>
    <name>PackageSearchAboutDlg</name>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="13"/>
        <source>Debian Package Search</source>
        <translation>Búsqueda de paquetes Debian</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="34"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:18pt; font-weight:600;&quot;&gt;Debian Package Search&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Búsqueda de paquetes Debian</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="48"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="69"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Debian Package Search&lt;/span&gt;&lt;br /&gt;(c) 2004-2008, Benjamin Mesing&lt;br /&gt;Email: &lt;a href=&quot;mailto:bensmail@gmx.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;bensmail@gmx.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="82"/>
        <source>Homepage: &lt;a href=http://packagesearch.sourceforge.net&gt;http://packagesearch.sourceforge.net&lt;/a&gt;</source>
        <translation>Página Web: &lt;a href=http://packagesearch.sourceforge.net&gt;http://packagesearch.sourceforge.net&lt;/a&gt;

&lt;br&gt;&lt;br&gt;Traducido al Español por: &lt;b&gt;Frannoe&lt;/b&gt; (LMDE Cosillas)&lt;br&gt;
&lt;b&gt; http://ubuntu-cosillas.blogspot.com&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="106"/>
        <source>Thanks To</source>
        <translation>Gracias a</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="127"/>
        <source>&lt;p&gt;
&lt;b&gt;Enrico Zini&lt;/b&gt; for the wonderful Debtags System and 
his tips regarding this application. Enrico does also sponsor the updates to the debian server.&lt;br&gt;
&lt;/p&gt;
The &lt;b&gt;Bug reporters&lt;/b&gt; who provided a lot of help tracking down the bugs:
&lt;ul&gt;
&lt;li&gt;Thomas Kabelmann&lt;/li&gt;
&lt;li&gt;Ross Boylan&lt;/li&gt;
&lt;/ul&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="162"/>
        <source>Licence</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="183"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="230"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PackageSearchImpl</name>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="121"/>
        <source>Displays the packages matching the search entered above.</source>
        <translation>Muestra los paquetes que coincidan con la búsqueda introducidos anteriormente.</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="220"/>
        <source>Main Toolbar</source>
        <translation>Barra de herramientas principal</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="261"/>
        <source>Error trying to open Xapian database</source>
        <translation>Error al intentar abrir la base de datos Xapian</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="262"/>
        <source>&lt;p&gt;An error occurred trying to open the xapian database at /var/lib/apt-xapian-index/index please make sure the file is readable and valid. You may try to run update-apt-xapian-index to fix this problem. The orginal error message was:&lt;br&gt;&quot;</source>
        <translation>&lt;p&gt;Se produjo un error al intentar abrir la base de datos xapian en /var/lib/apt-xapian-index/index por favor asegúrese de que el archivo es legible y válido. Usted puede tratar de ejecutar update-apt-xapian-index para intentar solucionar este problema. Mensaje de error original:&lt;br&gt;&quot;</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="267"/>
        <source>Would you like to run update-apt-xapian-index now? Otherwise packagesearch will be terminated.</source>
        <translatorcomment>¿Quieres ejecutar update-apt-xapian-index ahora? De lo contrario Packagesearch se dará por terminado.</translatorcomment>
        <translation>¿Quieres ejecutar update-apt-xapian-index ahora? De lo contrario Packagesearch se dará por terminado.</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="337"/>
        <source>Unable to write settings</source>
        <translation>No se puede escribir en su configuración</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="337"/>
        <source>Unable to write settings to</source>
        <translation>No se puede escribir en su configuración de</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="693"/>
        <location filename="../src/packagesearchimpl.cpp" line="705"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="737"/>
        <source>Evaluating searches</source>
        <translation>Evaluación de las búsquedas</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="982"/>
        <source>Package Search Help Page</source>
        <translation>Página de ayuda</translation>
    </message>
</context>
<context>
    <name>PluginControl</name>
    <message>
        <location filename="../src/plugincontrol.ui" line="13"/>
        <source>PluginControl</source>
        <translation>Control de Plugin</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="44"/>
        <source>Enabled</source>
        <translation>Habilitado</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="49"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="54"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="59"/>
        <source>Location</source>
        <translation>Localización</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="106"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../src/settingsdlg.ui" line="13"/>
        <source>Packagesearch Settings</source>
        <translation>Configuración de búsqueda de paquetes Debian</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="37"/>
        <source>Main Settings</source>
        <translation>Ajustes globales</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="43"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The program selected here will be used to execute commands  as root when neccessary (e.g. when installing packages). &lt;tt&gt;su&lt;/tt&gt; is the default and available on every Debian system.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;tt&gt;sudo&lt;/tt&gt; requires the package sudo to be installed. If configured properly, &lt;tt&gt;sudo&lt;/tt&gt; might be more convenient, because you need to enter the password only once.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;￼&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body&gt;￼
El programa seleccionado se utiliza aquí para obtener privilegios de root, (por ejemplo, para la instalación de paquetes). 

&lt;tt&gt;su&lt;/tt&gt; es la selección predeterminada y está disponible en cualquier sistema Debian.&lt;/p&gt;￼
&lt;p&gt;&lt;tt&gt;sudo&lt;/tt&gt; requiere de la instalación del paquete &lt;tt&gt;sudo&lt;/tt&gt;. Si se configura sudo, es más conveniente para su uso, ya que la contraseña no se introduce con tanta frecuencia.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="51"/>
        <source>Program used to become root</source>
        <translation>Comado deseado para convertirse en usuario Root</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="57"/>
        <source>Use &quot;su&quot; to become root
</source>
        <translation>Utilizar &quot;su&quot; para ser usuario Root
</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="61"/>
        <source>su</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="68"/>
        <source>Use &quot;sudo&quot; to become root</source>
        <translation>Utilizar &quot;sudo&quot; para ser usuario Root</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="71"/>
        <source>sudo</source>
        <translation></translation>
    </message>
</context>
</TS>
