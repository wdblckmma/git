<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>ChoosenTagsDisplay</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/choosentagsdisplay.ui" line="13"/>
        <source>Form2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/choosentagsdisplay.ui" line="34"/>
        <source>Show the packages with all of the tags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/choosentagsdisplay.ui" line="44"/>
        <source>Exclude packages with any of the tags</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DebtagsSettingsWidget</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="13"/>
        <source>Form2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="41"/>
        <source>Facets Shown</source>
        <translation>Angezeigte Facetten</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="65"/>
        <source>&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="72"/>
        <source>&lt;&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="89"/>
        <source>Facets Hidden</source>
        <translation>Ausgeblendete Facetten</translation>
    </message>
</context>
<context>
    <name>NPlugin::DebtagsPlugin</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugin.cpp" line="168"/>
        <source>Performing tag search on package database</source>
        <translation>Führe Facettensuche aus</translation>
    </message>
</context>
<context>
    <name>NPlugin::DebtagsPluginContainer</name>
    <message>
        <source>Tag Database Not Available</source>
        <translation type="obsolete">Tag-Datenbank nicht verfügbar</translation>
    </message>
    <message>
        <source>&lt;p&gt;The tag database is not available and the debtags plugin was disabled!&lt;/p&gt;&lt;p&gt;You must execute &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; as root on the commandline to download the database. If debtags is not on your system you can install it via &lt;tt&gt;&lt;b&gt;apt-get install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt;Afterwards you can enable the debtags plugin via the plugin menu -&gt; Control Plugins.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Die Tag-Datenbank is nicht verfügbar und der debtags plugin wurde deaktiviert. Sie müssen auf der Kommandozeile &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; als root ausführen um die Datenbank herunterzuladen. Falls Debtags nicht auf Ihrem System installiert ist, können Sie es über &lt;tt&gt;&lt;b&gt;aptitude install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt; herunterladen.&lt;br&gt;
Anschließend können Sie den debtags-Plugin unter &quot;Packagesearch&quot; -&gt; &quot;Plugins verwalten&quot; aktivieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugincontainer.h" line="84"/>
        <source>Debtags Plugins</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;p&gt;The tag database is not available and the debtags plugin was disabled!&lt;/p&gt;&lt;p&gt;You must execute &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; as root on the commandline to download the database. If debtags is not on your system you can install it via &lt;tt&gt;&lt;b&gt;apt-get install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt;Afterwards you can enable the debtags plugin via the Packagesearch menu -&gt; Control Plugins.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Die Tag-Datenbank is nicht verfügbar und der debtags plugin wurde deaktiviert. Sie müssen auf der Kommandozeile &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; als root ausführen um die Datenbank herunterzuladen. Falls Debtags nicht auf Ihrem System installiert ist, können Sie es über &lt;tt&gt;&lt;b&gt;aptitude install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt; herunterladen.&lt;br&gt;Anschließend können Sie den debtags-Plugin unter &quot;Packagesearch&quot; -&gt; &quot;Plugins verwalten&quot; aktivieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugincontainer.cpp" line="116"/>
        <source>No vocabulary available</source>
        <translation>Vokabular-Datei fehlt</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugincontainer.cpp" line="117"/>
        <source>&lt;p&gt;The vocabulary is not available. This should not happen. Please reinstall &lt;tt&gt;debtags&lt;/tt&gt; or check your /var/lib/debtags/vocabulary file manually.&lt;/p&gt;The debtags plugin will be disabled for now, you can re-enable it via the Packagesearch menu -&gt; Control Plugins.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Vokabular-Datei fehlt. Dies sollte nicht vorkommen. Bitte installieren Sie &lt;tt&gt;debtags&lt;/tt&gt; erneut oder überprüfen Sie die Datei /var/lib/debtags/vocabulary von Hand.&lt;/p&gt;
&lt;p&gt;Der Debtags-Plugin wird deaktiviert. Er kann über das Packagesearch Menü unter &quot;Plugins Verwalten&quot; wieder aktiviert werden.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>NPlugin::RelatedPlugin</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="61"/>
        <source>Similar Plugin</source>
        <translation>Plugin für Ähnlichkeitssuche</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="66"/>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="71"/>
        <source>Plugin for searching packages similar to another package.</source>
        <translation>Plugin zum Suchen von Paketen die ähnlich einem anderen sind.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="110"/>
        <source>Similar</source>
        <translation>Ähnlich</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="189"/>
        <source>Searching for similar packages</source>
        <translation>Suche nach ähnlichen Paketen</translation>
    </message>
</context>
<context>
    <name>NTagModel::SelectedTagsView</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="41"/>
        <source>deselect a tag by double-clicking</source>
        <translation>Tag durch Doppelklicken entfernen</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="42"/>
        <source>This list displays the tags currently searched for. To remove a tag double-click it.</source>
        <translation>Listet die Tags auf, nach denen derzeit gesucht wird. Um einen Tag zu entfernen muss dieser doppelt angeklickt werden.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="72"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="77"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>NTagModel::UnselectedTagsView</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="118"/>
        <source>select a tag by double-clicking</source>
        <translation>Tag durch Doppelklicken auswählen</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="119"/>
        <source>This list shows the tags that can be searched for. The tags are organised in a tree beneath their facets (groups of tags). To search for packages with a tag, double-click the tag. Multiple tags can be selected like this. Facets cannot be selected.</source>
        <translation>Diese Liste zeigt die Tags, nach denen gesucht werden kann. Die Tags sind in einem Baum innerhalb ihrer Facette (Kategorie) angeordnet. Um einen Tag für die Suche auszuwählen, kann dieser doppelt angeklickt werden. Mehrere Tags können auf diese Weise ausgewählt werden. Facetten können nicht ausgewählt werden.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="160"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="165"/>
        <source>Collapse all</source>
        <translation>Alle zusammenklappen</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="166"/>
        <source>Expand all</source>
        <translation>Alle aufklappen</translation>
    </message>
</context>
<context>
    <name>NWidgets::SelectionInputAndDisplay</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectioninputanddisplay.cpp" line="48"/>
        <source>filter string to filter tags</source>
        <translation>String um Tags zu filtern</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectioninputanddisplay.cpp" line="49"/>
        <source>Filters the tag names by the string in realtime.</source>
        <translation>Filtert die Tags entsprechend des eingegeben Strings.</translation>
    </message>
</context>
<context>
    <name>RelatedFeedbackWidget</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="13"/>
        <source>Form2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="25"/>
        <source>Search packages similar to</source>
        <translation>Suche Pakete ähnlich zu</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="40"/>
        <source>displays the package to search related packages for</source>
        <translation>Zeigt das Paket an, zu dem ähnliche Pakete gesucht werden</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="43"/>
        <source>This displays the package for which you want to search related packages for. Enter the package under the &quot;Related&quot; section.</source>
        <translation>Hier wird das Paket angezeigt zu dem ähnliche Pakete gesucht werden. Das Paket kann in der Sektion &quot;Ähnliche&quot; ausgewählt werden.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="53"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>RelatedInput</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="14"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="26"/>
        <source>Similar to package</source>
        <translation>Ähnlich zu Paket</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="51"/>
        <source>Result packages</source>
        <translation>Anzahl der Pakete</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="58"/>
        <source>Number of result packages</source>
        <translation>Anzahl der Pakete die die Ähnlichkeitssuche zurückgeben soll</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="61"/>
        <source>Defines the number of the most similar packages which will be displayed.</source>
        <translation>Anzahl der ähnlichsten Pakete die von der Suche zurückgegeben werden sollen.</translation>
    </message>
    <message>
        <source>Maximum Distance:</source>
        <translation type="obsolete">Maximale Distanz:</translation>
    </message>
    <message>
        <source>Maximum number of tags the packages may differ</source>
        <translation type="obsolete">Maximale Anzahl an Tags in sich Pakete unterscheiden dürfen</translation>
    </message>
    <message>
        <source>This is the maximum number of tags in which the packages may differ from the given one. &lt;br&gt;
Or more formal: &lt;br&gt;
|(A union B) difference (A intersect B)| &amp;lt;= MaxDistance</source>
        <translation type="obsolete">Die Maximale Anzahl an Tags, in denen die Pakete von dem angegeben Paket abweichen dürfen.&lt;br&gt;Formal: &lt;br&gt;|(A vereinigt B) abzüglich (A geschnitten B)| &amp;lt;= MaxAnzahl</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="103"/>
        <source>Clear realated search</source>
        <translation>Ähnlichkeitssuche löschen</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="106"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>TagChooserWidget</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="16"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="28"/>
        <source>Show packages with these tags</source>
        <translation>Pakete mit diesen Tags anzeigen</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="35"/>
        <source>Check this to be able to exclude some tags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="38"/>
        <source>Check this if you want to exclude some tags. If you check it, a list of tags will be shown. There you can select which to exclude.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="41"/>
        <source>Exclude Tags</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="48"/>
        <source>but not these tags</source>
        <translation></translation>
    </message>
</context>
</TS>
