<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>NPlugin::ScreenshotPlugin</name>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="72"/>
        <source>Screenshot Plugin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="77"/>
        <source>Displays screenshots of the packages to be installed</source>
        <translation>Zeigt Screenshots von dem zu installierenden Paket</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="82"/>
        <source>This plugin displays screenshots of the packages to be installed. It fetches the screenshots from http://screenshots.debian.net so a working internet connection is required.</source>
        <translation>Dieser Plugin zeigt Screenshots für das zu installierende Paket an. Die Screenshots werden von http://screenshots.debian.net heruntergeladen, daher ist eine Internetverbindung erforderlich.</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="106"/>
        <source>Loading screenshot
Establishing connection, please wait...</source>
        <translation>Lade Screenshot
Verbindung wird hergestellt, bitte warten Sie...</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="157"/>
        <source>The screenshot size exceeds 10 MB. Something seems to be wrong here!
 Aborting Download.</source>
        <translation>Der Screenshot ist größer als 10MB irgendetwas scheint schiefgegangen zu sein!
Download wird abgebrochen.</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="164"/>
        <source>Loading screenshot - Progress: </source>
        <translation>Lade screenshot - Fortschritt: </translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="172"/>
        <source>No screenshot available for </source>
        <translation>Kein Screenshot für das Paket verfügbar </translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugin.cpp" line="181"/>
        <source>An error occured while trying to download the screenshot:
</source>
        <translation>Ein Fehler ist beim herunterladen des Screenshots aufgetreten:
</translation>
    </message>
</context>
<context>
    <name>NPlugin::ScreenshotPluginContainer</name>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugincontainer.cpp" line="86"/>
        <location filename="../src/plugins/screenshotplugin/screenshotplugincontainer.cpp" line="87"/>
        <source>Screenshots not supported</source>
        <translation>Screenshots werden nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../src/plugins/screenshotplugin/screenshotplugincontainer.h" line="51"/>
        <source>Screenshot Plugins</source>
        <translation></translation>
    </message>
</context>
</TS>
