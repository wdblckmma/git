<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>AptSearchPluginShortInputWidget</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="14"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="26"/>
        <source>Search for pattern</source>
        <translation>Buscar paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="41"/>
        <source>Search the package database for the given expression</source>
        <translation>Buscar en la base de datos el paquete indicado</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="52"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <source>Search case sensitive</source>
        <translation type="obsolete">Groß-/Kleinschreibung beachten</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation type="obsolete">Groß-/Kleinschreibung</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="66"/>
        <source>Search the package descriptions</source>
        <translation>Buscar por descripción del paquete</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="72"/>
        <source>Search Descriptions</source>
        <translation>Buscar también por descripción</translation>
    </message>
    <message>
        <source>Search only full words matching the search patterns</source>
        <translation type="obsolete">Nur ganze Wörter suchen die dem Suchausdruck entsprechen, keine Teilausdrücke</translation>
    </message>
    <message>
        <source>When searching only full word matches of the pattern will be considered a match, i.e. if a part of a word matches the pattern it will not be considered a match.
Every non-alphanumeric character is considerd to be a word boundary.</source>
        <translation type="obsolete">Es wird nur nach ganzen Wörtern gesucht, die dem Suchausdruck entsprechen.
Jedes nicht-alphanumerische Zeichen wird dabei als Wortgrenze betrachtet.</translation>
    </message>
    <message>
        <source>Whole Words Only</source>
        <translation type="obsolete">Ganze Wörter</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="44"/>
        <source>This searches the package names and descriptions for the given pattern. If you enter more than one word all words must be contained.&lt;br&gt;
You can search for phrases by using double quotes around the phrase. To enter patterns or phrases which should not appear in the package precede them with a minus (&apos;-&apos;).</source>
        <translation>Busca los nombres de los paquetes y las descripciones. Si introduce más de una palabra todas las palabras estarán contenidas en la búsqueda. &lt;br&gt;
Puede buscar frases utilizando comillas dobles alrededor de la frase. Para buscar patrones o frases que no aparecan en el paquete debe estar precedido por un signo menos (&apos;-&apos;).</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="69"/>
        <source>Check this if you want to search in the package descriptions. The search will search the package name, the long description and the short description.&lt;br /&gt;If not checked only the package names will be searched.</source>
        <translation>Seleccione esta opción si desea buscar los paquetes por descripciones. La búsqueda se hará por el nombre del paquete, una descripción larga y una descripción corta. &lt;br&gt;Si no se marca, sólo el nombre del paquete será buscado.</translation>
    </message>
</context>
<context>
    <name>AptSettingsWidget</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="19"/>
        <source>The selected tool will be used to install/remove the selected packages</source>
        <translation>La opción seleccionada se utilizará para instalar o eliminar los paquetes seleccionados</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="22"/>
        <source>For package installation Debian Package Search relies on an external program. You can select wether to use apt or aptitude here.</source>
        <translation>Para la instalación o eliminación de paquetes se utiliza un programa externo. Puede seleccionar el tipo a usar apt-get o aptitude.</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="25"/>
        <source>Package Administration Tool</source>
        <translation>Herramientas de administración de paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="31"/>
        <source>apt-get</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="38"/>
        <source>aptitude</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>InstalledFilterWidget</name>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="13"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="25"/>
        <source>Installed Filter</source>
        <translation>Filtro de búsqueda</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="32"/>
        <source>Search packages by installed state</source>
        <translation>Busca paquetes según su estado</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="35"/>
        <source>Search packages by installed state.</source>
        <translation>Busca paquetes según su estado.</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="39"/>
        <source>All</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="44"/>
        <source>Installed</source>
        <translation>instalados</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="49"/>
        <source>Upgradable</source>
        <translation>Actualizables</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="54"/>
        <source>Not Installed</source>
        <translation>No instalados</translation>
    </message>
</context>
<context>
    <name>NPlugin::AptActionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="42"/>
        <source>Update Apt-Package Database</source>
        <translation>Actualizar la Base de Datos Apt</translation>
    </message>
    <message>
        <source>Calls &quot;apt-get update&quot; updating the package database</source>
        <translation type="obsolete">Ruft &quot;apt-get update&quot; bzw. &quot;aptitude update&quot; zum aktualisieren der Paketdatenbank auf</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="43"/>
        <source>Updates the package database</source>
        <translation>Actualizar la Base de Datos de los paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="48"/>
        <source>Reloads the package database from disk (e.g. if apt-get update was performed externally).</source>
        <translation>Vuelve a cargar el paquete de la base de datos desde el disco (por ejemplo, si apt-get update se realizó externamente).</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="54"/>
        <source>Copy Command Line for Installing Package to Clipboard</source>
        <translation>Copiar al portapapeles el comando</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="55"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="56"/>
        <source>Creates a command line to install the selected package, and copies it to the clipboard</source>
        <translation>Copiarlo al portapapeles la línea de comandos para instalar el paquete seleccionado</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="60"/>
        <source>Install/Update Package</source>
        <translation>Instalar o Actualizar el paquete</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="61"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="62"/>
        <source>Installs/updates the package</source>
        <translation>Instalar o Actualizar los paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="66"/>
        <source>Remove Package</source>
        <translation>Eliminar paquete</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="67"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="68"/>
        <source>Removes the package</source>
        <translation>Eliminar los paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="72"/>
        <source>Purge Package</source>
        <translation>Purgar paquete</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="73"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="74"/>
        <source>Removes package including configuration</source>
        <translation>Eliminar paquetes incluyendo su configuración</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="183"/>
        <source>Unable to launch command</source>
        <translation>No se puede iniciar los comandos</translation>
    </message>
</context>
<context>
    <name>NPlugin::AptPluginContainer</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptplugincontainer.cpp" line="199"/>
        <location filename="../src/plugins/aptplugin/aptplugincontainer.cpp" line="208"/>
        <source>Command not executed</source>
        <translation>El comando no se ejecuta</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptplugincontainer.cpp" line="199"/>
        <source>For an unknwon reason, the command could not be executed.</source>
        <translation>Por a alguna razón desconocida, el programa no puede ser ejecutado.</translation>
    </message>
</context>
<context>
    <name>NPlugin::AptSearchPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="37"/>
        <source>Apt-Search Plugin</source>
        <translation>Plugin para la búsqueda Apt</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="38"/>
        <source>Performs a full text search</source>
        <translation>Realiza búsquedas de texto completo</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="39"/>
        <source>This plugin can be used to search the packages for expressions.</source>
        <translation>Este plugin se puede utilizar para  la búsqueda de paquetes con términos específicos.</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="137"/>
        <source>Delayed evaluation - waiting for further input</source>
        <translation>Evaluación - a la espera de nuevas aportaciones</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="147"/>
        <source>Performing full text search on package database</source>
        <translation>Realizar búsquedas de texto completo en la base de datos de los paquetes</translation>
    </message>
</context>
<context>
    <name>NPlugin::AvailableVersionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/availableversionplugin.cpp" line="27"/>
        <source>Available Version Plugin</source>
        <translation>Plugin versión disponible</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/availableversionplugin.cpp" line="28"/>
        <location filename="../src/plugins/aptplugin/availableversionplugin.cpp" line="29"/>
        <source>Shows the version for the package available for download</source>
        <translation>Muestra la versión del paquete disponible para su descarga</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/availableversionplugin.h" line="65"/>
        <source>Available Version</source>
        <translation>Disponible</translation>
    </message>
</context>
<context>
    <name>NPlugin::InstalledVersionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/installedversionplugin.cpp" line="25"/>
        <source>Installed Version Plugin</source>
        <translation>Plugin versión instalada</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedversionplugin.cpp" line="26"/>
        <location filename="../src/plugins/aptplugin/installedversionplugin.cpp" line="27"/>
        <source>Shows the version of the installed package in the package list</source>
        <translation>Muestra información sobre la versión instalada de la lista de paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedversionplugin.h" line="63"/>
        <source>Installed Version</source>
        <translation>Instalado</translation>
    </message>
</context>
<context>
    <name>NPlugin::PackageDescriptionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="63"/>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.h" line="91"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="189"/>
        <source>&lt;h3&gt;Package not found&lt;/h3&gt;&lt;p&gt;Could not find a valid description for the package &lt;b&gt;</source>
        <translation>&lt;h3&gt; Paquete no encontrado&lt;/h3&gt;&lt;p&gt;No se pudo encontrar una descripción válida para el paquete &lt;b&gt;</translation>
    </message>
    <message>
        <source>&lt;/b&gt; in the database.&lt;br&gt;This could mean either that you have selected a virtual package or that the package description  was not found for an unknown reason. It is possible that your debtags and apt database are out of sync. Try running &lt;tt&gt;debtags update&lt;/tt&gt; and &lt;tt&gt;apt-get update&lt;/tt&gt; as root.&lt;/p&gt;</source>
        <translation type="obsolete"> konnte keine gültige Beschreibung in der Datenbank gefunden werden. &lt;br&gt;Dies bedeutet entweder dass ein virtuelles Paket ausgewählt wurde oder dass die Paketbeschreibung aus einem anderen Grund nicht gefunden werden konnte. &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.h" line="57"/>
        <source>PackageDescriptionPlugin</source>
        <translation>Plugin para la descripción de los paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="191"/>
        <source>&lt;/b&gt; in the database.&lt;br&gt;This could mean either that you have selected a virtual package or that the package description  was not found for an unknown reason.&lt;/p&gt;</source>
        <translation>&lt;/b&gt;. Esto significa que, o bien un paquete virtual se ha seleccionado o que la descripción del paquete por cualquier otra razón no pudo ser encontrada.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="217"/>
        <source>&lt;b&gt;Installed Version&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Versión instalada&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="219"/>
        <source>&lt;b&gt;Available Version&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Versión disponible&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="221"/>
        <source>&lt;b&gt;Available Version&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Versión disponible&lt;/b&gt;no disponible&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="223"/>
        <source>&lt;b&gt;Essential&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Esencial&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="227"/>
        <source>&lt;b&gt;Priority&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Prioritario&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="231"/>
        <source>&lt;b&gt;Section&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Grupo&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="233"/>
        <source>&lt;b&gt;Section&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Grupo&lt;/b&gt;: no disponibles&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="235"/>
        <source>&lt;b&gt;Installed Size&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Tamaño (KiB) una vez instalado&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="237"/>
        <source>&lt;b&gt;Installed Size&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Tamaño (KiB) instalado&lt;/b&gt;: no disponible&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="239"/>
        <source>&lt;b&gt;Maintainer&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Mantenedor&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="241"/>
        <source>&lt;b&gt;Maintainer&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Mantenedor&lt;/b&gt;: no disponible&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="243"/>
        <source>&lt;b&gt;Architecture&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Arquitectura&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="245"/>
        <source>&lt;b&gt;Architecture&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Arquitectura&lt;/b&gt;: no disponible&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="247"/>
        <source>&lt;b&gt;Source&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Fuente&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="251"/>
        <source>&lt;b&gt;Replaces&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Remplaza&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="256"/>
        <source>&lt;b&gt;Provides&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Proporciona&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="261"/>
        <source>&lt;b&gt;Pre-Depends&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Pre-Dependencias&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="266"/>
        <source>&lt;b&gt;Depends&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Dependencias&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="271"/>
        <source>&lt;b&gt;Recommends:&lt;/b&gt; </source>
        <translation>&lt;b&gt;Recomendados:&lt;/b&gt; </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="276"/>
        <source>&lt;b&gt;Suggests:&lt;/b&gt; </source>
        <translation>&lt;b&gt;Sugeridos:&lt;/b&gt; </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="281"/>
        <source>&lt;b&gt;Conflicts&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Conflictivos&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="284"/>
        <source>&lt;b&gt;Filename&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Nombre de archivo&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="286"/>
        <source>&lt;b&gt;Filename&lt;/b&gt;: not available&lt;br&gt;
</source>
        <translation>&lt;b&gt;Nombre de archivo&lt;/b&gt;: no disponible&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="290"/>
        <source>&lt;b&gt;Size&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Tamaño (bytes)&lt;/b&gt;: no disponible&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="292"/>
        <source>&lt;b&gt;MD5sum&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Valor hash MD5&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="294"/>
        <source>&lt;b&gt;MD5sum&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Valor hash MD5&lt;/b&gt;: no disponible&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="296"/>
        <source>&lt;b&gt;Conffiles&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Archivos de configuración&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="288"/>
        <source>&lt;b&gt;Size&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Tamaño (bytes)&lt;/b&gt;: </translation>
    </message>
</context>
<context>
    <name>NPlugin::PackageStatusPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/packagestatusplugin.cpp" line="19"/>
        <source>Package Status Plugin</source>
        <translation>Plugin para el estado del paquete</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="47"/>
        <source>Reload Package Database</source>
        <translation>Recargar la Base de Datos</translation>
    </message>
</context>
</TS>
