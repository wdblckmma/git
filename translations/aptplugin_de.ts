<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>AptSearchPluginShortInputWidget</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="14"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="26"/>
        <source>Search for pattern</source>
        <translation>Nach Zeichenkette suchen</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="41"/>
        <source>Search the package database for the given expression</source>
        <translation>Durchsucht die Paketdatenbank nach dem angegebenen Ausdruck</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="52"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Search case sensitive</source>
        <translation type="obsolete">Groß-/Kleinschreibung beachten</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation type="obsolete">Groß-/Kleinschreibung</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="66"/>
        <source>Search the package descriptions</source>
        <translation>Paketbeschreibungen mit durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="72"/>
        <source>Search Descriptions</source>
        <translation>Beschreibungen durchsuchen</translation>
    </message>
    <message>
        <source>Search only full words matching the search patterns</source>
        <translation type="obsolete">Nur ganze Wörter suchen die dem Suchausdruck entsprechen, keine Teilausdrücke</translation>
    </message>
    <message>
        <source>When searching only full word matches of the pattern will be considered a match, i.e. if a part of a word matches the pattern it will not be considered a match.
Every non-alphanumeric character is considerd to be a word boundary.</source>
        <translation type="obsolete">Es wird nur nach ganzen Wörtern gesucht, die dem Suchausdruck entsprechen.
Jedes nicht-alphanumerische Zeichen wird dabei als Wortgrenze betrachtet.</translation>
    </message>
    <message>
        <source>Whole Words Only</source>
        <translation type="obsolete">Ganze Wörter</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="44"/>
        <source>This searches the package names and descriptions for the given pattern. If you enter more than one word all words must be contained.&lt;br&gt;
You can search for phrases by using double quotes around the phrase. To enter patterns or phrases which should not appear in the package precede them with a minus (&apos;-&apos;).</source>
        <translation>Die Paketnamen und -beschreibungen werden nach der eingegebenen Zeichenkette durchsucht. Wird mehr als ein Wort angeben, so müssen alle Wörter gefunden werden.&lt;br&gt;
Nach Wortgruppen kann gesucht werden, indem diese in Anführungsstriche gesetzt werden. Um eine unerwünschte Zeichenkette einzugeben kann ein Minuszeichen &apos;-&apos; vorangestellt werden.</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchpluginshortinputwidget.ui" line="69"/>
        <source>Check this if you want to search in the package descriptions. The search will search the package name, the long description and the short description.&lt;br /&gt;If not checked only the package names will be searched.</source>
        <translation>Wenn dieses Kästchen aktiviert ist, werden die Paketbeschreibungen mit durchsucht. Die Suche sucht dann in dem Namen, der ausführlichen und der Kurzbeschreibung. Ansonsten werden nur die Paketnamen durchsucht.</translation>
    </message>
</context>
<context>
    <name>AptSettingsWidget</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="13"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="19"/>
        <source>The selected tool will be used to install/remove the selected packages</source>
        <translation>Das ausgewählte Programm wird zu Installation/Deinstallation von Paketen verwendet</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="22"/>
        <source>For package installation Debian Package Search relies on an external program. You can select wether to use apt or aptitude here.</source>
        <translation>Zur Installation/Deinstallation von Paketen verwendet die Debian Paketsuche ein externes Programm. Hier kann eingestellt werden welches Programm verwendet werden soll.</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="25"/>
        <source>Package Administration Tool</source>
        <translation>Programm zur Paketverwaltung</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="31"/>
        <source>apt-get</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsettingswidget.ui" line="38"/>
        <source>aptitude</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>InstalledFilterWidget</name>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="13"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="25"/>
        <source>Installed Filter</source>
        <translation>Installationsstatus</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="32"/>
        <source>Search packages by installed state</source>
        <translation>Pakete anhand ihres Installationsstatuses suchen</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="35"/>
        <source>Search packages by installed state.</source>
        <translation>Pakete anhand ihres Installationsstatuses suchen.</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="39"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="44"/>
        <source>Installed</source>
        <translation>Installiert</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="49"/>
        <source>Upgradable</source>
        <translation>Aktualisierbar</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedfilterwidget.ui" line="54"/>
        <source>Not Installed</source>
        <translation>Nicht Installiert</translation>
    </message>
</context>
<context>
    <name>NPlugin::AptActionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="42"/>
        <source>Update Apt-Package Database</source>
        <translation>Apt-Paketdatenbank aktualisieren</translation>
    </message>
    <message>
        <source>Calls &quot;apt-get update&quot; updating the package database</source>
        <translation type="obsolete">Ruft &quot;apt-get update&quot; bzw. &quot;aptitude update&quot; zum aktualisieren der Paketdatenbank auf</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="43"/>
        <source>Updates the package database</source>
        <translation>Paketdatenbank aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="48"/>
        <source>Reloads the package database from disk (e.g. if apt-get update was performed externally).</source>
        <translation>Aktualisiert die Paketinformationen von der Festplatte (z.B. wenn &quot;aptitude update&quot; extern aufgerufen wurde).</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="54"/>
        <source>Copy Command Line for Installing Package to Clipboard</source>
        <translation>Kommandozeile zur Paketinstallation in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="55"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="56"/>
        <source>Creates a command line to install the selected package, and copies it to the clipboard</source>
        <translation>Erzeugt Kommandozeile zur Installation des Paketes und kopiert diese in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="60"/>
        <source>Install/Update Package</source>
        <translation>Paket Installieren/Aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="61"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="62"/>
        <source>Installs/updates the package</source>
        <translation>Installiert/aktualisiert das Paket</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="66"/>
        <source>Remove Package</source>
        <translation>Paket entfernen</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="67"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="68"/>
        <source>Removes the package</source>
        <translation>Entfernt das Paket</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="72"/>
        <source>Purge Package</source>
        <translation>Paket restlos entfernen</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="73"/>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="74"/>
        <source>Removes package including configuration</source>
        <translation>Entfernt Paket inklusive der Konfigurationsdateien</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="183"/>
        <source>Unable to launch command</source>
        <translation>Konnte Programm nicht starten</translation>
    </message>
</context>
<context>
    <name>NPlugin::AptPluginContainer</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptplugincontainer.cpp" line="199"/>
        <location filename="../src/plugins/aptplugin/aptplugincontainer.cpp" line="208"/>
        <source>Command not executed</source>
        <translation>Programm nicht ausgeführt</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptplugincontainer.cpp" line="199"/>
        <source>For an unknwon reason, the command could not be executed.</source>
        <translation>Aufgrund eines unbekannten Fehlers konnte das Programm nicht ausgeführt werden.</translation>
    </message>
</context>
<context>
    <name>NPlugin::AptSearchPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="37"/>
        <source>Apt-Search Plugin</source>
        <translation>Plugin für Apt-Suche</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="38"/>
        <source>Performs a full text search</source>
        <translation>Führt Volltextsuche aus</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="39"/>
        <source>This plugin can be used to search the packages for expressions.</source>
        <translation>Dieser Plugin kann verwendet werden, um Pakete nach bestimmten Ausdrücken zu durchsuchen.</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="137"/>
        <source>Delayed evaluation - waiting for further input</source>
        <translation>Verzögerte Auswertung - warte auf weitere Eingaben</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/aptsearchplugin.cpp" line="147"/>
        <source>Performing full text search on package database</source>
        <translation>Volltextsuche über Paketdatenbank wird ausgeführt</translation>
    </message>
</context>
<context>
    <name>NPlugin::AvailableVersionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/availableversionplugin.cpp" line="27"/>
        <source>Available Version Plugin</source>
        <translation>Plugin für Verfügbare Version</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/availableversionplugin.cpp" line="28"/>
        <location filename="../src/plugins/aptplugin/availableversionplugin.cpp" line="29"/>
        <source>Shows the version for the package available for download</source>
        <translation>Zeigt Informationen über die verfügbare Version von Paketen an</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/availableversionplugin.h" line="65"/>
        <source>Available Version</source>
        <translation>Verfügbare Version</translation>
    </message>
</context>
<context>
    <name>NPlugin::InstalledVersionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/installedversionplugin.cpp" line="25"/>
        <source>Installed Version Plugin</source>
        <translation>Plugin für Installierte Version</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedversionplugin.cpp" line="26"/>
        <location filename="../src/plugins/aptplugin/installedversionplugin.cpp" line="27"/>
        <source>Shows the version of the installed package in the package list</source>
        <translation>Zeigt Informationen über die installierte Version von Paketen an</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/installedversionplugin.h" line="63"/>
        <source>Installed Version</source>
        <translation>Installierte Version</translation>
    </message>
</context>
<context>
    <name>NPlugin::PackageDescriptionPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="63"/>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.h" line="91"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Package not found&lt;/h3&gt;&lt;p&gt;Could not find a valid description for the package &lt;b&gt;</source>
        <translation type="obsolete">&lt;h3&gt;Paket nicht gefunden&lt;/h3&gt;&lt;p&gt;Für das Paket &lt;b&gt;</translation>
    </message>
    <message>
        <source>&lt;/b&gt; in the database.&lt;br&gt;This could mean either that you have selected a virtual package or that the package description  was not found for an unknown reason. It is possible that your debtags and apt database are out of sync. Try running &lt;tt&gt;debtags update&lt;/tt&gt; and &lt;tt&gt;apt-get update&lt;/tt&gt; as root.&lt;/p&gt;</source>
        <translation type="obsolete"> konnte keine gültige Beschreibung in der Datenbank gefunden werden. &lt;br&gt;Dies bedeutet entweder dass ein virtuelles Paket ausgewählt wurde oder dass die Paketbeschreibung aus einem anderen Grund nicht gefunden werden konnte. &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.h" line="57"/>
        <source>PackageDescriptionPlugin</source>
        <translation>Plugin für Paketbeschreibungen</translation>
    </message>
    <message>
        <source>&lt;/b&gt; in the database.&lt;br&gt;This could mean either that you have selected a virtual package or that the package description  was not found for an unknown reason.&lt;/p&gt;</source>
        <translation type="obsolete"> konnte keine gültige Beschreibung in der Datenbank gefunden werden. &lt;br&gt;Dies bedeutet entweder dass ein virtuelles Paket ausgewählt wurde oder dass die Paketbeschreibung aus einem anderen Grund nicht gefunden werden konnte. &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="189"/>
        <source>&lt;h3&gt;Package not found&lt;/h3&gt;&lt;p&gt;Could not find a valid description for &lt;b&gt;</source>
        <translation>&lt;h3&gt;Paket nicht gefunden&lt;/h3&gt;&lt;p&gt;Für das Paket &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="191"/>
        <source>&lt;/b&gt;.&lt;br&gt;</source>
        <translation>konnte keine Beschreibung gefunden werden.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="192"/>
        <source>This could mean either that you have selected a virtual package or that the package description  was not found for an unknown reason.&lt;/p&gt;</source>
        <translation>Dies bedeutet entweder dass ein virtuelles Paket ausgewählt wurde oder dass die Paketbeschreibung aus einem anderen Grund nicht gefunden werden konnte.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="217"/>
        <source>&lt;b&gt;Installed Version&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Installierte Version&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="219"/>
        <source>&lt;b&gt;Available Version&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Verfügbare Version&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="221"/>
        <source>&lt;b&gt;Available Version&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Verfügbare Version: &lt;/b&gt; nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="223"/>
        <source>&lt;b&gt;Essential&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Essenziell&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="227"/>
        <source>&lt;b&gt;Priority&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Priorität&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="231"/>
        <source>&lt;b&gt;Section&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Sektion&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="233"/>
        <source>&lt;b&gt;Section&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Sektion&lt;/b&gt;: nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="235"/>
        <source>&lt;b&gt;Installed Size&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Installierte Größe&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="237"/>
        <source>&lt;b&gt;Installed Size&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Installierte Größe&lt;/b&gt;: nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="239"/>
        <source>&lt;b&gt;Maintainer&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Verwalter&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="241"/>
        <source>&lt;b&gt;Maintainer&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Verwalter&lt;/b&gt;: nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="243"/>
        <source>&lt;b&gt;Architecture&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Architektur&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="245"/>
        <source>&lt;b&gt;Architecture&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Architektur&lt;/b&gt;: nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="247"/>
        <source>&lt;b&gt;Source&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Quelle&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="251"/>
        <source>&lt;b&gt;Replaces&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Ersetzt&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="256"/>
        <source>&lt;b&gt;Provides&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Bietet an&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="261"/>
        <source>&lt;b&gt;Pre-Depends&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Vorabhängigkeiten&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="266"/>
        <source>&lt;b&gt;Depends&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Hängt ab&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="271"/>
        <source>&lt;b&gt;Recommends:&lt;/b&gt; </source>
        <translation>&lt;b&gt;Empfiehlt:&lt;/b&gt; </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="276"/>
        <source>&lt;b&gt;Suggests:&lt;/b&gt; </source>
        <translation>&lt;b&gt;Schlägt vor:&lt;/b&gt; </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="281"/>
        <source>&lt;b&gt;Conflicts&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Konflikt mit&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="284"/>
        <source>&lt;b&gt;Filename&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Dateiname&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="286"/>
        <source>&lt;b&gt;Filename&lt;/b&gt;: not available&lt;br&gt;
</source>
        <translation>&lt;b&gt;Dateiname&lt;/b&gt;: nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="290"/>
        <source>&lt;b&gt;Size&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;Größe&lt;/b&gt;: nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="292"/>
        <source>&lt;b&gt;MD5sum&lt;/b&gt;: </source>
        <translation>&lt;b&gt;MD5-Hash-Wert&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="294"/>
        <source>&lt;b&gt;MD5sum&lt;/b&gt;: not available&lt;br&gt;</source>
        <translation>&lt;b&gt;MD5-Hash-Wert&lt;/b&gt;: nicht verfügbar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="296"/>
        <source>&lt;b&gt;Conffiles&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Konfigurationsdateien&lt;/b&gt;: </translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="298"/>
        <source>&lt;b&gt;Homepage&lt;/b&gt;: &lt;a HREF=&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/aptplugin/packagedescriptionplugin.cpp" line="288"/>
        <source>&lt;b&gt;Size&lt;/b&gt;: </source>
        <translation>&lt;b&gt;Größe&lt;/b&gt;: </translation>
    </message>
</context>
<context>
    <name>NPlugin::PackageStatusPlugin</name>
    <message>
        <location filename="../src/plugins/aptplugin/packagestatusplugin.cpp" line="19"/>
        <source>Package Status Plugin</source>
        <translation>Plugin für Paketstatus</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/aptplugin/aptactionplugin.cpp" line="47"/>
        <source>Reload Package Database</source>
        <translation>Neuladen der Paketdatenbank</translation>
    </message>
</context>
</TS>
