<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>FilenameFeedbackWidget</name>
    <message>
        <source>Search for filename</source>
        <translation type="obsolete">Suche nach Dateinamen</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>Shows the filename that was searched for</source>
        <translation type="obsolete">Der Dateiname nach dem gesucht wurde</translation>
    </message>
</context>
<context>
    <name>FilenameSearchInput</name>
    <message>
        <source>Search packages with files containing</source>
        <translation>Buscar paquetes con archivos que contiene</translation>
    </message>
    <message>
        <source>search installed packages only</source>
        <translation>buscar sólo paquetes instalados</translation>
    </message>
    <message>
        <source>Search packages containing a file whose filename matches the pattern</source>
        <translation type="obsolete">Suche nach Pakete mit einer Datei deren Name die angegebene Zeichenkette enthält</translation>
    </message>
    <message>
        <source>Check this if you want to search only the installed packages (usually much faster)</source>
        <translation type="obsolete">Nur in installierten Paketen suchen (normalerweise deutlich schneller)</translation>
    </message>
</context>
<context>
    <name>FilenameView</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="56"/>
        <source> not available</source>
        <translation>no disponible</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="57"/>
        <source>The &lt;tt&gt;</source>
        <translation>El Comando&lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="57"/>
        <source>&lt;/tt&gt; command is not available.
Please make sure that the &lt;tt&gt;mime-support&lt;/tt&gt; package is installed and that you have permission to execute &lt;tt&gt;</source>
        <translation>&lt;/tt&gt; no está disponible.
Por favor, asegúrese de que el paquete &lt;tt&gt;mime-support&lt;/tt&gt; esté instalado y que tiene permisos para ejecutarlo &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="71"/>
        <source>Unable to launch </source>
        <translation>No se puede iniciar </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="72"/>
        <source>Launching &lt;tt&gt;</source>
        <translation>La ejecución &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="72"/>
        <source>&lt;/tt&gt; failed due to an unknown reason.</source>
        <translation>&lt;/tt&gt; falló debido a una error desconocido.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="95"/>
        <source>Error viewing file</source>
        <translation>Error al visualizar el archivo</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="95"/>
        <source>Unable to view file </source>
        <translation>No se puede ver el archivo </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="96"/>
        <source>
Tried &lt;tt&gt;</source>
        <translation>
Probando &lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="96"/>
        <source>&lt;/tt&gt; and
&lt;tt&gt;</source>
        <translation>&lt;/tt&gt; con
&lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="153"/>
        <source>Copy to clipboard</source>
        <translation>Copiar al portapapeles</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="154"/>
        <source>Copy all filenames to clipboard</source>
        <translation>Copia todos los archivos al portapapeles</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="155"/>
        <source>View file (depends on settings in /etc/mailcap)</source>
        <translation>Ver archivo (esto depende de suconfiguración en /etc/mailcap)</translation>
    </message>
    <message>
        <source>Show the filelist for the selected package</source>
        <translation type="obsolete">Zeigt Dateiliste für ausgewähltes Paket an</translation>
    </message>
    <message>
        <source>Shows a list of the files which are included in the package. If the list is already shown it will be updated.&lt;br&gt;
For installed packages the list is shown by default because it is quite fast. For not installed package it is only shown if this button is clicked as it takes a considerable amount of time.</source>
        <translation type="obsolete">Zeigt die Liste der Dateien in dem aktuellen Paket an. Wenn die Liste bereits angezeigt ist, so wird diese aktualisiert.&lt;br&gt;
Für installierte Pakete wird die Dateiliste standardmäßig angezeigt da dies schnell ist. Für nicht-installierte Pakete wird diese nur angezeigt, wenn der Knopf angeklickt wird da diese eine zeitlang dauert.</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="59"/>
        <source>&lt;/tt&gt;.</source>
        <translation>&lt;/tt&gt;.</translation>
    </message>
    <message>
        <source>Filter files to be shown</source>
        <translation type="obsolete">Filter für angezeigte Dateien</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="51"/>
        <source>Trying to view </source>
        <translation>Tratando de ver </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="103"/>
        <source>Retrying to view </source>
        <translation>Intentando de nuevo para ver </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="103"/>
        <source> with mimetype text/plain</source>
        <translation> con mime type text/a simple vista</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="114"/>
        <source>Finished viewing </source>
        <translation>Ver el final del archivo </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="130"/>
        <source>Can&apos;t view file </source>
        <translation>No se puede ver el archivo </translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameview.cpp" line="130"/>
        <source>, it is not viewable</source>
        <translation>, este archivo no es visible</translation>
    </message>
</context>
<context>
    <name>NPlugin::FilenameActionPlugin</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameactionplugin.cpp" line="29"/>
        <source>Calls &quot;apt-file update&quot; updating the file database</source>
        <translation>Llama a &quot;apt-file update&quot; para actualizar la base de datos</translation>
    </message>
</context>
<context>
    <name>NPlugin::FilenamePlugin</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="66"/>
        <source>Filename Plugin</source>
        <translation>Plugin para nombres de archivos</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="71"/>
        <source>Plugin to search by files and diplay files in packages.</source>
        <translation>Este plugin permite navegar por los paquetes y ver los archivos en los paquetes.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="76"/>
        <source>This plugin allows to search the packages for files, and to display the files included in the packages
For information about not installed packages, it needs the &lt;tt&gt;apt-file&lt;/tt&gt; tool.</source>
        <translation>El plugin permite navegar por los archivos y ver archivos en paquetes.
Para ver los archivos en el paquete que no esté instalado es necesario el paquete&lt;tt&gt;apt-file&lt;/tt&gt;.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="115"/>
        <source>Files</source>
        <translation>Archivos</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="278"/>
        <source>There is no file information for the current package available.</source>
        <translation>No hay información de archivo para el paquete más reciente disponible.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="308"/>
        <source>delayed evaluation - waiting for further input</source>
        <translation>Evaluación de retardo - a la espera de nuevas aportaciones</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="326"/>
        <source>Apt file search not availabe</source>
        <translation>Búsqueda de archivo Apt no disponible</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="327"/>
        <source>You need the &lt;tt&gt;apt-file&lt;/tt&gt; utility to search files in packages not installed.&lt;br&gt;To get apt-file fetch it via &lt;tt&gt;apt-get install apt-file&lt;/tt&gt; and run &lt;tt&gt;apt-file update&lt;/tt&gt; afterwards.</source>
        <translation>Usted necesita &lt;tt&gt;apt-file&lt;/tt&gt; es una utilidad para buscar archivos en paquetes no instalados.&lt;br&gt;Para obtener apt-file realice &lt;tt&gt;apt-get install apt-file&lt;/tt&gt; luego ejecute &lt;tt&gt;apt-file update&lt;/tt&gt;.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="340"/>
        <source>Performing search for filenames, this might take a while</source>
        <translation>Buscar por nombre de archivo, esto puede tomar algún tiempo</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="178"/>
        <source>Filenames</source>
        <translation>Nombre de archivo</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="237"/>
        <source>Querying database for file list</source>
        <translation>Consultar la lista de archivos de la base de datos</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="417"/>
        <source>&lt;font color=#606060&gt;&lt;p&gt;File list for &lt;b&gt;</source>
        <translation>&lt;font color=#606060&gt;&lt;p&gt;Lista de archivos &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="417"/>
        <source>&lt;/b&gt; not available.&lt;/p&gt;&lt;p&gt;For displaying the file list for not-installed packages, the &quot;apt-file&quot; package is required. Please install &quot;apt-file&quot; and retrieve the latest file database by running  &lt;tt&gt;apt-file update&lt;/tt&gt;.&lt;/p&gt;&lt;/font&gt;</source>
        <translation>&lt;/b&gt; no está disponible. &lt;/p&gt;&lt;p&gt;Para mostrar la lista de archivos para los paquetes no-instalados, es necesario el paquete &quot;apt-file&quot;. Por favor, instale &quot;apt-file&quot; y actualice la lista de la base de datos de archivos mediante la ejecución de&lt;tt&gt;apt-file update&lt;/tt&gt;.&lt;/p&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugin.cpp" line="136"/>
        <source>&lt;font color=#606060&gt;For packages &lt;b&gt;not installed&lt;/b&gt;, the file list is not shown by default. This is because retreiving the file list will take some time.&lt;br&gt;Please click the &lt;b&gt;&amp;quot;Show&amp;quot;&lt;/b&gt; button to show the filelist for the selected package.&lt;/font&gt;</source>
        <translation>&lt;font color=#606060&gt;Para los paquetes &lt;b&gt;no instalados&lt;/b&gt;, la lista de archivos no se muestra por defecto. Recuperar la lista de archivos tomará algún tiempo.&lt;br&gt; Por favor, haga clic en el botón&lt;b&gt;&amp;quot;Mostrar&amp;quot;&lt;/b&gt; para ver la lista de archivos del paquete seleccionado.&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>NPlugin::FilenamePluginContainer</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="103"/>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="112"/>
        <source>Command not executed</source>
        <translation>Comando no ejecutado</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="103"/>
        <source>For an unknwon reason, the command could not be executed.</source>
        <translation>Por razones desconocidas, el comando no puede ser ejecutado.</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="124"/>
        <source>Update not successfully completed</source>
        <translation>Actualización fallida</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="125"/>
        <source>The apt-file update was not completed successfully.&lt;br&gt;The database might be broken, rerun &lt;tt&gt;apt-file update&lt;/tt&gt; to fix this.</source>
        <translation>La actualización apt-file no se completó correctamente. &lt;br&gt;Pueden existir paquetes rotos en la base de datos, &lt;br&gt;vuelva a ejecutar &lt;tt&gt;apt-file update&lt;/tt&gt; para intentar solucionar este problema.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameactionplugin.cpp" line="28"/>
        <source>Update File Database</source>
        <translation>Actualización de los archivos de la base de datos</translation>
    </message>
    <message>
        <location filename="../src/plugins/filenameplugin/filenameplugincontainer.cpp" line="83"/>
        <source>Filename Plugins</source>
        <translation>Plugins para nombres de archivos</translation>
    </message>
</context>
</TS>
