<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>ChoosenTagsDisplay</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/choosentagsdisplay.ui" line="13"/>
        <source>Form2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/choosentagsdisplay.ui" line="34"/>
        <source>Show the packages with all of the tags</source>
        <translation>Mostrar las etiquetas en los paquetes</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/choosentagsdisplay.ui" line="44"/>
        <source>Exclude packages with any of the tags</source>
        <translation>No mostrar las etiquetas en los paquetes</translation>
    </message>
</context>
<context>
    <name>DebtagsSettingsWidget</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="13"/>
        <source>Form2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="41"/>
        <source>Facets Shown</source>
        <translation>Añadir a la interfaz</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="65"/>
        <source>&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="72"/>
        <source>&lt;&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagssettingswidget.ui" line="89"/>
        <source>Facets Hidden</source>
        <translation>Ocultar de la interfaz</translation>
    </message>
</context>
<context>
    <name>NPlugin::DebtagsPlugin</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugin.cpp" line="168"/>
        <source>Performing tag search on package database</source>
        <translation>Búsqueda de etiquetas en la base de datos de los paquetes</translation>
    </message>
</context>
<context>
    <name>NPlugin::DebtagsPluginContainer</name>
    <message>
        <source>Tag Database Not Available</source>
        <translation type="obsolete">Tag-Datenbank nicht verfügbar</translation>
    </message>
    <message>
        <source>&lt;p&gt;The tag database is not available and the debtags plugin was disabled!&lt;/p&gt;&lt;p&gt;You must execute &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; as root on the commandline to download the database. If debtags is not on your system you can install it via &lt;tt&gt;&lt;b&gt;apt-get install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt;Afterwards you can enable the debtags plugin via the plugin menu -&gt; Control Plugins.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Die Tag-Datenbank is nicht verfügbar und der debtags plugin wurde deaktiviert. Sie müssen auf der Kommandozeile &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; als root ausführen um die Datenbank herunterzuladen. Falls Debtags nicht auf Ihrem System installiert ist, können Sie es über &lt;tt&gt;&lt;b&gt;aptitude install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt; herunterladen.&lt;br&gt;
Anschließend können Sie den debtags-Plugin unter &quot;Packagesearch&quot; -&gt; &quot;Plugins verwalten&quot; aktivieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugincontainer.h" line="84"/>
        <source>Debtags Plugins</source>
        <translation>Plugin Etiquetas Deb</translation>
    </message>
    <message>
        <source>&lt;p&gt;The tag database is not available and the debtags plugin was disabled!&lt;/p&gt;&lt;p&gt;You must execute &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; as root on the commandline to download the database. If debtags is not on your system you can install it via &lt;tt&gt;&lt;b&gt;apt-get install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt;Afterwards you can enable the debtags plugin via the Packagesearch menu -&gt; Control Plugins.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Die Tag-Datenbank is nicht verfügbar und der debtags plugin wurde deaktiviert. Sie müssen auf der Kommandozeile &lt;tt&gt;&lt;b&gt;debtags update&lt;/b&gt;&lt;/tt&gt; als root ausführen um die Datenbank herunterzuladen. Falls Debtags nicht auf Ihrem System installiert ist, können Sie es über &lt;tt&gt;&lt;b&gt;aptitude install debtags&lt;/b&gt;&lt;/tt&gt;&lt;br&gt; herunterladen.&lt;br&gt;Anschließend können Sie den debtags-Plugin unter &quot;Packagesearch&quot; -&gt; &quot;Plugins verwalten&quot; aktivieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugincontainer.cpp" line="115"/>
        <source>No vocabulary available</source>
        <translation>No disponible en el vocabulario</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/debtagsplugincontainer.cpp" line="116"/>
        <source>&lt;p&gt;The vocabulary is not available. This should not happen. Please reinstall &lt;tt&gt;debtags&lt;/tt&gt; or check your /var/lib/debtags/vocabulary file manually.&lt;/p&gt;The debtags plugin will be disabled for now, you can re-enable it via the Packagesearch menu -&gt; Control Plugins.&lt;/p&gt;</source>
        <translation>El vocabulario no está disponible. Esto no debería ocurrir. . Por favor, vuelva a instalar &lt;tt&gt;debtags&lt;/tt&gt; o consulte manualmente el archivo: /var/lib/debtags/vocabulary &lt;/p&gt; El plugin debtags será deshabilitado. Se puede volver a habilitar a través del menú Packagesearch -&gt; Control de plugins &lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>NPlugin::RelatedPlugin</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="60"/>
        <source>Similar Plugin</source>
        <translation>Plugin de búsqueda por similitud</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="65"/>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="70"/>
        <source>Plugin for searching packages similar to another package.</source>
        <translation>Plugin de búsqueda de paquetes que son similares entre si.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="109"/>
        <source>Similar</source>
        <translation>Similares</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedplugin.cpp" line="188"/>
        <source>Searching for similar packages</source>
        <translation>Búsqueda de paquetes similares</translation>
    </message>
</context>
<context>
    <name>NTagModel::SelectedTagsView</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="41"/>
        <source>deselect a tag by double-clicking</source>
        <translation>Para anular la selección de una etiqueta, haga doble clic</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="42"/>
        <source>This list displays the tags currently searched for. To remove a tag double-click it.</source>
        <translation>Esta lista muestra las etiquetas en la búsqueda actual. Para eliminar una etiqueta , haga doble clic en ella.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="72"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectedtagsview.cpp" line="77"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>NTagModel::UnselectedTagsView</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="116"/>
        <source>select a tag by double-clicking</source>
        <translation>doble clic para seleccionar una etiqueta</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="117"/>
        <source>This list shows the tags that can be searched for. The tags are organised in a tree beneath their facets (groups of tags). To search for packages with a tag, double-click the tag. Multiple tags can be selected like this. Facets cannot be selected.</source>
        <translation>Esta lista muestra las etiquetas que se pueden buscar. Las etiquetas se organizan en un árbol debajo de sus facetas (Categorías). Para buscar paquetes con una etiqueta determinada, haga doble clic en la etiqueta. Puede seleccionr varias etiquetas de esta manera. Las Categorías no se pueden seleccionar.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="158"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="163"/>
        <source>Collapse all</source>
        <translation>Ocultar todo</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/unselectedtagsview.cpp" line="164"/>
        <source>Expand all</source>
        <translation>Expandir todo</translation>
    </message>
</context>
<context>
    <name>NWidgets::SelectionInputAndDisplay</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectioninputanddisplay.cpp" line="48"/>
        <source>filter string to filter tags</source>
        <translation>cadena para filtrar las etiquetas</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/selectioninputanddisplay.cpp" line="49"/>
        <source>Filters the tag names by the string in realtime.</source>
        <translation>Filtra los nombres de las etiquetas por la cadena en tiempo real.</translation>
    </message>
</context>
<context>
    <name>RelatedFeedbackWidget</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="13"/>
        <source>Form2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="25"/>
        <source>Search packages similar to</source>
        <translation>Búsqueda de los paquetes por similitud</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="40"/>
        <source>displays the package to search related packages for</source>
        <translation>muestra el paquete en la búsqueda de paquetes relacionados</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="43"/>
        <source>This displays the package for which you want to search related packages for. Enter the package under the &quot;Related&quot; section.</source>
        <translation>Aquí se muestra el paquete para el que desea buscar paquetes relacionados. Introduzca el paquete en la sección &quot;relacionados&quot;.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedfeedbackwidget.ui" line="53"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
</context>
<context>
    <name>RelatedInput</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="14"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="26"/>
        <source>Similar to package</source>
        <translation>Paquetes similares</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="51"/>
        <source>Result packages</source>
        <translation>Límite de resultados</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="58"/>
        <source>Number of result packages</source>
        <translation>Número de paquetes adevolver del resultado en la búsqueda por similitud</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="61"/>
        <source>Defines the number of the most similar packages which will be displayed.</source>
        <translation>Define el número total a mostrar de paquetes similares.</translation>
    </message>
    <message>
        <source>Maximum Distance:</source>
        <translation type="obsolete">Maximale Distanz:</translation>
    </message>
    <message>
        <source>Maximum number of tags the packages may differ</source>
        <translation type="obsolete">Maximale Anzahl an Tags in sich Pakete unterscheiden dürfen</translation>
    </message>
    <message>
        <source>This is the maximum number of tags in which the packages may differ from the given one. &lt;br&gt;
Or more formal: &lt;br&gt;
|(A union B) difference (A intersect B)| &amp;lt;= MaxDistance</source>
        <translation type="obsolete">Die Maximale Anzahl an Tags, in denen die Pakete von dem angegeben Paket abweichen dürfen.&lt;br&gt;Formal: &lt;br&gt;|(A vereinigt B) abzüglich (A geschnitten B)| &amp;lt;= MaxAnzahl</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="103"/>
        <source>Clear realated search</source>
        <translation>Borrar  la búsqueda por similitud</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/relatedinput.ui" line="106"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
</context>
<context>
    <name>TagChooserWidget</name>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="16"/>
        <source>Form1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="28"/>
        <source>Show packages with these tags</source>
        <translation>Ver paquetes con las siguientes etiquetas</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="35"/>
        <source>Check this to be able to exclude some tags</source>
        <translation>Marca esta casilla para  excluir algunas etiquetas</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="38"/>
        <source>Check this if you want to exclude some tags. If you check it, a list of tags will be shown. There you can select which to exclude.</source>
        <translation>Seleccione esta opción si desea excluir algunas etiquetas. Si la selecciona, la lista de etiquetas será muestrada. Luego usted puede seleccionar cualas excluir.</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="41"/>
        <source>Exclude Tags</source>
        <translation>Excluir Etiquetas</translation>
    </message>
    <message>
        <location filename="../src/plugins/debtagsplugin/tagchooserwidget.ui" line="48"/>
        <source>but not these tags</source>
        <translation>pero no estas etiquetas</translation>
    </message>
</context>
</TS>
