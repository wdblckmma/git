<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>ColumnControlDlg</name>
    <message>
        <location filename="../src/columncontroldlg.ui" line="13"/>
        <source>Control Columns</source>
        <translation>Spalten konfigurieren</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="74"/>
        <source>Shown</source>
        <translation>Eingeblendet</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="109"/>
        <source>&gt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="116"/>
        <source>&lt;&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="148"/>
        <source>Hidden</source>
        <translation>Ausgeblendet</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="195"/>
        <source>&amp;OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="198"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="205"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/columncontroldlg.ui" line="208"/>
        <source>Alt+C</source>
        <translation>Alt+A</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <location filename="../src/main.cpp" line="69"/>
        <source>Usage: packagesearch [options]
	-h, --help     prints this message
	-v, --version  prints the version information
If started without options the UI is launched
</source>
        <translation>Verwendung: packagesearch [Optionen]	-h, --help     gibt diesen Hilfetext aus	-v, --version  gibt die Version ausWird packagesearch ohne Option ausgeführt, dann wird die Nutzeroberfläche gestartet</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="112"/>
        <source>Unknown option </source>
        <translation>Unbekannte Option</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="118"/>
        <source>Debian Package Search - Version </source>
        <translation>Debian Paketsuche - Version </translation>
    </message>
</context>
<context>
    <name>NPackageSearch::PackageDisplayWidget</name>
    <message>
        <location filename="../src/packagedisplaywidget.cpp" line="56"/>
        <source>Customize Columns</source>
        <translation>Spalten anpassen</translation>
    </message>
    <message>
        <location filename="../src/packagedisplaywidget.cpp" line="212"/>
        <source>Hide Column</source>
        <translation>Spalte ausblenden</translation>
    </message>
    <message>
        <location filename="../src/packagedisplaywidget.cpp" line="290"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/packagedisplaywidget.cpp" line="298"/>
        <source>Score</source>
        <translation>Relevanz</translation>
    </message>
</context>
<context>
    <name>NPlugin::PackageNamePlugin</name>
    <message>
        <location filename="../src/packagenameplugin.cpp" line="23"/>
        <source>Package Name Plugin</source>
        <translation>Plugin für Paketnamen</translation>
    </message>
    <message>
        <location filename="../src/packagenameplugin.cpp" line="24"/>
        <location filename="../src/packagenameplugin.cpp" line="25"/>
        <source>Displays the package names</source>
        <translation>Zeigt die Namen der Pakete an</translation>
    </message>
    <message>
        <location filename="../src/packagenameplugin.h" line="63"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>NPlugin::PluginManager</name>
    <message>
        <location filename="../src/pluginmanager.cpp" line="232"/>
        <source>No translation for plugin </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/pluginmanager.cpp" line="233"/>
        <source> found for current locale </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NPlugin::ScoreDisplayPlugin</name>
    <message>
        <location filename="../src/scoredisplayplugin.cpp" line="30"/>
        <source>Score Display Plugin</source>
        <translation>Score-Wert-Anzeige-Plugin</translation>
    </message>
    <message>
        <location filename="../src/scoredisplayplugin.cpp" line="31"/>
        <source>Displays the package scores</source>
        <translation>Zeigt den Scorewert für die Pakete an</translation>
    </message>
    <message>
        <location filename="../src/scoredisplayplugin.cpp" line="32"/>
        <source>Displays the search scores of the packages returned by the current search.&lt;br&gt;
The higher the scores, the better the packages matches to the search.</source>
        <translation>Zeigt den Score-Wert für die Pakete aus der aktuellen Suche an.
Ein höherer Score-Wert bedeutet eine bessere Übereinstimmung des Paketes mit der Suche.</translation>
    </message>
    <message>
        <location filename="../src/scoredisplayplugin.h" line="77"/>
        <source>Score</source>
        <translation>Relevanz</translation>
    </message>
</context>
<context>
    <name>PackageSearch</name>
    <message>
        <location filename="../src/packagesearch.ui" line="14"/>
        <source>Debian Package Search</source>
        <translation>Debian Paketsuche</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="46"/>
        <source>&lt;b&gt;Search&lt;/b&gt;</source>
        <translation>&lt;b&gt;Suche&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="124"/>
        <source>Clear Search</source>
        <translation>Suche löschen</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="151"/>
        <source>Dummy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="202"/>
        <source>&lt;b&gt;Result&lt;/b&gt;</source>
        <translation>Suchergebnis</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="256"/>
        <source>&lt;font size=&quot;-1&quot;&gt;No search active&lt;/font&gt;</source>
        <translation>&lt;font size=&quot;-1&quot;&gt;Keine Suche aktiv&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="287"/>
        <source>Choose a package</source>
        <translation>Paket auswählen</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="290"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Nimbus Sans L; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Here you can choose to display information about a package, from all  packages available.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Nimbus Sans L; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Hier kann das Paket ausgewählt werden, für dass informaitonen angezeigt werden sollen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="319"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="329"/>
        <source>Forward</source>
        <translation>Vorwärts</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="342"/>
        <source>Details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="391"/>
        <source>&amp;Packagesearch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="407"/>
        <source>&amp;System</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="420"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="428"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="433"/>
        <source>Pa&amp;ckages</source>
        <translation>Pa&amp;kete</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="445"/>
        <source>&amp;Contents...</source>
        <translation>&amp;Inhalt...</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="448"/>
        <source>Contents</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="456"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="459"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="467"/>
        <location filename="../src/packagesearch.ui" line="470"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="475"/>
        <location filename="../src/packagesearch.ui" line="478"/>
        <source>Control Plugins</source>
        <translation>Plugins verwalten</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="481"/>
        <source>Control plugins</source>
        <translation>Plugins verwalten</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="484"/>
        <source>Enable and disable plugins</source>
        <translation>Aktivieren/deaktivieren von Plugins</translation>
    </message>
    <message>
        <location filename="../src/packagesearch.ui" line="489"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>PackageSearchAboutDlg</name>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="14"/>
        <source>Debian Package Search</source>
        <translation>Debian Paketsuche</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="26"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:18pt; font-weight:600;&quot;&gt;Debian Package Search&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Debian Paketsuche</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="40"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="52"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-weight:600;&quot;&gt;Debian Package Search&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Nimbus Sans L&apos;;&quot;&gt;&lt;br /&gt;(c) 2004-2012, Benjamin Mesing&lt;br /&gt;Email: &lt;/span&gt;&lt;a href=&quot;mailto:bensmail@gmx.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Nimbus Sans L&apos;; text-decoration: underline; color:#0000ff;&quot;&gt;bensmail@gmx.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-weight:600;&quot;&gt;Debian Paketsuche&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Nimbus Sans L&apos;;&quot;&gt;&lt;br /&gt;(c) 2004-2012, Benjamin Mesing&lt;br /&gt;Email: &lt;/span&gt;&lt;a href=&quot;mailto:bensmail@gmx.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Nimbus Sans L&apos;; text-decoration: underline; color:#0000ff;&quot;&gt;bensmail@gmx.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="102"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Enrico Zini&lt;/span&gt; for the wonderful Debtags System and his tips regarding this application. Enrico does also sponsor the updates to the debian server.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Frannoe &lt;/span&gt;for the Spanish translation and icon.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;Bug reporters&lt;/span&gt; who provided a lot of help tracking down the bugs: &lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Thomas Kabelmann&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ross Boylan&lt;/li&gt;&lt;/ul&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;p&gt;￼&lt;b&gt;Enrico Zini&lt;/b&gt; für das durchdachte Debtags System und seine Hinweise zu dieser Anwendung.&lt;/p&gt;￼
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Frannoe &lt;/span&gt;for the Spanish translation and icon.&lt;/p&gt;
Die &lt;b&gt;Fehler-Melder&lt;/b&gt; die viel Informationen geliefert haben um die ￼Fehler zu beheben&lt;ul&gt;￼&lt;li&gt;Thomas Kabelmann&lt;/li&gt;￼&lt;li&gt;Ross Boylan&lt;/li&gt;￼&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="149"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Nimbus Sans L&apos;;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="66"/>
        <source>Homepage: &lt;a href=http://packagesearch.sourceforge.net&gt;http://packagesearch.sourceforge.net&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="90"/>
        <source>Thanks To</source>
        <translation>Danksagung</translation>
    </message>
    <message>
        <source>&lt;p&gt;
&lt;b&gt;Enrico Zini&lt;/b&gt; for the wonderful Debtags System and 
his tips regarding this application. Enrico does also sponsor the updates to the debian server.&lt;br&gt;
&lt;/p&gt;
The &lt;b&gt;Bug reporters&lt;/b&gt; who provided a lot of help tracking down the bugs:
&lt;ul&gt;
&lt;li&gt;Thomas Kabelmann&lt;/li&gt;
&lt;li&gt;Ross Boylan&lt;/li&gt;
&lt;/ul&gt;</source>
        <translation type="obsolete">&lt;p&gt;￼&lt;b&gt;Enrico Zini&lt;/b&gt; für das durchdachte Debtags System und seine Hinweise zu dieser Anwendung.&lt;/p&gt;￼Die &lt;b&gt;Fehler-Melder&lt;/b&gt; die viel Informationen geliefert haben um die ￼Fehler zu beheben&lt;ul&gt;￼&lt;li&gt;Thomas Kabelmann&lt;/li&gt;￼&lt;li&gt;Ross Boylan&lt;/li&gt;￼&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="137"/>
        <source>Licence</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../src/packagesearchaboutdlg.ui" line="188"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PackageSearchImpl</name>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="125"/>
        <source>Displays the packages matching the search entered above.</source>
        <translation>Zeigt die Pakete an, die der oben angegebenen Suche entsprechen.</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="224"/>
        <source>Main Toolbar</source>
        <translation>Haupt-Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="265"/>
        <source>Error trying to open Xapian database</source>
        <translation>Fehler beim öffnen der Xapian-Datenbank</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="266"/>
        <source>&lt;p&gt;An error occurred trying to open the xapian database at /var/lib/apt-xapian-index/index please make sure the file is readable and valid. You may try to run update-apt-xapian-index to fix this problem. The orginal error message was:&lt;br&gt;&quot;</source>
        <translation>Ein Fehler ist beim Öffnen der Xapian Datenbank unter /var/lib/apt-xapian-index/index aufgetreten. Stellen Sie sicher das die Datei lesbar und gültig ist. Sie könne versuchen update-apt-xapian-index auszuführen um das Problem zu beheben, Die ursprüngliche Fehlermeldung war:&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="271"/>
        <source>Would you like to run update-apt-xapian-index now? Otherwise packagesearch will be terminated.</source>
        <translation>Möchten sie update-apt-xapian-index jetzt ausführen? Andernfalls wird packagesearch beendet.</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="345"/>
        <source>Unable to write settings</source>
        <translation>Konnte einstelungen nicht speichern</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="345"/>
        <source>Unable to write settings to</source>
        <translation>Konnte Einstellungen nicht speichern in </translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="707"/>
        <location filename="../src/packagesearchimpl.cpp" line="719"/>
        <source>Details</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="751"/>
        <source>Evaluating searches</source>
        <translation>Suche auswerten</translation>
    </message>
    <message>
        <location filename="../src/packagesearchimpl.cpp" line="1003"/>
        <source>Package Search Help Page</source>
        <translation>Debian Paketsuche - Hilfe</translation>
    </message>
</context>
<context>
    <name>PluginControl</name>
    <message>
        <location filename="../src/plugincontrol.ui" line="13"/>
        <source>PluginControl</source>
        <translation>Plugins verwalten</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="44"/>
        <source>Enabled</source>
        <translation>Aktiviert</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="49"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="54"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="59"/>
        <source>Location</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="../src/plugincontrol.ui" line="106"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../src/settingsdlg.ui" line="14"/>
        <source>Packagesearch Settings</source>
        <translation>Einstellungen - Debian Paketsuche</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="30"/>
        <source>Main Settings</source>
        <translation>Globale Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="36"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Nimbus Sans L&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The program selected here will be used to execute commands  as root when neccessary (e.g. when installing packages). &lt;tt&gt;su&lt;/tt&gt; is the default and available on every Debian system.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;tt&gt;sudo&lt;/tt&gt; requires the package sudo to be installed. If configured properly, &lt;tt&gt;sudo&lt;/tt&gt; might be more convenient, because you need to enter the password only once.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;￼&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body&gt;￼
Das hier ausgewählt Programm wird für die Erlangung von Root-Rechten verwendet, wenn dies erforderlich ist (z. B. für die Installation von Paketen). 

&lt;tt&gt;su&lt;/tt&gt; ist die Standardauswahl und ist auf jedem Debian-System verfügbar.&lt;/p&gt;￼
&lt;p&gt;&lt;tt&gt;sudo&lt;/tt&gt; erfordert die Installation des Paketes sudo. Wenn sudo entsprechend konfiguriert ist, ist es von der Verwendung bequemer, da das Passwort nicht so oft neu eingegeben werden muss.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="44"/>
        <source>Program used to become root</source>
        <translation>Anwendung zum Erlangen von Root-Rechten</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="50"/>
        <source>Use &quot;su&quot; to become root
</source>
        <translation>&quot;su&quot; verwenden um Root zu werden</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="54"/>
        <source>su</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="61"/>
        <source>Use &quot;sudo&quot; to become root</source>
        <translation>&quot;sudo&quot; verwenden um Root zu werden</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="64"/>
        <source>sudo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="76"/>
        <source>Enable/Disable Proxy Server</source>
        <translation>Ein-/Ausschlaten des Proxy Servers</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="79"/>
        <source>A proxy server might be used </source>
        <translation>Ein Proxy-Server kann verwendet werden</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="82"/>
        <source>Proxy Server</source>
        <translation>Proxy-Server</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="92"/>
        <source>  http://</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="102"/>
        <source>Proxy server to be used</source>
        <translation>Server der als Proxy-Server verwendet werden soll</translation>
    </message>
    <message>
        <location filename="../src/settingsdlg.ui" line="112"/>
        <source>Proxy Server Port</source>
        <translation>Port des Proxy-Servers</translation>
    </message>
</context>
</TS>
