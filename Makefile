#!/usr/bin/make -f

ICONDIR = /usr/share/pixmaps/packagesearch

RELEASE_ARGS := "CONFIG-=debug" "CONFIG+=release"
TEST_ARGS := "CONFIG+=test" "CONFIG+=debug"
MAKE_ARGS := "-j2"
QMAKE = /usr/bin/qmake
QMAKE_ARGS = 
LRELEASE = /usr/bin/lrelease
LRELEASE_ARGS = 

export QT_SELECT=qt5


all: delete-makefiles translations
	( cd src ; $(QMAKE) $(QMAKE_ARGS) $(RELEASE_ARGS) ; make $(MAKE_ARGS))
	( cd src/plugins ; $(QMAKE) $(QMAKE_ARGS) $(RELEASE_ARGS) ; make $(MAKE_ARGS))

translations:
	$(LRELEASE) $(LRELEASE_ARGS) src/packagesearch.pro
	$(LRELEASE) $(LRELEASE_ARGS) src/plugins/debtagsplugin/debtagsplugin.pro
	$(LRELEASE) $(LRELEASE_ARGS) src/plugins/aptplugin/aptplugin.pro
	$(LRELEASE) $(LRELEASE_ARGS) src/plugins/filenameplugin/filenameplugin.pro
	$(LRELEASE) $(LRELEASE_ARGS) src/plugins/orphanplugin/orphanplugin.pro
	$(LRELEASE) $(LRELEASE_ARGS) src/plugins/screenshotplugin/screenshotplugin.pro

delete-translations:
	rm -f translations/*.qm

clean-all: clean clean-debug clean-test delete-translations

clean: delete-makefiles delete-translations
	( cd src ; $(QMAKE) $(QMAKE_ARGS) $(RELEASE_ARGS) ; make clean )
	( cd src/plugins ; $(QMAKE) $(QMAKE_ARGS) $(RELEASE_ARGS) ; make clean )
	make delete-makefiles
	rm src/.qmake.stash

debug: delete-makefiles translations
	( cd src ; $(QMAKE) $(QMAKE_ARGS) ; make $(MAKE_ARGS))
	( cd src/plugins ; $(QMAKE) $(QMAKE_ARGS) ; make $(MAKE_ARGS))

clean-debug: delete-makefiles delete-translations
	( cd src ; $(QMAKE) $(QMAKE_ARGS) ; make clean )
	( cd src/plugins ; $(QMAKE) $(QMAKE_ARGS) ; make clean )
	make delete-makefiles


#install:
#	install -d /usr/lib/packagesearch
#	install -d /usr/share/doc/packagesearch
#	install -d /usr/local/bin
#	install -d $(ICONDIR)
#	install src/packagesearch /usr/local/bin/
#	install -m 644 icons/{packagesearch.png,install-package.png,remove-package.png,forward.png,back.png} $(ICONDIR)
#	install -m 644 README TODO CHANGELOG doc/content.html doc/COPYING.txt /usr/share/doc/packagesearch/
#	install src/plugins/lib*so /usr/lib/packagesearch
	
#uninstall:
#	rm -f /usr/local/bin/packagesearch
#	rm -f /usr/share/doc/packagesearch/{README,TODO,CHANGELOG,content.html,COPYING.txt}
#	rmdir /usr/share/doc/packagesearch
#	rm -f /usr/lib/packagesearch/libaptplugin.so
#	rm -f /usr/lib/packagesearch/libdebtagsplugin.so
#	rm -f /usr/lib/packagesearch/libfilenameplugin.so
#	rm -f /usr/lib/packagesearch/liborphanplugin.so
#	rmdir /usr/lib/packagesearch
#	rm -f  $(ICONDIR)/{packagesearch.png,install-package.png,remove-package.png,forward.png,back.png}
#	rmdir $(ICONDIR)

delete-makefiles:
	rm -f src/Makefile
	rm -f src/plugins/Makefile
	rm -f src/plugins/aptplugin/Makefile
	rm -f src/plugins/debtagsplugin/Makefile
	rm -f src/plugins/filenameplugin/Makefile
	rm -f src/plugins/orphanplugin/Makefile
	rm -f src/plugins/screenshotplugin/Makefile
	rm -f src/plugins/Makefile.test
	rm -f src/plugins/aptplugin-test/Makefile

test: build-test run-test

build-test: debug
	( cd src ; $(QMAKE) $(QMAKE_ARGS) "CONFIG+= testlib" ; make $(MAKE_ARGS) -f Makefile.testlib)
	( cd test-src ; $(QMAKE) $(QMAKE_ARGS) ; make $(MAKE_ARGS))
	( cd src/plugins ; $(QMAKE) $(QMAKE_ARGS)  plugins-test.pro ; make $(MAKE_ARGS) -f Makefile.test)

run-test: build-test
	( cd test-src ; export LD_LIBRARY_PATH=../src ; ./run-tests )
	( cd src ; export LD_LIBRARY_PATH=.:plugins ; plugins/aptplugin/run-tests )
	
clean-test:
	( cd src/plugins ; $(QMAKE) $(QMAKE_ARGS) "CONFIG+= testlib" ; make -f src/Makefile.testlib clean )
	make -f src/Makefile.test clean
	make -f src/plugins/Makefile.test clean
	rm -f src/Makefile.testlib
	rm -f src/Makefile.test

.PHONY : all debug clean clean-all clean-debug clean-test install uninstall delete-makefiles test build-test run-test translations delete-translations
