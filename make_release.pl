#!/usr/bin/perl -w
# creates the release, assuming build application

sub createSourceTarball();

print "Please enter a version string: ";
my $version = <STDIN>;
chomp($version);

print "Did you remember to run dch -v $version and commit the changes? (y/n)?\n";
my $decide = <STDIN>;
if ( $decide =~ /n/ )
{
	die "aborting";
}

my $headmodule = "packagesearch";	# toplevel module

my $releaseDir = "../packagesearch-release";


mkdir("$releaseDir");
my $src_filename = "packagesearch_$version.tar.gz";

createSourceTarball();



sub createSourceTarball() {
	my $targetSourceDir = "$releaseDir/$headmodule-$version";
	my $doCheckout = 1;
	if (-e $targetSourceDir ) {
		print "There is already a directory $targetSourceDir available, would you like to overwrite this?\n";
		my $decide = <STDIN>;
		if ( $decide =~ /n/ ) {
			$doCheckout = 0;
		}
		else {
			system("rm -rf $targetSourceDir");
		}
	}
	if ($doCheckout) {
		print "Cloning git version\n";
		(system("git clone ./ $targetSourceDir")==0)
			|| die("Could not clone git version.\n");
	}
	print "creating source tarball $src_filename \n";
	`rm -rf $targetSourceDir/.git`;
	(system("GZIP=-9 tar -C $releaseDir -czf $releaseDir/$src_filename $headmodule-$version\n")==0) || die("Could not create archive.\n");
	print "source tarball $src_filename created\n";

	
}
