//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_PACKAGENOTFOUNDEXCEPTION_H_2005_03_01
#define __NPLUGIN_PACKAGENOTFOUNDEXCEPTION_H_2005_03_01



#include <string>

#include "exception.h"

using namespace std;

namespace NPlugin {

/** @brief This exception is thrown if information for the requested package was not 
  * found in a plugin.
  * 
  * @author Benjamin Mesing
  */
class PackageNotFoundException : public NException::Exception
{
	/** The name of the package not found. */
 	string _name;
public:
	/** @param packageName the name of the package that was not found*/
	PackageNotFoundException(const string& packageName) :
		_name(packageName)
	{}
	virtual ~PackageNotFoundException();
	virtual string description() const
	{
		return "Package " + _name  + "not found\n";
	}
	/** Returns #_name. */
	const string& name()	{ return _name; }
};

};

#endif	//  __NPLUGIN_PACKAGENOTFOUNDEXCEPTION_H_2005_03_01
