#!/usr/bin/perl -w
# this script replaces a regexp by a string

sub replaceFunc;

replaceFunc();
exit 0;


sub replaceFunc
{
	foreach $filename (@ARGV)
	{
		print "Processing file $filename\n";
		open( FILE, $filename);
		@lines = <FILE>;
		close( FILE );
		`cp $filename $filename.0~`;
		open(FILE, ">$filename");	# overwrite the exiting file
		foreach $line (@lines)
		{
 			$line =~ s/Tagcoll::Tagcoll::/Tagcoll::/g;
#			$line =~ s/InputMerger<string>/Tagcoll::InputMerger<string>/g;
			print FILE $line;
#			print $line;
		}
		print "\n--------------\n\n";
	}
}



