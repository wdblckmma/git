//
// C++ Interface: globals
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __NPACKAGESEARCH_GLOBALS_H_2005_09_30
#define __NPACKAGESEARCH_GLOBALS_H_2005_09_30

#include <QString>

namespace NPackageSearch
{

const QString VERSION("2.7.10");

}
#endif	// __NPACKAGESEARCH_GLOBALS_H_2005_09_30
