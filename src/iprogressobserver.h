//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NUTIL_IPROGRESSOBSERVER_H_2005_02_22
#define __NUTIL_IPROGRESSOBSERVER_H_2005_02_22

#include <algorithm>

#include <qstring.h>

using namespace std;

namespace NUtil 
{

/**
@author Benjamin Mesing
*/
class IProgressObserver
{
	/** The lower bound of the current range. 
	  *
	  * <b>Default:</b> 0
	  * @invariant 0 &lt;= _lowerBound <= 100
	  * @invariant _lowerBound &lt;= _upperBound
	  */
	int _lowerBound;
	/** The lower bound of the current range. 
	  *
	  * <b>Default:</b> 100
	  * @invariant 0 &lt;= _lowerBound &lt;= 100
	  * @invariant _lowerBound &lt;= _upperBound
	  */
	int _upperBound;
protected:
	/** Sets the absolute progress of the loading state in %.
	  *
	  * @pre 0 &lt;= progress &lt;= 100
	  */
	virtual void setAbsoluteProgress(int progress)=0;
	
public:
	IProgressObserver() 
	{
		_lowerBound = 0;
		_upperBound = 100;
	};

	virtual ~IProgressObserver() {};
	/** Sets the progress range currently active.
	  *
	  * While a range is set each call of setProgress(int) will be interpreted as being in this
	  * range.
	  * 
	  * @param lower lower bound of the range
	  * @param upper upper bound of the range
	  * @param setToLower if this is true, the progress will be set to the lower bound
	  * @pre 0 &lt;= lower  
	  * @pre upper &lt;= 100 
	  * @pre lower &lt;= upper
	  * @see setProgress(int)
	  */
	void setProgressRange(int lower, int upper, bool setToLower = true)
	{ 
		_lowerBound = lower;
		_upperBound = upper;
		if (setToLower)
			setAbsoluteProgress(lower);
	};
	/** Returns #_lowerBound. */
	int lowerBound() const	{ return _lowerBound; };
	/** Returns #_lowerBound. */
	int upperBound() const	{ return _upperBound; };
	/** Sets the progress of the plugin.
	  *
	  * The progress will be interpreted as % of the current range. I.e. the real progress 
	  * will be computed by <tt>#_lowerBound + (#_upperBound - #_lowerBound) / 100 * progress</tt>
	  *
	  * @param progress progress of the reporting plugin in % should be in [0..100]
	  * if it is out of range it will be clamped
	  */
	void setProgress(int progress)
	{
		progress = max(0, progress);
		progress = min(100, progress);
		setAbsoluteProgress(_lowerBound + int(float(_upperBound - _lowerBound) / 100.0f * float(progress)));
	}
	/** Sets an arbitatry text to be displayed.
	  *
	  * @param text the text will be displayed, it can be used to describe what is 
	  * currently done
	  */
	virtual void setText(const QString& text)=0;
};

};

#endif //  __NUTIL_IPROGRESSOBSERVER_H_2005_02_22
