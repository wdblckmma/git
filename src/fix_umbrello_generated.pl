#!/usr/bin/perl -w
# this script fixes the include guards for umbrello generated files
# enter the files to process as input, a backup will be save under filename~

sub fixMacro;
sub fixVirtual;

fixVirtual();
fixMacro();
exit 0;


sub fixVirtual
{
	foreach $filename (@ARGV)
	{
		if ($filename =~ /\.h$/)	# if we have a header file
		{
			print "Processing file $filename\n";
			open( FILE, $filename);
			@lines = <FILE>;
			`cp $filename $filename.0~`;
			close( FILE );
			open(FILE, ">$filename");	# overwrite the exiting file
			foreach $line (@lines)
			{
				if ( $line !~ /^\s*virtual/)	# if the line doesn't declare a virtual function
				{
					$line =~ s/\t \*/\t  \*/;	# fix doxygen comments
					$line =~ s/(\w) \(/$1\(/;	# remove space before braces
					if ( $line =~ /(\s*)(.*)=\s*0(.*)/ )	# but we have an = 0 anywhere
					{
						$line =~ s/(\s*)(.*)/$1virtual $2/;	# insert a virtual before the first nonspace
						$line =~ s/  / /;	# remove double spaces as I don't like them
					}
				}
				print FILE $line;
#				print $line;
			}
			print "\n--------------\n\n";
		}
	}
}


sub fixMacro
{
	foreach $filename (@ARGV)
	{
		if ($filename =~ /\.h$/)	# if we have a header file
		{
			print "Processing file $filename\n";
			open( FILE, $filename);
			@lines = <FILE>;
			`cp $filename $filename.1~`;
			close( FILE );
			open(FILE, ">$filename");	# overwrite the exiting file
			$datestring = "2004_06_23";
			foreach $line (@lines)
			{
				if ( $line =~ /#/ )
				{
					if ($line =~ /#ifndef.*_H$/)
					{
						$line =~ s/(#ifndef\s*)(.*_H)$/$1__$2_$datestring/;
					}
					elsif ($line =~ /#define.*_H$/)
					{
						$line =~ s/(#define\s*)(.*_H)$/$1__$2_$datestring/;
					}
					elsif ($line =~ /#endif.*_H$/)
					{
						$line =~ s/(#endif)\s*\/\/(.*_H)$/$1\t\/\/ __$2_$datestring/;
					}
				}
				print FILE $line;
			}
			print "\n--------------\n\n";
		}
	}
}