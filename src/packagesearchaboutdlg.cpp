//
// C++ Implementation: packagesearchaboutdlg
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "packagesearchaboutdlg.h"

#include <iostream>

#include <QFile>

// NPackageSearch
#include "globals.h"

using namespace std;

namespace NPackageSearch
{

PackageSearchAboutDlg::PackageSearchAboutDlg()
{
	setupUi(this);
	_pTitleLabel->setText("<b style=\" font-size:18pt;\">Debian Package Search " + 
		VERSION + "</b>");
	QFile licenseFile("/usr/share/common-licenses/GPL");
	if (licenseFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QString licenseString(licenseFile.readAll());
		_pLicenceDisplay->setHtml("<html><body><pre>"+licenseString+"</pre></body></html>");
	}
	else
	{
		cerr << "Could not open /usr/share/common-licenses/GPL" << endl;
	}
}


PackageSearchAboutDlg::~PackageSearchAboutDlg()
{
}





}	// NPackageSearch
