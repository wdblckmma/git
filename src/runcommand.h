//
// C++ Interface: runcommand
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __RUNCOMMAND_H_2004_05_12
#define __RUNCOMMAND_H_2004_05_12

#include <qobject.h>

// NException
#include <exception.h>

#include "gainroot.h"

class QString;

namespace NApplication {

/** @brief Interface to execute a console application.
  *
  * After calling start the object will display the output of the started command to the user 
  * in an independant window. After exiting the signal processExited() will be emitted.\n
  * The command executed will consist of the list of arguments, where the first 
  * argument is the application itself. Arguments can be added via addArgument().
  * @author Benjamin Mesing
  */
class RunCommand : public QObject
{
	Q_OBJECT
	/** @brief Holds which tool to use for gaining root priviledges */
	static GainRoot s_gainRoot;
public:
	RunCommand();
	virtual ~RunCommand();
	/** @brief Adds a new argument to the command to be executed. 
	  *
	  * The first argument is the command to be executed.
	  */
	virtual void addArgument(const QString& arg)=0;
	/** @brief Execute the command, returning immidiately.
	  *
	  * The application will be launched and processed in the background. The control is 
	  * returned immidiately.
	  * @returns if the command could be executed
	  * @throws NException::RuntimeError if the command could not be executed
	  * for any reason
	  */
	virtual bool start()=0;
	/** @brief Execute the command asking for root rights, returning immidiately.
	  *
	  * The application will be launched and processed in the background. The control is 
	  * returned immon.hidiately. The user will have to authentificate,
	  * if he is not root already.
	  * @returns if the command could be launched
	  * @throws NException::RuntimeError if the command could not be executed
	  * for any reason
	  */
	virtual bool startAsRoot()=0;
	/** @returns true if the process exited successful, i.e. was not aborted and returned an
	  * exit state of 0. */
	virtual bool processExitedSuccessful() const=0;
	/** @brief Sets the title of the window displaying the command to be executed. 
	  *
	  * By default the title is the command to be executed with all its arguments.
	  */
	virtual void setTitle(const QString& title)=0;

   // Docu Taken from QT
	/** Blocks until the process has finished and the finished() signal has been emitted, 
	  * or until msecs milliseconds have passed.
	  * Returns true if the process finished; otherwise returns false (if the operation 
	  * timed out or if an error occurred). 
     */
	virtual bool waitForFinished(int msecs = 30000 ) = 0;
	
	/** @brief Returns true if the process has already finished, else false. */
	virtual bool finished() = 0;

	/** @brief Sets the command to be used to become root.
	  *
	  * Default is <tt>su</tt>.
	  */
	static GainRoot gainRootCommand();
	/** @brief Returns the command used to gain root rights. */
	static void setGainRootCommand(GainRoot gainRoot);


signals:
	/** @brief Emitted, when the command finished. */
	void processExited();
	/** This signal will be emitted, when RunCommand class is completly ready, this might happen long after
	  * the processExited signal was emitted (e.g. if the window has to be closed manually).\n 
	  * The quit signal is quaranteed to be emitted after the processExited() signal. 
	  * @note this signal is delete safe, i.e. you might delete the object as a reaction to it as the
	  * emitting function does not access its members after this. */
	void quit();
private:
	RunCommand(RunCommand& rc);
};

};

#endif	//  __RUNCOMMAND_H_2004_05_12
