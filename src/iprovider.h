#ifndef __PLUGINMANAGER_H_2004_06_21
#define __PLUGINMANAGER_H_2004_06_21

#include <QMenu>
#include <QMainWindow>
#include <qstring.h>

#include <string>
#include <set>

class QNetworkAccessManager;
class QPushButton;
class QStatusBar;
class QWidget;

namespace Xapian {
	class Database;
}

namespace ept 
{
	namespace apt {
		class Apt;
	}
	namespace debtags {
		class Vocabulary;
	}
}

namespace Xapian 
{
	class Database;
}

using namespace std;

namespace NUtil
{
	class IProgressObserver;
}

namespace NPlugin
{

class IPluginUser;
class PluginContainer;
class Plugin;

/** @brief This is the interface which provides informations for the plugins.
  *
  * 
  */
class IProvider
{
public:
	virtual ~IProvider() {}

	/** @brief Returns the path were the icons are located. */
	virtual QString iconDir() const = 0;
	/**
	  * @brief Disables the whole search window.
	  * 
	  * This might be used if all updating should be locked.
	  * @param enabled 
	  */
	virtual void setEnabled (bool enabled) = 0;
	/** Returns the currently active package. */
	virtual QString currentPackage() const = 0;
	/** @brief Creates a button which can be used as to clear the input of a line.
	  *
	  * The button will only have an icon which looks like this:
	  *
	  <pre>
	  +------
	  |      \
	  |  \/   \
	  |  /\   /
	  |      /
	  +------
	 </pre>
	  */
	virtual QPushButton* createClearButton(QWidget* pParent = 0, const char* name = 0) const = 0;
	/**
	  * @brief Reports an error to the user.
	  * @param title a title for the error
	  * @param message a description of what went wrong
	  */
	virtual void reportError (const QString& title, const QString& message) = 0;
	/**
	  * @brief Reports a warning to the user.
	  * @param title a title for the warning
	  * @param message a description of what happened
	  */
	virtual void reportWarning(const QString& title, const QString& message) = 0;
	/** @brief This can be used by the plugins to report that they are currently busy.
	  *
	  * It must be followed by a report ready.
	  * @param pPlugin the plugin which report busy
	  * @param message a short description of the activity (it can be used to display
	  * whats currently going on, e.g. using the status bar).
	  */
	virtual void reportBusy(Plugin* pPlugin, const QString& message) = 0;
	/** This can be used by a busy plugins to report that it is ready.
	  * It must be procceeded by a report ready. */
	virtual void reportReady(Plugin* pPlugin) = 0;
	/** @brief Returns a list of all packages available. 
	  * 
	  * Only real packages will be listed here i.e. no virtuals, or missing dependencies.
	  */
	virtual const set<string>& packages() const = 0;
	/** Returns a pointer to the main window for the plugins. */
	virtual QMainWindow* mainWindow() = 0;
	/** Returns a pointer to the status bar. */
	virtual QStatusBar* statusBar() = 0;
	/** Allows access to the loader interface to report back loading progress. */
	virtual NUtil::IProgressObserver* progressObserver() const = 0;
	
	/** Returns a reference to the apt object. */
	virtual const ept::apt::Apt& apt() const = 0;
	/** Returns a reference to the debtags vocabulary object. */
	virtual const ept::debtags::Vocabulary& vocabulary() const = 0;
	/** Returns a reference to the apt-xapian database. */
	virtual const Xapian::Database& xapian() const = 0;

	/** @brief Returns the central network manager which can be used to access the network. */
	virtual QNetworkAccessManager* network() = 0;
	
	/** @brief Reloads the package database. */
	virtual void reloadAptFrontCache() = 0;
};

}	// namespace NPlugin

#endif //	__PLUGINMANAGER_H_2004_06_21

