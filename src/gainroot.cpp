//
// C++ Implementation: gainroot
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "gainroot.h"

namespace NApplication
{

QString GainRootPaths::su = "/bin/su";
QString GainRootPaths::sudo = "/usr/bin/sudo";

}	// namespace NApplication
