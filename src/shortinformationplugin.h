//
// C++ Interface: shortinformationplugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __SHORTINFORMATIONPLUGIN_H_2004_07_08
#define __SHORTINFORMATIONPLUGIN_H_2004_07_08

#include <string>

#include <plugin.h>

using namespace std;

namespace NPlugin {

/** @brief This offers short information about packages.
  *
  * The type of information offered is shortly described with the shortInformationCaption() 
  * function.\n
  * Classes which implement this interface should take care, that the shortInformationText(const string&) 
  * function should be as efficient as possible (e.g. using caching of information).
  * @author Benjamin Mesing
  */
class ShortInformationPlugin : virtual public Plugin
{
public:
	ShortInformationPlugin() {};
	virtual ~ShortInformationPlugin() {};
	/**
	  * @returns a short information for the package. This will be shown in the listing 
	  * of the SearchWindows result view.
	  * @param package the package to show information for
	  */
	virtual const QString shortInformationText(const string& package) = 0;
	/**
	  * @returns the caption for the section off the short information
	  */
	virtual QString shortInformationCaption() const = 0;
	/** @brief Returns the priority of this short information plugin. 
	  * 
	  * Lower values mean that the plugins output will be shown most visible (e.g. on top). */
	virtual uint shortInformationPriority() const = 0;
	/** @brief Returns the preferred width for the column displaying the short information. 
	  *
	  * If nothing is specified -1 will be returned.
	  * @returns the preferred width in number of characters
	  */
	virtual int preferredColumnWidth() const { return -1; };
};


/** @brief This function object can be used to compare the caption of ShortInformationPlugin 
  * with QString objects.
  * 
  * Returns true if they are equal.
  */
class ShortInformationCaptionEquals
{
	const QString _caption;
public:
	ShortInformationCaptionEquals(const QString caption) :
		_caption(caption)
	{}
	bool operator() (const ShortInformationPlugin* pPlugin)
	{
		return pPlugin->shortInformationCaption() == _caption;
	}
};

};

#endif	// __SHORTINFORMATIONPLUGIN_H_2004_07_08
