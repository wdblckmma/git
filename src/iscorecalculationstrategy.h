//
// C++ Interface: iscorecalculationstrategy
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_ISCORECALCULATIONSTRATEGY_H_2005_07_30
#define __NPLUGIN_ISCORECALCULATIONSTRATEGY_H_2005_07_30

#include <set>
#include <map>

using namespace std;

namespace NPlugin {

/** @brief This class provides the interface to calculate scores for plugins.
  *
  * Using the score strategy, you must call calculateScore() first and afterwards
  * you can get the result with getScore(package) or getScore.
  *
  * This is intended to be used by the ScorePlugins to realize score 
  * calculation. However ScorePlugins are not required to use this interface.
  * 
  * Note that you would probably want to inherit ScoreCalculationStrategyBase
  * instead of inheriting this interface directly.
  * @see ScoreCalculationStrategyBase
  * @author Benjamin Mesing
  */
class IScoreCalculationStrategy
{
public:
	IScoreCalculationStrategy() {};
	virtual ~IScoreCalculationStrategy() {};
	/** @brief Calculates the scores for the handed set of packages.
	  *
	  * Old calculations will be cleared.
	  */
	virtual void calculateScore(const set<string>& packages) = 0;
	/** @brief Returns the score for the handed package.
	  *
	  * @pre the score for this package must have been calculated
	  */
	virtual float getScore(const string& package) const = 0;
	/** @brief Returns a map, mapping the packageId to the scores calculated.
	  *
	  * @see calculateScore()
	  */
	virtual const map<string, float>& getScore() const = 0;
	/** @brief Clears the scores calculated. */
	virtual void clear() = 0;
};

}

#endif	// __NPLUGIN_ISCORECALCULATIONSTRATEGY_H_2005_07_30
