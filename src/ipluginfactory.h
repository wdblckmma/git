//
// C++ Interface: ipluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __IPLUGINFACTORY_H_2004_09_09
#define __IPLUGINFACTORY_H_2004_09_09

#include <string>

using namespace std;

namespace NPlugin 
{

class Plugin;

/** @brief This class can be used to create plugins by name.
  * @author Benjamin Mesing
  */
class IPluginFactory
{
public:
	IPluginFactory() {};
	virtual ~IPluginFactory() {};
	/** @brief Use this function to create a plugin for the given name.
	  * 
	  * @returns the plugin created, 0 if no plugin type is known for the given name. 
	  */
	virtual Plugin* createPlugin(const string& name) const = 0;
};

};

#endif	//  __IPLUGINFACTORY_H_2004_09_09
