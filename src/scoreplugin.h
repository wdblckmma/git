//
// C++ Interface: scoreplugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGINSCOREPLUGIN_H_
#define __NPLUGINSCOREPLUGIN_H_

#include <map>
#include <set>

// NPlugin
#include <plugin.h>

using namespace std;

namespace NPlugin {

/** @brief Plugin that offers scores for packages.
  *
  * The scores calculated can either depend on an active search (e.g. 
  * scores depending on the fulltext search pattern), or can be 
  * totally independent of such data (e.g. popcon scores).
  *
  * @author Benjamin Mesing
  */
class ScorePlugin : virtual public Plugin
{
public:
	virtual ~ScorePlugin() {};
	/** @brief Returns the score of the handed packages for the currently active search.
	  *
	  * The scores are in [0..1] where 0.0f means the lowest and 1.0f the highest score.
	  * The scores are always calculated for a whole set of packages, because scores
	  * may be relative to each other (i.e. there is no absolute criterion to score package
	  * but we can say that package1 matches better than package2)
	  *
	  * @pre offersScore() == true
	  * @post returnedValue.keys() == packages<br>
	  * i.e. all the handed <tt>packages</tt> must be contained in the returned map
	  * @returns a map mapping each package to its scores
	  * @see offersScore()
	  */
	virtual map<string, float> getScore(const set<string>& packages) const = 0;
	/** @brief Returns if the plugin is currently offering scores.
	  * 
	  * This could return <tt>false</tt> e.g. if scores are only calculated for an active search 
	  * (like e.g. a fulltext search), but no search is currently active.
	  * @see getScore()
	  */
	virtual bool offersScore() const = 0;
};

}

#endif	// 
