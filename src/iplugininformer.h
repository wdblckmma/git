//
// C++ Interface: iplugininformer
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __IPLUGININFORMER_H_2004_09_08
#define __IPLUGININFORMER_H_2004_09_08

namespace NPlugin 
{

class IPluginUser;
class Plugin;

/** @brief Interface to manage and inform plugin users.
  *
  * All added plugin users will be informed about added or removed plugins
  * when the informAddPlugin() / informRemovePlugin() function is called.
  * @author Benjamin Mesing
  */
class IPluginInformer
{
public:
	IPluginInformer() {};
 	virtual ~IPluginInformer() {};
	/** @brief Adds a plugin user. Plugin users will be informed about loading
	  * and unloading of plugins.
	  * 
	  * Each plugin user can be added only once, if it is added a second time, nothing will happen.
	  * @param pUser the user to add
	  * @see removePluginUser()
	  */
	virtual void addPluginUser(IPluginUser* pUser) = 0;
	/** @brief Removes the handed plugin user.
	  * 
	  * If it is not in the list of users nothing will happen.
	  * 
	  * @param pUser the user to remove
	  * @see addPluginUser()
	  */
	virtual void removePluginUser(IPluginUser* pUser) = 0;
//	virtual void informAddPlugin(Plugin* pPlugin) = 0;
//	virtual void informRemovePlugin(Plugin* pPlugin) = 0;
};

};

#endif	//  __IPLUGININFORMER_H_2004_09_08
