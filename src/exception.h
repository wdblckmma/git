//
// C++ Interface: exception
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __EXCEPTION_H_2004_06_15
#define __EXCEPTION_H_2004_06_15

#include <string>

using namespace std;

namespace NException {

/** Base class for custom exceptions.
  * 
  * @author Benjamin Mesing
  */
class Exception{
public:
	Exception();
	virtual ~Exception();
	/** @returns a string descibing the problem. */
	virtual string description() const =0;
};

/** Class that stores the error message in a simple string.
  * 
  * @author Benjamin Mesing
  */
class SimpleString : virtual public Exception
{
	string _description;
public:
	/** Create an exception containing a simple string. */
	SimpleString(const string& description)	{ _description = description; }
	virtual string description() const	{ return _description; }
};

/** @brief Errors which result from wrong code - which are not caused by user interaction. */
class ProgrammerException : virtual public SimpleString
{
public:
	ProgrammerException(const string& description) : SimpleString(description)	{}
};

/** @brief Errors which result from user interaction. */
class RuntimeException : virtual public SimpleString
{
public:
	RuntimeException(const string& description) : SimpleString(description)	{}
};


};

#endif	//  __EXCEPTION_H_2004_06_15
