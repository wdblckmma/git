//
// C++ Interface: runcommandwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __RUNCOMMANDWIDGET_H_2005_08_28
#define __RUNCOMMANDWIDGET_H_2005_08_28

#include <qdialog.h>
#include <ui_runcommandwidget.h>

/**
@author Benjamin Mesing
*/
class RunCommandWidget : public QDialog, public Ui::RunCommandWidget
{
Q_OBJECT
public:
	RunCommandWidget(QWidget *parent = 0, const char *name = 0);
	~RunCommandWidget();
	void replaceLastLine( const QString line );
	void append( const QString line );
public slots:
	void onClose();
signals:
	void quit();

};

#endif	// __RUNCOMMANDWIDGET_H_2005_08_28
