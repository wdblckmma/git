//
// C++ Implementation: applicationfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "applicationfactory.h"

#ifdef __USE_KDE_LIBS
#include "runcommandinwindow.h"
#else
#include "runcommandinxterm.h"
#endif


namespace NApplication {

ApplicationFactory* ApplicationFactory::_pFactory = new ApplicationFactory();

ApplicationFactory::ApplicationFactory()
{
}


ApplicationFactory::~ApplicationFactory()
{
}

RunCommand* ApplicationFactory::getRunCommand(const QString& name)
{
#ifdef __USE_KDE_LIBS
	return new RunCommandInWindow(0, name);
#else
	return new RunCommandInXterm();
#endif
}

ApplicationFactory* ApplicationFactory::getInstance() 
{
	return _pFactory;
}

void ApplicationFactory::setInstance(ApplicationFactory* pFactory)
{
	_pFactory = pFactory;
}


};
