//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __NPLUGIN_PACKAGENAMEPLUGIN_H_2005_03_13
#define __NPLUGIN_PACKAGENAMEPLUGIN_H_2005_03_13

#include <string>

#include <QObject>

#include "shortinformationplugin.h"

using namespace std;


namespace NPlugin {

/** This plugin is simply for a unified handling showing the package names.
  *
  * @author Benjamin Mesing
  */
class PackageNamePlugin : public QObject, public ShortInformationPlugin
{
	const QString _title;
	const QString _briefDescription;
	const QString _description;
public:
	const static QString PLUGIN_NAME;
	PackageNamePlugin();
	~PackageNamePlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider* ) {};
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const { return _title; };
	virtual QString briefDescription() const { return _briefDescription; };
	virtual QString description() const { return _description; };
	//@}

	/** @name ShortInformationPlugin interface
	  * 
	  * Implementation of the ShortInformationPlugin interface
	  */
	//@{
	virtual uint shortInformationPriority() const	{ return 0; }
	virtual const QString shortInformationText(const string& package);
	/** @brief Returns <b>Name</b> */
	virtual QString shortInformationCaption() const { return tr("Name"); };
	// documented in base class
	virtual int preferredColumnWidth() const { return 20; }
	//@}

};

};

#endif	//  __NPLUGIN_PACKAGENAMEPLUGIN_H_2005_03_13
