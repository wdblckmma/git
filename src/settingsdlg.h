//
// C++ Interface: pluginsettingsdlg
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __PLUGINSETTINGSDLG_H_2004_08_31
#define __PLUGINSETTINGSDLG_H_2004_08_31

#include <QDialog>
#include <QNetworkProxy>

#include <networksettings.h>

#include "ui_settingsdlg.h"

class QTabWidget;

namespace NPlugin {
	class PluginContainer;
}

namespace NPackageSearch 
{


/** @brief Class which controls different plugin containers.
  *
  * @todo Rename to "SettingsDlg"
  * @author Benjamin Mesing
  */
class SettingsDlg : public QDialog, public Ui::SettingsDlg
{
Q_OBJECT
public:
	/** Initialises the dialog. 
	 *
	 * Initially the settings from the proxy are applied.
	 */
	SettingsDlg(const NUtil::NetworkSettings& network, QWidget *parent = 0, const char *name = 0);
	~SettingsDlg();
	/** @brief Adds a plugin to show settings for. */
	void addPlugin(NPlugin::PluginContainer* pPluginContainer);
	/** @brief Applies the proxy settings configured within this GUI to the QNetworkProxy configuration. 
	 *
	 * Returns the proxy configured through this dialog.
	 */
	NUtil::NetworkSettings networkSettings();
public Q_SLOTS:
	void on__pProxyServerEnabledCheck_stateChanged(int state);
	
};

};

#endif	// __PLUGINSETTINGSDLG_H_2004_08_31
