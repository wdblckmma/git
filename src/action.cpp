//
// Author: Benjamin Mesing <bensmail@gmx.net>
//
// Copyright: See COPYING file that comes with this distribution
//
//
// This file was generated on Wed Aug 31 2005 at 10:43:44
//

#include <action.h>

namespace NPlugin
{

Action::Action(QAction* pAction, bool packageAction, QString menu, QString toolBar)
{
	_pAction = pAction;
	_packageAction = packageAction;
	_menu = menu;
	_toolBar = toolBar;
}

}
