//
// C++ Implementation: pluginsettingsdlg
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <QButtonGroup>
#include <qlabel.h>
#include <qlayout.h>
#include <QNetworkProxy>
#include <qtabwidget.h>
#include <qpushbutton.h>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "settingsdlg.h"
#include "helpers.h"

#include "plugincontainer.h"


namespace NPackageSearch
{

SettingsDlg::SettingsDlg(const NUtil::NetworkSettings& network, QWidget *parent, const char *name)
 : QDialog(parent)
{
	setObjectName(name);
	setupUi(this);
	_pProxyTypeGroup->setId(_pNoProxyButton, NUtil::NetworkSettings::NO_PROXY);
	_pProxyTypeGroup->setId(_pSystemProxyButton, NUtil::NetworkSettings::SYSTEM_PROXY);
	_pProxyTypeGroup->setId(_pCustomProxyButton, NUtil::NetworkSettings::CUSTOM_PROXY);
	switch (network.proxyType()) {
		case NUtil::NetworkSettings::NO_PROXY:
			_pNoProxyButton->setChecked(true);
			break;
		case NUtil::NetworkSettings::SYSTEM_PROXY:
			_pSystemProxyButton->setChecked(true);
			break;
		case NUtil::NetworkSettings::CUSTOM_PROXY:
			_pCustomProxyButton->setChecked(true);
			break;
	}
	_pProxyServerName->setText(toQString(network.host()));
	_pProxyServerPort->setValue(network.port() != -1 ? network.port() : 0);
}


SettingsDlg::~SettingsDlg()
{
}

void SettingsDlg::addPlugin(NPlugin::PluginContainer* pPluginContainer)
{
	QWidget* pSettingsWidget = pPluginContainer->getSettingsWidget(this);
	if (pSettingsWidget != 0)
	{
		_pSettingsTabWidget->addTab(pSettingsWidget, pPluginContainer->title());
	}
}

void SettingsDlg::on__pProxyServerEnabledCheck_stateChanged(int state) 
{
	bool proxyInputEnabled = (state == Qt::Checked);
	_pProxyProtocolLabel->setEnabled(proxyInputEnabled);
	_pProxyServerName->setEnabled(proxyInputEnabled);
	_pProxyServerPort->setEnabled(proxyInputEnabled);
}


NUtil::NetworkSettings SettingsDlg::networkSettings()
{
	QString serverName = _pProxyServerName->text();
	return NUtil::NetworkSettings((NUtil::NetworkSettings::ProxyType) _pProxyTypeGroup->checkedId(), 
											serverName.toStdString(), _pProxyServerPort->value());
}


};
