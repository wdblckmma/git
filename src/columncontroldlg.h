//
// C++ Interface: columncontrol
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __COLUMNCONTROLDLG_H_2004_08_24
#define __COLUMNCONTROLDLG_H_2004_08_24

#include "ui_columncontroldlg.h"
#include <QDialog>

/**
Implementing Ui::ColumnControl


@author Benjamin Mesing
*/
class ColumnControlDlg : public QDialog, Ui::ColumnControlDlg
{
	Q_OBJECT
public:
	ColumnControlDlg(QWidget* parent=0);

	~ColumnControlDlg();
	void setContent(const QStringList & shown, const QStringList & hidden);
	QStringList shownColumns();
	QStringList hiddenColumns();
protected slots:
	void on__pHideButton_clicked();
	void on__pShowButton_clicked();
};

#endif	// __COLUMNCONTROLDLG_H_2004_08_24
