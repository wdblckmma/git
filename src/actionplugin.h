//
// Author: Benjamin Mesing <bensmail@gmx.net>
//
// Copyright: See COPYING file that comes with this distribution
//
//
// This file was generated on Wed Aug 31 2005 at 10:43:44
//

#ifndef __NPLUGIN_ACTIONPLUGIN_H_2005_08_31
#define __NPLUGIN_ACTIONPLUGIN_H_2005_08_31

#include <vector>

// NPlugin
#include "plugin.h"
#include "action.h"

using namespace std;

namespace NPlugin 
{ 

class ActionPlugin : public Plugin 
{
public:
	/** @brief Returns the Actions offered by this plugin. */
	virtual vector<Action*> actions() const = 0;
};

} 
#endif // __NPLUGIN_ACTIONPLUGIN_H_2005_08_31

