#ifndef __INFORMATIONPLUGIN_H_2004_06_21
#define __INFORMATIONPLUGIN_H_2004_06_21

#include <string>

#include <qstring.h>

class QWidget;

#include "plugin.h"

using namespace std;

namespace NPlugin
{

/** @brief Interface for plugins, providing information about packages.
  *
  * An information plugin can provide short information about packages 
  * in a single string, which will be shown in a common widget accumulating
  * such information from all plugins. It can also provide extended information
  * by providing a separate widget where information can be displayed.
  *
  * For plugins providing short information, offersInformationText() must 
  * return true, and informationText() must return the information string 
  * for the package. 
  *
  * To provide information in a separate widget, the plugin
  * must create a widget, return a pointer to it on 
  * informationWidget(), and provide the title for the widget through
  * informationWidgetTitle(). The widget must be updated with new
  * package information on updateInformationWidget().
  *
  * @author Benjamin Mesing
  */
class InformationPlugin : virtual public Plugin 
{
public:
	/** Returns a widget for displaying information about the package.
	  *
	  * @returns a widget where the information offered by this plugin is shown, or 
	  * 0 if no information widget is provided by this plugin.
	  * 
	  * @see informationWidgetTitle()
	  */
	virtual QWidget* informationWidget() const = 0;
	/** @brief Returns the title of the information widget.
	  * @note Can only be called, if (informationWidget() != 0)
	  * @see informationWidget()
	  */
	virtual QString informationWidgetTitle() const = 0;
	/** @brief Updates the information widget to display the information about the 
	  * given package.
	  *
	  * @param package the name of the package to display information for.
	  */
	virtual void updateInformationWidget(const string& package) = 0;
	/** @brief Clears the content of the information widget.  */
	virtual void clearInformationWidget() = 0;
	/** @brief Returns if this plugin offers information which should be shown in
	  * the details section of the search windows result view.
	  */
	virtual bool offersInformationText() const = 0;
	/** @brief Returns an information text for the given package
	  *
	  * @param package the name of the package for which information shall be returned.
	  * @returns the information text for the given package
	  * @see offersInformationText()
	  */
	virtual QString informationText (const string& package) = 0;
	/** @brief Returns the priority of this information plugin. 
	  * 
	  * Lower values mean that the plugins output will be shown most visible (e.g. on top). */
	virtual uint informationPriority() const = 0;
};


}	// namespace NPlugin

#endif //	__INFORMATIONPLUGIN_H_2004_06_21

