//
// C++ Interface: scoredisplayplugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_SCOREDISPLAYPLUGIN_H_2005_07_19
#define __NPLUGIN_SCOREDISPLAYPLUGIN_H_2005_07_19

#include <string>
#include <map>
#include <set>

#include <QObject>

// NPlugin
#include <ipluginuser.h>
#include <plugin.h>
#include <scoreplugin.h>
#include <shortinformationplugin.h>


using namespace std;

namespace NPlugin {

/** @brief This plugin is used to display the search scores packages in the search result.
  *
  * It holds a reference to the PackageSearchImpl and uses the 
  * PackageSearchImpl#getScorePlugins() function.
  * Though this breaks the clean encapsulation a little, I preferred this to factoring out 
  * this single method to a new interface.
  * @author Benjamin Mesing
  */
class ScoreDisplayPlugin : public QObject, public ShortInformationPlugin, public IPluginUser
{
	const QString _title;
	const QString _briefDescription;
	const QString _description;
	typedef map<string, float> ScoreMap;
	typedef set<ScorePlugin*> ScorePluginContainer;
	/** The score plugins used to calculate the scores. */
	ScorePluginContainer _scorePlugins;
	/** @brief Maps the package to the scores. */
	ScoreMap _packageToScore;
public:
	ScoreDisplayPlugin();
	~ScoreDisplayPlugin();
	const static QString PLUGIN_NAME;
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider*) {};
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const { return _title; };
	virtual QString briefDescription() const { return _briefDescription; };
	virtual QString description() const { return _description; };
	//@}

	/** @name ShortInformationPlugin interface
	  * 
	  * Implementation of the ShortInformationPlugin interface
	  */
	//@{
	virtual uint shortInformationPriority() const	{ return 0; }
	virtual const QString shortInformationText(const string& package);
	/** @brief Returns <b>Score</b> */
	virtual QString shortInformationCaption() const { return tr("Score"); };
	virtual int preferredColumnWidth() const { return 5; }
	//@}
	/** @brief Called whenever the scores of the package have changed.
	  *
	  * @param resultPackages the packages returned by the current search
	  */
	void updateScores(const set<string>& resultPackages);

	/** @name IPluginUser interface */
	//@{
	/** @brief Tests if the handed plugin is a ScorePlugin and in case adds it to the #_scorePlugins. */
	virtual void addPlugin(Plugin* pPlugin);
	/** @brief Removes the plugin from the #_scorePlugins.
	  *
	  * Does nothing if the plugin is not in.
	  */
	virtual void removePlugin(Plugin* pPlugin);

};

}

#endif	// __NPLUGIN_SCOREDISPLAYPLUGIN_H_2005_07_19
