//
// C++ Interface: plugininformer
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __PLUGININFORMER_H_2004_09_08
#define __PLUGININFORMER_H_2004_09_08

#include <set>

#include "iplugininformer.h"

using namespace std;

namespace NPlugin 
{

class IPluginUser;
class Plugin;

/** @brief Used to manage and inform plugin users.
  *
  * Do not have too many observers as it has linear complexity in
  * the operation.
  * @author Benjamin Mesing
  */
class PluginInformer : public IPluginInformer
{
	typedef set<IPluginUser*> PluginUserContainer;
	PluginUserContainer _users;
public:
	PluginInformer();
	~PluginInformer();
	// documented in base class
	virtual void addPluginUser(IPluginUser* pUser)	{ _users.insert(pUser); };
	// documented in base class
	virtual void removePluginUser(IPluginUser* pUser);
	virtual void informAddPlugin(Plugin* pPlugin);
	virtual void informRemovePlugin(Plugin* pPlugin);
};

};

#endif	// __PLUGININFORMER_H_2004_09_08
