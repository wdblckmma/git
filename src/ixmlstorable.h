//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NXML_IXMLSTORABLE_H_2005_04_23
#define __NXML_IXMLSTORABLE_H_2005_04_23

#include <qdom.h>


namespace NXml {

class XmlData;

/** @brief Implement this interface if your class can be stored in an XML file
  * 
  * The saveSettings(NXml::XmlData& outData, QDomElement parent) function is used 
  * to store the data of the implementing object. For this purpose it should 
  * create exactly one node as child of <i>parent</i> whereunder the data is added.
  * If the object does not need to save any settings, it is also possible to
  * add no node. The loadSettings(QDomElement source)
  * routine is called when loading with the node created in saveSettings() as 
  * <i>source</i>. If no node was create by saveSettings(), the loadSettings() 
  * function is called nevertheless - with <i>source</i> being an arbitary element.
  * Therefor the loadSettings() function should perform a test if the handed node
  * did actually is the expected one.
  *
  * A class <tt>Container</tt> that handles IXmlStorable objects could be implemented like this:
@code
void Container::saveSettings()
{
	NXml::XmlData xmlData("container");
	QDomElement root = xmlData.root();
	
	_pFirstStorable->saveSettings(xmlData, root);
	_pSecondStorable->saveSettings(xmlData, root);
	...
	if ( !xmlData.writeFile(_settingsFilename) )
		cerr << "Unable to write settings to "<< _settingsFilename <<endl;
}

void Container::loadSettings()
{
	NXml::XmlData xmlData;
	if (!xmlData.loadFile(_settingsFilename))
		return;
	QDomElement root = xmlData.root();
	QDomElement element = NXml::getFirstElement(xmlData.root().firstChild());
	element = _pFirstStorable->loadSettings(element);
	element = _pSecondStorable->loadSettings(element);
	...
}
@endcode  
  * Where the implementation of a class implementing IXmlStorable could look like this:
@code
class MyStorable : public IXmlStorable
{
	virtual void saveSettings(NXml::XmlData& outData, QDomElement parent)
	{
		// nothing to save?
		if (_settings1.isEmpty())
			return;
		QDomElement myStorable = outData.addElement(parent, "myStorable");
		outData.addAttribute(pluginManager, _settings1, "_settings1");
	};
	virtual QDomElement loadSettings(QDomElement source)
	{
		// noting was saved 
		if (source.tagName() != "myStorable")
			return source;
		NXml::getAttribute(source, _settings1, "settings1", "default");
		return NXml::getNextElement(source);
	}
@endcode  
  
  * @author Benjamin Mesing
  */
class IXmlStorable
{
public:
	IXmlStorable() {};
	virtual ~IXmlStorable() {};
	/** @brief Save the data for this object into the given XML tree.
	  *
	  * You should create <b>exactly</b> one new node as child of <i>parent</i>
	  * When loading the XML file later this node will be handed as the <i>source</i> 
	  * parameter to the loadSettings() function. You can also add no node here,
	  * where the loadSettings routine should return immidiately.
	  * 
	  * @param outData XML Document which owns parent
	  * @param parent the parent where to add the settings
	  */
	virtual void saveSettings(NXml::XmlData& outData, QDomElement parent) const = 0;
	/** @brief Loads the settings for this object from inData.
	  * 
	  * @param source the element where the information is stored
	  * @returns the next sibling element of <i>source</i> or <i>source</i> if 
	  * <i>source</i> did not contain information for this object.
	  */
	virtual QDomElement loadSettings(QDomElement source) = 0;
};

};

#endif	// __NXML_IXMLSTORABLE_H_2005_04_23
