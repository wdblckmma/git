//
// C++ Implementation: scorecalculationstrategybase
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "scorecalculationstrategybase.h"

#include <QDebug>

namespace NPlugin {

ScoreCalculationStrategyBase::ScoreCalculationStrategyBase()
 : NPlugin::IScoreCalculationStrategy()
{
}


ScoreCalculationStrategyBase::~ScoreCalculationStrategyBase()
{
}

/////////////////////////////////////////////////////
// IScoreCalculationStrategy Interface
/////////////////////////////////////////////////////


float ScoreCalculationStrategyBase::getScore(const string& package) const
{
	map<string, float>::const_iterator it = _packagesToScore.find(package);
	if (it == _packagesToScore.end()) {
		qWarning("No scores for package %s", package.c_str());
		return 1;
	}
	return it->second;
}

const map<string, float>& ScoreCalculationStrategyBase::getScore() const
{
	return _packagesToScore;
}

void ScoreCalculationStrategyBase::clear()
{ 
	_packagesToScore.clear();
};



}
