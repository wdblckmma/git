//
// C++ Implementation: runcommand
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "runcommand.h"

namespace NApplication {

GainRoot RunCommand::s_gainRoot = SU;

RunCommand::RunCommand()
{
}


RunCommand::~RunCommand()
{
}


GainRoot RunCommand::gainRootCommand()
{
	return s_gainRoot;
}

void RunCommand::setGainRootCommand(GainRoot gainRoot)
{
	s_gainRoot = gainRoot;
}

};
