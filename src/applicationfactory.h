//
// C++ Interface: applicationfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __APPLICATIONFACTORY_H_2004_05_13
#define __APPLICATIONFACTORY_H_2004_05_13

#include <qstring.h>

#include "gainroot.h"

namespace NApplication {

class RunCommand;

/** @brief Factory class responsible for creating run commands.
  *
  * This class is a Singleton, though it allows subclasses to be defined.
  *
  * By default the factory will create RunCommandInXterm instances.
  * @author Benjamin Mesing
  */
class ApplicationFactory
{
	static ApplicationFactory* _pFactory;
protected:
	ApplicationFactory();
public:
	virtual ~ApplicationFactory();
	
	/** @brief Returns the global application factory. */
	static ApplicationFactory* getInstance();
	/** @brief Sets the global application factory. */
	static void setInstance(ApplicationFactory* pFactory);
	
	/** @brief Returns an instance of RunCommand, created by new.
	  *
	  * The exact type of this instance depends on the implentation of this
	  * function, so it is determinded at compile time. It is your responsibility
	  * to delete the command created by this function.
	  * @param name this is used to give the created instance a name,
	  * if naming is supported by the implementing RunCommand. This name is
	  * only internal (e.g. a QObject name).
	  */
	virtual RunCommand* getRunCommand(const QString& name);
};

};

#endif	//  __APPLICATIONFACTORY_H_2004_05_13
