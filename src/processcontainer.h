//
// C++ Interface: processcontainer.h
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) %year%
//
// Copyright: See COPYING file that comes with this distribution
//
//
// This file was generated on Tue Jun 14 2005 at 09:09:53
//

#ifndef __NAPPLICATION_PROCESSCONTAINER_H_2005_06_14
#define __NAPPLICATION_PROCESSCONTAINER_H_2005_06_14

#include <set>

using namespace std;

#include <qmutex.h>
#include <QProcess>


namespace NApplication { 
/**
 * @brief This class can be used to keep track of different processes running.
 * 
 * A new process is added with the launch command. As soon as the process terminates it will
 * be removed from the controlled processes and the processExited(QProcess*) signal will be emitted.
 */
class ProcessContainer : public QObject
{
	Q_OBJECT
	typedef set<QProcess*> ProcessContainerType;
	/** @brief The processes controlled by this Container. */
	ProcessContainerType _processes;
	/** @brief Mutex enforce start() and onProcessExited() to be self and mutal exclusive. 
	  *
	  * Note that the mutex is recursive.
	  */
	QMutex _mutex;
public:
	ProcessContainer() : _mutex(QMutex::Recursive) {}
	/**
	 * @brief Launches the handed process and adds it to the list of controlled processes.
	 * 
	 * If the process could not be launched, it won't be added to the controlled processes.
	 * The process will be removed from the controlled ones, as soon as it exits and the 
	 * processExited(QProcess*, int) signal will be emitted.
	 * @returns if the process was launched successfully (timeout value 3000ms)
	 */
	bool start(QProcess* pProcess, QString command, QStringList arguments);
protected Q_SLOTS:
	/** @brief Called on the exit of any of the given processes.
	  *
	  * It detemines the process that exited and emits the processExited(QProcess*) signal.
	  */
	void onProcessExited();
signals:
	/**
	 * @brief Emitted whenever a process has exited.
	 * 
	 * @param pProcess a pointer to the process that exited
	 */
	void processExited(QProcess* pProcess);
};


} // namespace NApplication

#endif // __NAPPLICATION_PROCESSCONTAINER_H_2005_06_14

