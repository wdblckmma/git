//
// C++ Interface: plugincompare
//
// Description: 
// I had to remove the operators from the interface files, because I got
// multiple definition linker errors with the functions in the interface 
// files. (I think it has something to do with the Q_OBJECT macro present 
// in both files which triggered the linker error (plugin.h and searchplugin.h)
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __PLUGINCOMPARE_H_2004_08_12
#define __PLUGINCOMPARE_H_2004_08_12

#include "searchplugin.h"
#include "informationplugin.h"
#include "shortinformationplugin.h"

namespace NPlugin 
{

/** @brief Compares the plugins by priority, or if the priority is equal compares the 
  * adresses of the objects. 
  *
  * Ensures: \f$(pPlugin1 < pPlugin2) \wedge (pPlugin2 < pPlugin1) \Rightarrow (pPlugin1 == pPlugin2)\f$
  */
bool operator<(const SearchPlugin& plugin1, const SearchPlugin& plugin2)
{
	if (plugin1.searchPriority() == plugin2.searchPriority())
		return &plugin1 < &plugin2;
	else
		return plugin1.searchPriority() < plugin2.searchPriority();
}

/** @brief Compares the plugins by priority, or if the priority is equal compares the 
  * adresses of the objects. 
  *
  * Ensures: \f$(pPlugin1 < pPlugin2) \wedge (pPlugin2 < pPlugin1) \Rightarrow (pPlugin1 == pPlugin2)\f$
  */
bool operator<(const InformationPlugin& plugin1, const InformationPlugin& plugin2)
{
	if (plugin1.informationPriority() == plugin2.informationPriority())
		return &plugin1 < &plugin2;
	else
		return plugin1.informationPriority() < plugin2.informationPriority();
}

/** @brief Compares the plugins by priority, or if the priority is equal compares the 
  * adresses of the objects. 
  *
  * Ensures: \f$(pPlugin1 < pPlugin2) \wedge (pPlugin2 < pPlugin1) \Rightarrow (pPlugin1 == pPlugin2)\f$
  */
bool operator<(const ShortInformationPlugin& plugin1, const ShortInformationPlugin& plugin2)
{
	if (plugin1.shortInformationPriority() == plugin2.shortInformationPriority())
		return &plugin1 < &plugin2;
	else
		return plugin1.shortInformationPriority() < plugin2.shortInformationPriority();
}



}


#endif	// __PLUGINCOMPARE_H_2004_08_12
