//
// C++ Interface: ipluginuser
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __IPLUGINUSER_H_2004_08_11
#define __IPLUGINUSER_H_2004_08_11

namespace NPlugin 
{

class Plugin;

/** @brief This interface must be implemented if the class wants to register
  * itself at the pluginmanager to be informed about changes in the loaded plugin.
  *
  * @author Benjamin Mesing
  */
class IPluginUser{
public:
	IPluginUser() {};
	virtual ~IPluginUser() {};
	/** This method is called by the PluginManager whenever a new plugin is loaded. */
	virtual void addPlugin(Plugin* pPlugin) = 0;
	/** This method is called by the PluginManager whenever a plugin is removed. */
	virtual void removePlugin(Plugin* pPlugin) = 0;
};

};

#endif	// __IPLUGINUSER_H_2004_08_11
