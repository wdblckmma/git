//
// C++ Interface: singleton
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __SINGLETON_H_2004_07_03
#define __SINGLETON_H_2004_07_03

/** @brief This class implements the singleton pattern for classes. 
  * 
  * The class to be managed must be handled as template argument. It must offer a 
  * default constructor which is used to create single instance. The Singleton
  * will be a child class of the managed class.
  *
  * @author Benjamin Mesing
  */
template <class Base>
class Singleton : public Base
{
	static Singleton<Base>* _pInstance;
public:
	/** This gets the instance managed by the singleton. */
 	static Singleton<Base>* instance() 
	{
		if (_pInstance == 0)
			_pInstance = new Singleton<Base>;
		return _pInstance;
	}
	/** This can be used to get the instance managed by the singleton. It assumes the instance
	  * to be allready created
	  * @pre the instance must have been created (i.e. instance must have been called) */
	static Singleton<Base>* uncheckedInstance() 
	{
		assert (_pInstance != 0);
		return _pInstance;
	}
protected:
	 Singleton() {};
};

#endif	// __SINGLETON_H_2004_07_03
