#!/usr/bin/perl -w

@names = <>;	# reads the entries to convert
chomp(@names);
foreach (@names)
{
	next if (!/^\s*(\w+)\s*_?(\w+)\s*;\s*$/);
	my ($type, $identifier) = /(\w+)\s*_?(\w+)/;
# 	$identifier=lc($_);
#	print "\t\tstringToEntry[\"$identifier\"]=".uc($_).";\n";
# 	$name = ucfirst($identifier);
	
	print ("virtual $type $identifier() const;\n");
}
