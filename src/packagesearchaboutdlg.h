//
// C++ Interface: packagesearchaboutdlg
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPACKAGESEARCH_PACKAGESEARCHABOUTDLG_H_2005_09_30
#define __NPACKAGESEARCH_PACKAGESEARCHABOUTDLG_H_2005_09_30

#include <QDialog>

#include <ui_packagesearchaboutdlg.h>


namespace NPackageSearch
{

/**
@author Benjamin Mesing
*/
class PackageSearchAboutDlg : public QDialog, public Ui::PackageSearchAboutDlg
{
public:
	PackageSearchAboutDlg();
	~PackageSearchAboutDlg();
};

}	// namespace NPackageSearch

#endif	// __NPACKAGESEARCH_PACKAGESEARCHABOUTDLG_H_2005_09_30
