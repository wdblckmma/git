#!/bin/bash

export LANG=de_DE.UTF-8
export LC_ALL=de_DE.UTF-8
export LC_MESSAGES=de_DE.UTF-8
export LC_CTYPE=de_DE.UTF-8
export LC_MONETARY=de_DE.UTF-8
export LC_TIME=de_DE.UTF-8
export LC_NUMERIC=de_DE.UTF-8
./packagesearch $@
