//
// C++ Interface: gainroot
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __GAINROOT_20080719
#define __GAINROOT_20080719

#include <QString>

namespace NApplication
{

enum GainRoot 
{
	SU,
	SUDO
};

struct GainRootPaths 
{
	static QString su;
	static QString sudo;
};


}	// namespace NApplication

#endif // __GAINROOT_20080719
