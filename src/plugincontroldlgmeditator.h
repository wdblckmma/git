//
// C++ Interface: plugincontroldlgmeditator
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_PLUGINCONTROLDLGMEDITATOR_H_2007_12_27
#define __NPLUGIN_PLUGINCONTROLDLGMEDITATOR_H_2007_12_27

#include <QObject>

class QWidget;
class QTableWidget;

namespace NPlugin {

class PluginManager;


/** @brief This class is responsible for displaying the dialog control dialog and
  * handling events caused by the dialog (e.g. unselecting a plugin by a user).
  *
  * Hand the PluginManager to the constructor and call displayDialog to show the
  * plugin control dialog. This will show the plugin to enable and disable plugins.
  *
  * @author Benjamin Mesing <bensmail@gmx.net>
  */
class PluginControlDlgMeditator : public QObject
{
	Q_OBJECT
	PluginManager* _pPluginManager;
	/** @brief This points to the list displaying the plugins. 
	  *
	  * While the plugin control dialog is not shown, this is set to 0.
	  */
	QTableWidget* _pPluginDisplay;
// 	typedef PluginManager::PluginData PluginData;
public:
	PluginControlDlgMeditator(PluginManager* pPluginManager);
	~PluginControlDlgMeditator();
	void displayDialog(QWidget* pParent);
	/** Returns the name of the plugin at the given row. 
	  *
	  * @pre _pPluginDisplay != 0
	  */
	QString pluginName(int row);
	/** Returns the directory of the plugin at the given row.
	  *
	  * @pre _pPluginDisplay != 0
	  */
	QString pluginDirectory(int row);
private slots:
	/** This should be called only when a plugin was selected or deselected. */
	void onCellChanged(int row, int column);
	
	

};

}

#endif // __NPLUGIN_PLUGINCONTROLDLGMEDITATOR_H_2007_12_27
