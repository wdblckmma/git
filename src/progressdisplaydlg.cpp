//
// C++ Implementation: %{MODULE}
//
// Description:
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "progressdisplaydlg.h"

#include <QProgressBar>
#include <qapplication.h>
#include <qlabel.h>



namespace NUtil {

ProgressDisplayDlg::ProgressDisplayDlg(QWidget *parent, const char *name, bool modal)
 : QProgressDialog(parent), IProgressObserver()
{
	setObjectName(name);
	setModal(modal);
}


ProgressDisplayDlg::~ProgressDisplayDlg()
{
}

/////////////////////////////////////////////////////
// IProgressObserver Interface
/////////////////////////////////////////////////////

void ProgressDisplayDlg::setAbsoluteProgress(int progress)
{
	setValue(progress);
	qApp->processEvents();
}

void ProgressDisplayDlg::setText(const QString& text)
{
	setLabelText(text);
	qApp->processEvents();
}



};
