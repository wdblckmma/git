//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NUTILPROGRESSDISPLAYDLG_H_2005_03_20
#define __NUTILPROGRESSDISPLAYDLG_H_2005_03_20

#include <QProgressDialog>

#include "iprogressobserver.h"

namespace NUtil 
{

/**
@author Benjamin Mesing
*/
class ProgressDisplayDlg : public QProgressDialog, public IProgressObserver
{
Q_OBJECT
public:
	ProgressDisplayDlg(QWidget *parent = 0, const char *name = 0, bool modal = false);
	virtual ~ProgressDisplayDlg();
	
	/** @name IProgressObserver interface
	  *
	  * These functions implement the IProgressObserver interface.
	  */
	//@{	
	// documented in base class 
	virtual void setAbsoluteProgress(int progress);
	// documented in base class 
	virtual void setText(const QString& text);
	//@}
};

};

#endif	//  __NUTILPROGRESSDISPLAYDLG_H_2005_03_20
