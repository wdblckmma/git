//
// C++ Implementation: singleton
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "singleton.h"

template <class Base>
Singleton<Base>* Singleton<Base>::_pInstance = 0;
