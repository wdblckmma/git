//
// C++ Implementation: runcommandwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "runcommandwidget.h"

RunCommandWidget::RunCommandWidget(QWidget *parent, const char *name)
 : QDialog(parent)
{
	if (name)
		setObjectName(name);
	setupUi(this);
}


RunCommandWidget::~RunCommandWidget()
{
}



void RunCommandWidget::onClose()
{
	close();
	emit quit();
}


void RunCommandWidget::replaceLastLine( const QString line )
{
	// change the last line
	QString text = _pConsoleOutput->text();
	int lastLine = text.lastIndexOf("\n");
	if (lastLine != -1)
	{
		text.truncate(lastLine+1);
		text.append(line);
	}
	else
		text = line;
	_pConsoleOutput->setText(text);
/*	_pOutput->_pConsoleOutput->removeParagraph(_pOutput->_pConsoleOutput->paragraphs()-1);
	_pOutput->_pConsoleOutput->append(line);*/

}


void RunCommandWidget::append( const QString line )
{
	_pConsoleOutput->append(line);
}

