//
// C++ Interface: runcommandinwindow
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef __USE_KDE_LIBS

#ifndef __RUNCOMMANDINWINDOW_H_2004_05_07
#define __RUNCOMMANDINWINDOW_H_2004_05_07

#include <q3textedit.h>
#include <qtimer.h>

#include <iostream>

#include "runcommand.h"
#include "runcommandwidget.h"

using namespace std;

class QProcess;

namespace NApplication
{

/** @brief This class encapsulates running an external program and displays its output in a textwindow.
  * 
  * It is similar to a console except yet without input.
  *
  * @note This class assumes that only one \\r occurs per input flush.
  *
  * 
  * @author Benjamin Mesing
  */
class RunCommandInWindow : public RunCommand
{
	Q_OBJECT
	RunCommandWidget* _pOutput;
	/** The process that will be run. */
	QProcess* _pProcess;
	/** Holds the content of the last line. */
	QString _lastLine;
	QStringList _arguments;
	QString _command;
	
	/** Holds the paragraph number of the last line. */
// 	int _lastLinePar;
private slots:
	void onProcessExited();
	/** This function closes the widget and emits the quit() signal. */
	virtual void onCommandWindowClosed();
protected slots:
	/** Handles new output on the command line. */
	virtual void onReadyReadStdout();
	/** This is called whenever the user choose to abort the command. */
	void onAbort();
public:
	RunCommandInWindow(QObject* parent=0, const char* name=0);
	~RunCommandInWindow();
	virtual bool start();
	virtual void addArgument(const QString& s);
	virtual bool processExitedSuccessful() const;
};

}	// namespace NApplication



#endif	//  __RUNCOMMANDINWINDOW_H_2004_05_07

#endif	// __USE_KDE_LIBS
