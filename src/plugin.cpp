//
// C++ Implementation: plugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "plugin.h"

#include <xmldata.h>

namespace NPlugin
{

const QString Plugin::_emptyString;

QDomElement Plugin::loadSettings(const QDomElement source)
{
	return source;
}

void Plugin::saveSettings(NXml::XmlData&, QDomElement) const
{
}


}

