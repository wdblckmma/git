//
// C++ Implementation: %{MODULE}
//
// Description:
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "packagenameplugin.h"

#include "helpers.h"

namespace NPlugin 
{

const QString PackageNamePlugin::PLUGIN_NAME = "PackageNamePlugin";

PackageNamePlugin::PackageNamePlugin()
 : ShortInformationPlugin(),
 	_title(tr("Package Name Plugin")),
	_briefDescription(tr("Displays the package names")),
	_description(tr("Displays the package names"))
{
}


PackageNamePlugin::~PackageNamePlugin()
{
}


/////////////////////////////////////////////////////
// Short Information Plugin Interface
/////////////////////////////////////////////////////

const QString PackageNamePlugin::shortInformationText(const string& package)
{
	return toQString(package);
}

};
