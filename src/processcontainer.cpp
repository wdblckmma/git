//
// C++ Implementation: processcontainer.cpp
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
// This file was generated on Tue Jun 14 2005 at 09:09:53
//

#include "processcontainer.h"

namespace NApplication 
{ 

bool ProcessContainer::start(QProcess* pProcess, QString command, QStringList arguments)
{
	_mutex.lock();
	pProcess->start(command, arguments);
	bool started = pProcess->waitForStarted();
	connect(pProcess, SIGNAL(finished(int)), this, SLOT(onProcessExited()));
	if (started)
	{
		_processes.insert(pProcess);
	}
	else
	{
		disconnect(pProcess, SIGNAL(finished(int)), this, SLOT(onProcessExited()));
	}
	_mutex.unlock();
	return started;
}
	


void ProcessContainer::onProcessExited()
{
	_mutex.lock();
	// Each process not running should be a finished process.
	for (ProcessContainerType::iterator it = _processes.begin(); it != _processes.end(); ++it)
	{
		if ( (*it)->state() == QProcess::NotRunning )
		{
			// call the erase first because emitting processExited might trigger a 
			// re-add of the plugin
			_processes.erase(it);
			emit(processExited(*it));
			_mutex.unlock();
			return;
		}
	}
	_mutex.unlock();
}


}	// namespace NApplication
