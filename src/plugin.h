#ifndef __PLUGIN_H_2004_06_21
#define __PLUGIN_H_2004_06_21

#include <string>

#include "ixmlstorable.h"

namespace NPlugin
{

class IProvider;

/**
  * @author Benjamin Mesing
  */
class Plugin : NXml::IXmlStorable
{
protected:
	/** This offers an empty string for its children. */
	static const QString _emptyString;
public:
	virtual ~Plugin() {};
	/**
	  * 
	  * @param pProvider provides information needed by the plugin
	  */
	virtual void init(IProvider* pProvider) = 0;
	/**
	  * Enables/Disables all widgets that belong to this plugin.
	  * @param enabled 
	  */
	virtual void setEnabled(bool enabled) = 0;
	/**
	  * Shows or hides the given plugin.
	  * @param visible 
	  */
	virtual void setVisible(bool visible) = 0;
	
	/** @brief Returns the name of the plugin, this should be unique in the whole application.
	  *
	  * This name should be used in the IPluginFactory to create the plugins for 
	  * the container.
	  *
	  * You will probably want code like this:
	  * @code
class MyPlugin : SearchPlugin	// SearchPlugin inherits Plugin
{
public:
	static const QString PLUGIN_NAME = "MyPlugin";	// note that you can't initialize this way in C++
	virtual QString name() const { return PLUGIN_NAME; }
	[...]
};
	  * @endcode
	  * It is probably sufficient to make the name unique in the plugin container it belongs
	  * to, but I'm not sure about this.
	  */
	virtual QString name() const = 0;

	/** @brief Returns the title of the plugin, this is how it will be shown in configuration
	  * dialogs or similar.
	  * 
	  */
	virtual QString title() const = 0;
	/** @returns a brief description for this plugin. */
	virtual QString briefDescription() const = 0;
	/** @returns a description for this plugin. */
	virtual QString description() const = 0;
	/** @brief Loads the settings from the element node.
	  *
	  * Does nothing in its default implementation. 
	  * @param source data used as source for loading
	  */
	virtual QDomElement loadSettings(const QDomElement source);
	/** @brief Save the settings from this plugin container into the given XML tree
	  * 
	  * Does nothing in its default implementation.\n
	  * If settings are to be saved they must contain of a single tree with a 
	  * QDomElement in the root which must be added under parent.
	  * @param parent the parent under which to add the settings
	  * @param outData XML Document which owns parent
	  */
	virtual void saveSettings(NXml::XmlData& outData, QDomElement parent) const;

};



}	// namespace NPlugin

#endif	// __PLUGIN_H_2004_06_21

