//
// C++ Interface: runcommandinxterm
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __RUNCOMMANDINXTERM_H_2004_05_12
#define __RUNCOMMANDINXTERM_H_2004_05_12

#include <list>

#include <QStringList>

#include "extalgorithm.h"
#include "runcommand.h"


using namespace std;

class QProcess;

namespace NApplication 
{

/** This class runs a command in an XTerm. The XTerm is closes as soon as the started program is active.
  * @author Benjamin Mesing
  */
class RunCommandInXterm : public RunCommand
{
	Q_OBJECT
	QStringList _arguments;
	/** @brief The process currently executed. 0 if no process is currently executed.  */
	QProcess* _pProcess;
	/** @brief The title of the window where the output is shown. */
	QString _title;
private slots:
	void onProcessExited();
protected:
	/** @brief This is a helper function implementing start() and startAsRoot() 
	  *
	  * It does what is described in either start() or startAsRoot().
	  * @param root if true, the command will be started as root
	  * @see start(), startAsRoot()
	  */
	bool start(bool root);
public:
	RunCommandInXterm();
	~RunCommandInXterm();
	virtual void addArgument(const QString& arg);
	virtual bool start();
	virtual bool startAsRoot();
	/** @brief This returns always true as I do not see a way to get hold of the exit status using an xterm. */
	virtual bool processExitedSuccessful() const	{ return true; };
	virtual void setTitle(const QString& title);
	/** @see RunCommand::waitForFinished()
	  * @pre a process must be started
	  */
	virtual bool waitForFinished(int msecs = 30000 );
	virtual bool finished();
};

};	// namespace NApplication

#endif	//  __RUNCOMMANDINXTERM_H_2004_05_12
