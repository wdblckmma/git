//
// C++ Interface: screenshotpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGINSCREENSHOTPLUGINFACTORY_H_2010_08_05
#define __NPLUGINSCREENSHOTPLUGINFACTORY_H_2010_08_05

#include <ipluginfactory.h>

namespace NPlugin 
{

class ScreenshotPluginContainer;

/** @brief This creates the screenshot plugins.
  *
  * 
  * @author Benjamin Mesing
  */
class ScreenshotPluginFactory : public IPluginFactory
{
	ScreenshotPluginContainer* _pContainer;
	ScreenshotPluginFactory();
	static ScreenshotPluginFactory* _pInstance;
public:
	~ScreenshotPluginFactory();
	void setContainer(ScreenshotPluginContainer* pContainer) { _pContainer = pContainer; };
	/** @name IPluginFactory interface */
	//@{
	/** @brief This creates a plugin for the given name.
	  *
	  * Accepted names are: 
	  * \li "ScreenshotPlugin"
	  *
	  * @pre setContainer() must have been called or 0 will be returned.
	  */
	virtual Plugin* createPlugin(const string& name) const;
	//@}
	static ScreenshotPluginFactory* getInstance();
};

};

#endif	//  __NPLUGINSCREENSHOTPLUGINFACTORY_H_
