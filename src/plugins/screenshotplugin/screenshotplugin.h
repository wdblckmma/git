#ifndef __SCREENSHOTPLUGIN_H_2010_08_05
#define __SCREENSHOTPLUGIN_H_2010_08_05

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QString>

#include <string>

#include <informationplugin.h>
#include <screenshotplugincontainer.h>

using namespace std;

class QLabel;
class QScrollArea;

namespace NPlugin
{

/** This plugin offers displaying of screenshots.
  *
  * @author Benjamin Mesing
  */
class ScreenshotPlugin : public QObject, public InformationPlugin 
{
	Q_OBJECT

	/** @brief This holds a link to the manager which manages the plugin.
	  *
	  * Acquaintance relation */
	IProvider* _pProvider;
	/** The container which holds this plugin. */
	const ScreenshotPluginContainer& _container;
	QLabel* _pScreenshotWidget;
	QScrollArea* _pScrollArea;
	/** @brief Holds the networt reply for the current screenshot-download or 0 if no download
	  * is active. 
	  */
	QNetworkReply *_pReply;
public:
	static const QString PLUGIN_NAME;
	/** Create this plugin as plugin in container.
	  *
	  * @param container the container which contains this plugin. 
	  */
	ScreenshotPlugin(const ScreenshotPluginContainer& container);;
	virtual ~ScreenshotPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface 
	  */
	//@{
	/** @brief Initializes the plugin. 
	  */
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const;
	virtual QString briefDescription() const;
	virtual QString description() const;
	//@}
	//@{
	/** @name InformationPlugin interface
	  * 
	  * Implementation of the InformationPlugin interface
	  */
	//@{
	virtual uint informationPriority() const	{ return 5; };
	/** This plugin does not offer a separate information widget. */
	virtual QWidget* informationWidget() const;
	/** This plugin does not offer a separate information widget. */
	virtual QString informationWidgetTitle() const	{ return "Screenshot"; };
	/** @brief Triggers downloading of a screenshot for the given package
	  *
	  * Displays screenshot as soon as it is available.
	  */
	virtual void updateInformationWidget(const string& package);
	/** This plugin does not offer a separate information widget. */
	virtual void clearInformationWidget() {};
	/** This plugin offers an information text. */
	virtual bool offersInformationText() const	{ return false; };
	/** Returns a string which lists the tags for the requested package. */
	virtual QString informationText (const string& ) { return ""; };
	//@}
	
private Q_SLOTS:
	/** @brief Aborts the active download of the screenshot. 
	  *
	  * Does nothing, if no download is running (i.e. _pReply == 0).
	  */
	void abortDownload();
	void httpFinished();
	void httpDownloadProgress(qint64 bytesReceived, qint64 bytesTotal );
	void httpError(QNetworkReply::NetworkError);
	
};

}	// namespace NPlugin

#endif //	__SCREENSHOTPLUGIN_H_2010_08_05

