//
// C++ Implementation: screenshotpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "screenshotpluginfactory.h"

#include "screenshotplugin.h"

namespace NPlugin {

ScreenshotPluginFactory* ScreenshotPluginFactory::_pInstance = 0;

ScreenshotPluginFactory::ScreenshotPluginFactory()
{
}


ScreenshotPluginFactory::~ScreenshotPluginFactory()
{
}

Plugin* ScreenshotPluginFactory::createPlugin(const string& name) const
{
	if (name=="ScreenshotPlugin")
	{
		return new ScreenshotPlugin(*_pContainer);
	}
	return 0;
}

ScreenshotPluginFactory* ScreenshotPluginFactory::getInstance()
{
	if (_pInstance == 0)
		_pInstance = new ScreenshotPluginFactory;
	return _pInstance;
}



};
