#include <cassert>

#include <qmessagebox.h>
#include <qaction.h>
#include <QMainWindow>

#include "screenshotplugincontainer.h"

// NUtil
#include "helpers.h"

// NApplication
#include "applicationfactory.h"
#include "runcommand.h"

// NPlugin
#include <iprovider.h>
#include <plugincontainer.h>
#include <iprogressobserver.h>
#include "screenshotplugin.h"
#include "screenshotpluginfactory.h"


#include <globals.h>

extern "C" 
{ 
	NPlugin::PluginContainer* new_screenshotplugin() 
	{
		return new NPlugin::ScreenshotPluginContainer;
	} 

	NPlugin::PluginInformation get_pluginInformation()
	{
		return NPlugin::PluginInformation("screenshotplugin", toString(NPackageSearch::VERSION), "Benjamin Mesing");
	} 
}

/** Initialize the plugin. */
__attribute__ ((constructor)) void init() 
{
}
      
      
// __attribute__ ((destructor)) void fini() 
// {
//   /* code here is executed just before dlclose() unloads the module */
// } 


namespace NPlugin
{

ScreenshotPluginContainer::ScreenshotPluginContainer()
{
	_pScreenshotPlugin = 0;
	addPlugin("ScreenshotPlugin");
	
	_screenshotEnabled=true;
	
}
 
ScreenshotPluginContainer::~ScreenshotPluginContainer()
{
	unloadAllPlugins();
}

/////////////////////////////////////////////////////
// PluginContainer Interface
/////////////////////////////////////////////////////


bool ScreenshotPluginContainer::init(IProvider* pProvider)
{
	BasePluginContainer::init(pProvider, ScreenshotPluginFactory::getInstance());
	
	if (screenshotEnabled())
	{
		// use dynamic cast here because of the virtual base class 
		// (static_cast is not allowed there)
		_pScreenshotPlugin = dynamic_cast<ScreenshotPlugin*>(requestPlugin("ScreenshotPlugin"));
	}
	else
	{
		provider()->reportError(
			tr("Screenshots not supported" ),
			tr("Screenshots not supported")
		);
		return false;
	}
	return screenshotEnabled();
}




/////////////////////////////////////////////////////
// Helper Methods
/////////////////////////////////////////////////////

void ScreenshotPluginContainer::setScreenshotEnabled(bool enabled)
{
	_screenshotEnabled = enabled;
}


}	// namespace NPlugin
