#ifndef __SCREENSHOTPLUGINCONTAINER_H_2010_08_05
#define __SCREENSHOTPLUGINCONTAINER_H_2010_08_05

#include <string>

#include <QObject>
#include <QString>

#include <baseplugincontainer.h>

using namespace std;




class ScreenshotSettingsWidget;

namespace NPlugin
{

class ScreenshotPlugin;

/** @brief This class provides plugins for displaying screenshots.
  * 
  * @author Benjamin Mesing
  */
class ScreenshotPluginContainer : public QObject, public BasePluginContainer 
{
	Q_OBJECT
	
	/** @brief This holds if the screenshot operations are currently enabled. 
	  * 
	  * This will be set to false if the reading of the tag database failed. 
	  */
	bool _screenshotEnabled;

	/** Holds the screenshot plugin created. */
	ScreenshotPlugin* _pScreenshotPlugin;
public:
	/** Empty Constructor  */
	ScreenshotPluginContainer();
	~ScreenshotPluginContainer();
	/** @name PluginContainer Interface
	  *
	  * These functions implement the PluginContainer interface.
	  */
	//@{
	virtual bool init(IProvider* pProvider);
	/** @returns "screenshotplugin" */
	virtual string name() const {	return "screenshotplugin"; };
	virtual QString title() const	{ return tr("Screenshot Plugins"); };
	//@}
	
	/** @brief Returns the #_screenshotEnabled property. */
	bool screenshotEnabled()	{ return _screenshotEnabled; };
protected:
	/** @brief This sets the screenshot operations to be enabled/ disabled 
	  *
	  * If set to false all widgets will be disabled, else they will be enabled.
	  * @see #_screenshotEnabled 
	  */
	void setScreenshotEnabled(bool enabled);
};

}	// namespace NPlugin

#endif //	__SCREENSHOTPLUGINCONTAINER_H_2010_08_05

