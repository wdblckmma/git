//
// C++ Interface: orphanpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_ORPHANPLUGINFACTORY_H_2005_06_08
#define __NPLUGIN_ORPHANPLUGINFACTORY_H_2005_06_08

#include <ipluginfactory.h>

namespace NPlugin 
{

class OrphanPluginFactory;

/**
@author Benjamin Mesing
*/
class OrphanPluginFactory : public IPluginFactory
{
private:
	OrphanPluginFactory();
	static OrphanPluginFactory* _pInstance;
public:
	~OrphanPluginFactory();
	/** @name IPluginFactory interface */
	//@{
	virtual Plugin* createPlugin(const string& name) const;
	//@}
	/** @brief Returns the single instance of this class. */
	static OrphanPluginFactory* getInstance();
};

}

#endif // __NPLUGIN_ORPHANPLUGINFACTORY_H_2005_06_08
