//
// C++ Implementation: orphanfeedbackwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "orphanfeedbackwidgetimpl.h"

OrphanFeedbackWidget::OrphanFeedbackWidget(QWidget *parent)
 : QWidget(parent), Ui::OrphanFeedbackWidget()
{
	setupUi(this);
}


OrphanFeedbackWidget::~OrphanFeedbackWidget()
{
}



void OrphanFeedbackWidget::setClearButton( QPushButton * pButton, int index )
{
	delete(_pClearButton);
	_pClearButton = pButton;
	static_cast<QBoxLayout*>(layout())->insertWidget(index, pButton);
}
