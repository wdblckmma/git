# File generated by kdevelop's qmake manager. 
# ------------------------------------------- 
# Subdir relative project main directory: ./orphanplugin
# Target is a library:  

FORMS += orphansearchinput.ui \
         orphanfeedbackwidget.ui 
HEADERS += orphanplugincontainer.h \
           orphanpluginfactory.h \
           orphanplugin.h \
           orphansearchinputimpl.h \
           orphanfeedbackwidgetimpl.h
SOURCES += orphanplugincontainer.cpp \
           orphanpluginfactory.cpp \
           orphanplugin.cpp \
           orphansearchinputimpl.cpp \
           orphanfeedbackwidgetimpl.cpp
TRANSLATIONS = ../../../translations/orphanplugin_de.ts \
	../../../translations/orphanplugin_es.ts
INCLUDEPATH = . \
	../../
PKGCONFIG += libept

# this is required to remove the --no-undefined flag, which causes a linker error
QMAKE_LFLAGS -= -Wl,--no-undefined
# yeah, go for C++11!
QMAKE_CXXFLAGS += -std=c++11
# add standard Debian flags
QMAKE_CXXFLAGS_RELEASE += $$system(dpkg-buildflags --get CPPFLAGS)
QMAKE_CXXFLAGS_RELEASE += $$system(dpkg-buildflags --get CXXFLAGS)
QMAKE_LFLAGS += $$system(dpkg-buildflags --get LDFLAGS)

MOC_DIR = .moc
UI_DIR = .ui
OBJECTS_DIR = .obj
DESTDIR = ../
CONFIG += warn_on \
	qt \
	thread \
	plugin \
	stl \
	link_pkgconfig
VERSION = 0.0.1
TEMPLATE = lib
QT += xml widgets 
debug {
    message("generating debug version")
    OBJECTS_DIR = .obj_debug
    DEFINES += __DEBUG
    CONFIG -= release
}
else {
    message("generating release version")
    OBJECTS_DIR = .obj
}
