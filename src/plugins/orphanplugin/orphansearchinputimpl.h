//
// C++ Interface: orphansearchinputimpl
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_ORPHANSEARCHINPUTIMPL_H_2005_06_28
#define __NPLUGIN_ORPHANSEARCHINPUTIMPL_H_2005_06_28

#include <qstring.h>

#include "ui_orphansearchinput.h"


class QButtonGroup;

namespace NPlugin 
{



/**
@author Benjamin Mesing

*/
class OrphanSearchInputImpl : public QWidget, public Ui::OrphanSearchInput
{
	Q_OBJECT
	QButtonGroup* _pOptionButtonGroup;
public:
	// This enum corresponds to the values in the IDs of the radio buttons
	enum OrphanSearchOption
	{
		DEFAULT = 1,
		LIBDEVEL,
		ALL,
		GUESS_USELESS,
		RESIDUAL_CONFIGURATION
	};
	OrphanSearchInputImpl(QWidget *parent = 0);
	~OrphanSearchInputImpl();
	virtual QString getDeborphanCommandLine();
	virtual void clearSearch();
	virtual bool isActive();
	/** @brief Returns the current search option. 
	  *
	  * Never returns a value not in OrphanSearchOption.
	  */
	virtual OrphanSearchOption searchOption() const; 
	/** Sets the search option. */
	virtual void setSearchOption(OrphanSearchOption searchOption);
	
	
public Q_SLOTS:
	/** @brief Slot which emits the search changed signal. */
	virtual void onSearchChanged();
	void on__pSearchOrphanedOption_toggled(bool checked);

signals:
	void searchChanged();
};


}
#endif	// __NPLUGIN_ORPHANSEARCHINPUTIMPL_H_2005_06_28
