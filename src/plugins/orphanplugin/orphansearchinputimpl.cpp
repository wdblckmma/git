//
// C++ Implementation: orphansearchinputimpl
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "orphansearchinputimpl.h"

#include <QButtonGroup>
#include <qcheckbox.h>

namespace NPlugin {

OrphanSearchInputImpl::OrphanSearchInputImpl(QWidget* parent)
 : QWidget(parent)
{
	setupUi(this);
	_pOptionButtonGroup = new QButtonGroup(this);
	_pOptionButtonGroup->addButton(_pDefaultOption);
	_pOptionButtonGroup->addButton(_pLibdevelOption);
	_pOptionButtonGroup->addButton(_pAllOption);
	_pOptionButtonGroup->addButton(_pGuessUselessOption);
	_pOptionButtonGroup->addButton(_pResidualConfigurationOption);
	connect(_pOptionButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)), SLOT(onSearchChanged()));
}


OrphanSearchInputImpl::~OrphanSearchInputImpl()
{
}



void OrphanSearchInputImpl::onSearchChanged()
{
	emit(searchChanged());
}




QString OrphanSearchInputImpl::getDeborphanCommandLine()
{
	QString commandLine = "deborphan";
	// switch according to the buttonID set in the designer
	switch (searchOption())
	{
		case DEFAULT:
			break;
		case LIBDEVEL:
			commandLine.append(" --libdevel");
			break;
		case ALL:
			commandLine.append(" --all-packages --no-show-section");
			break;
		case GUESS_USELESS:
			commandLine.append(" --guess-all");
			break;
		case RESIDUAL_CONFIGURATION:
			commandLine.append(" --find-config");
			break;
	}
	return commandLine;
}


void OrphanSearchInputImpl::clearSearch()
{
	_pSearchOrphanedOption->setChecked(false);
}


bool OrphanSearchInputImpl::isActive()
{
	return _pSearchOrphanedOption->isChecked();
}

OrphanSearchInputImpl::OrphanSearchOption OrphanSearchInputImpl::searchOption() const
{
	if (_pOptionButtonGroup->checkedButton() == _pDefaultOption)
		return DEFAULT;
	else if (_pOptionButtonGroup->checkedButton() == _pLibdevelOption)
		return LIBDEVEL;
	else if (_pOptionButtonGroup->checkedButton() == _pAllOption)
		return ALL;
	else if (_pOptionButtonGroup->checkedButton() == _pGuessUselessOption)
		return GUESS_USELESS;
	else if (_pOptionButtonGroup->checkedButton() == _pResidualConfigurationOption)
		return RESIDUAL_CONFIGURATION;
	qDebug("Warning: Unknown Orphan search option");
	return DEFAULT;
}

void OrphanSearchInputImpl::setSearchOption(OrphanSearchOption searchOption)
{
	QRadioButton* pCurrentButton = 0;
	switch (searchOption)
	{
		case DEFAULT:
			pCurrentButton = _pDefaultOption;
			break;
		case LIBDEVEL:
			pCurrentButton = _pLibdevelOption;
			break;
		case ALL:
			pCurrentButton = _pAllOption;
			break;
		case GUESS_USELESS:
			pCurrentButton = _pGuessUselessOption;
			break;
		case RESIDUAL_CONFIGURATION:
			pCurrentButton = _pResidualConfigurationOption;
			break;
	}
	pCurrentButton->setChecked(true);
	
}


void OrphanSearchInputImpl::on__pSearchOrphanedOption_toggled(bool checked)
{
	_pOptionGroup->setEnabled(checked);
	emit(searchChanged());
}




}
