//
// C++ Interface: orphanfeedbackwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __ORPHANFEEDBACKWIDGET_H_2005_08_24
#define __ORPHANFEEDBACKWIDGET_H_2005_08_24

#include <qwidget.h>
#include <ui_orphanfeedbackwidget.h>

/**
@author Benjamin Mesing
*/
class OrphanFeedbackWidget : public QWidget, public Ui::OrphanFeedbackWidget
{
Q_OBJECT
public:
	OrphanFeedbackWidget(QWidget *parent = 0);

	~OrphanFeedbackWidget();
	void setClearButton( QPushButton * pButton, int index );
};

#endif	// __ORPHANFEEDBACKWIDGET_H_2005_08_24
