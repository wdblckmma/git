//
// C++ Interface: orphanplugincontainer
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_ORPHANPLUGINCONTAINER_H_2005_06_08
#define __NPLUGIN_ORPHANPLUGINCONTAINER_H_2005_06_08

#include <qobject.h>
#include <baseplugincontainer.h>

namespace NPlugin {

/**
@author Benjamin Mesing
*/
class OrphanPluginContainer : public QObject, public BasePluginContainer
{
Q_OBJECT
public:
	OrphanPluginContainer();

	virtual ~OrphanPluginContainer();

	/** @name PluginContainer Interface
	  *
	  * These functions implement the PluginContainer interface.
	  */
	//@{
	virtual bool init(IProvider* pProvider);
	/** This gets a plugin with the given name.
	  *
	  * Accepted names are: 
	  * \li "FilenamePlugin"
	  * @see PluginContainer::requestPlugin()
	  */
	/** @returns "filenameplugin" */
	virtual string name() const {	return "orphanplugin"; };
	virtual QString title() const;
	/** This returns an empty list. */
	vector< pair<QString, QAction*> > actions() {	return vector< pair<QString, QAction*> >(); };
	//@}
};

}

#endif	// __NPLUGIN_ORPHANPLUGINCONTAINER_H_2005_06_08
