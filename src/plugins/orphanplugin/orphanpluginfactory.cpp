//
// C++ Implementation: orphanpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "orphanpluginfactory.h"

#include "orphanplugin.h"

namespace NPlugin {

OrphanPluginFactory* OrphanPluginFactory::_pInstance = 0;


OrphanPluginFactory::OrphanPluginFactory()
{
}


OrphanPluginFactory::~OrphanPluginFactory()
{
}

Plugin* OrphanPluginFactory::createPlugin(const string& name) const
{
	if (name=="OrphanPlugin")
	{
		return new OrphanPlugin();
	}
	else
		return 0;
}


OrphanPluginFactory* OrphanPluginFactory::getInstance()
{
	if (_pInstance == 0)
		_pInstance = new OrphanPluginFactory;
	return _pInstance;
}



}
