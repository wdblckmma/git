//
// C++ Interface: orphanplugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGINORPHANPLUGIN_H_2005_06_08
#define __NPLUGINORPHANPLUGIN_H_2005_06_08

#include <QObject>

#include <searchplugin.h>

class OrphanFeedbackWidget;

namespace NPlugin {

class OrphanSearchInputImpl;


/**
@author Benjamin Mesing
*/
class OrphanPlugin : public SearchPlugin
{
Q_OBJECT
public:
	static const QString PLUGIN_NAME;
	OrphanPlugin();
	virtual ~OrphanPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual uint priority() const { return 7; };
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	/** @returns &quot;OrphanPlugin&quot; */
	virtual QString title() const;
	virtual QString briefDescription() const;
	virtual QString description() const;
	virtual QDomElement loadSettings(const QDomElement source);
	virtual void saveSettings(NXml::XmlData& outData, QDomElement parent) const;
	//@}
	
	/** @name SearchPlugin interface
	  * 
	  * Implementation of the SearchPlugin interface
	  */
	//@{
	virtual uint searchPriority() const { return 9; };
	virtual QWidget* inputWidget() const;
	virtual QString inputWidgetTitle() const;
	virtual QWidget* shortInputAndFeedbackWidget() const;
	virtual void clearSearch();
	virtual bool usesFilterTechnique() const { return false; };
	virtual const std::set<string>& searchResult() const	{ return _searchResult; };
	virtual bool filterPackage(const string&) const	{ return true; };
	virtual bool isInactive() const;
	//@}
protected Q_SLOTS:
	/** @brief Evaluates the currently entered search. */
	void evaluateSearch();
	/** @brief Slot that calls clearSearch() */
	void onClearSearch()	{ clearSearch(); };

	/** @brief Extracts the package name from the result line from deborphan.
	 *
	 * Currently this only strips the arch-part from the line, if available.
	 */
	static string extractPackageNameFromDeborphanLine(const string& line );
private:
	/** Widget where the search might be enterd. */
	OrphanSearchInputImpl* _pInputWidget;
	/** The widget where the search selected is shown in. */
	OrphanFeedbackWidget* _pOrphanFeedbackWidget;
	/** Holds a pointer to the pluginmanager which manages this pugin. */
	IProvider* _pProvider;
	/** This holds the result of the currently active search. */
	std::set<string> _searchResult;
};

}

#endif	// __NPLUGINORPHANPLUGIN_H_2005_06_08
