//
// C++ Implementation: orphanplugincontainer
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <iprovider.h>

#include <globals.h>
#include <helpers.h>

#include "orphanplugincontainer.h"

#include "orphanpluginfactory.h"


extern "C" 
{ 
	NPlugin::PluginContainer* new_orphanplugin() 
	{
		return new NPlugin::OrphanPluginContainer;; 
	} 
	
	NPlugin::PluginInformation get_pluginInformation()
	{
		return NPlugin::PluginInformation("orphanplugin", toString(NPackageSearch::VERSION), "Benjamin Mesing");
	} 
}


namespace NPlugin 
{

OrphanPluginContainer::OrphanPluginContainer()
{
	addPlugin("OrphanPlugin");
}


OrphanPluginContainer::~OrphanPluginContainer()
{
	unloadAllPlugins();
}


/////////////////////////////////////////////////////
// Plugin Container Interface
/////////////////////////////////////////////////////

bool OrphanPluginContainer::init(IProvider* pProvider)
{
	BasePluginContainer::init(pProvider, OrphanPluginFactory::getInstance());
	requestPlugin("OrphanPlugin");
	if (!QFile::exists("/usr/bin/deborphan"))
	{
		provider()->reportError( 
			tr("deborphan not available"), 
			tr("The <tt>deborphan</tt> application, needed by the orphan plugin, was not found. "
				"The orphan plugin was disabled. "
				"To use the orphan plugin install the deborphan package via<br>"
				"<tt>apt-get install deborphan</tt><br>" 
				"and reenable the plugin using <i>Packagesearch -> Control Plugins</i> afterwards."
			)
		);
		return false;
	}
	return true;
}


QString OrphanPluginContainer::title() const
{
	return QObject::tr("Orphan Plugins");
}



}
