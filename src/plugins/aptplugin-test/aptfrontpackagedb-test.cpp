#ifdef __UNIT_TEST_PP

#include <UnitTest++.h>
#include <QString>
#include <iostream>

#include <aptfrontpackagedb.h>

using namespace std;


/// testing code for AptFrontPackageDB::searchString(text, searchPattern, caseSensitive, wholeWords)
TEST(AptFrontPackageDB__searchString)
{
	using namespace NApt;
	QString text;
	text = "apt-get";
	CHECK( AptFrontPackageDB::searchString(text, "apt", false, false));
	CHECK( AptFrontPackageDB::searchString(text, "apt", false, true));
	CHECK( AptFrontPackageDB::searchString(text, "apt", true, false));
	CHECK(!AptFrontPackageDB::searchString(text, "-get", true, true));
	CHECK( AptFrontPackageDB::searchString(text, "-get", true, false));
	CHECK( AptFrontPackageDB::searchString(text, "Apt", false, false));
	CHECK(!AptFrontPackageDB::searchString(text, "Apt", true, false));
	CHECK(!AptFrontPackageDB::searchString(text, "Apt", true, true));
	text = "aptget-apt";
	CHECK( AptFrontPackageDB::searchString(text, "apt", false, true));
	CHECK( AptFrontPackageDB::searchString(text, "apt", true, false));
	CHECK( AptFrontPackageDB::searchString(text, "apt", true, true));
	CHECK(!AptFrontPackageDB::searchString(text, "get", false, true));
	CHECK( AptFrontPackageDB::searchString(text, "get", true, false));
	CHECK(!AptFrontPackageDB::searchString(text, "get", true, true));
	text = "python-apt-doc";
	CHECK(AptFrontPackageDB::searchString(text, "python", false, false));
}

#endif
