TEMPLATE = app

CONFIG += warn_on \
	qt \
	thread \
	stl \
	link_pkgconfig

QT += xml

TARGET = run-tests

SOURCES += aptfrontpackagedb-test.cpp \
		main-test.cpp

INCLUDEPATH = . \
	../../ \
	../aptplugin

debug {
    message("generating debug version")
    OBJECTS_DIR = .obj_debug
    DEFINES += __DEBUG
    CONFIG -= release
}
else {
    message("generating release version")
    OBJECTS_DIR = .obj
}


# use unit test ++
DEFINES += __UNIT_TEST_PP
PKGCONFIG += unittest++
LIBS += -L../../ -L../ -lpackagesearch -laptplugin
QMAKE_CXXFLAGS += -std=c++11




