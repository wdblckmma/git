//
// C++ Interface: aptsearchpluginshortinputwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __APTSEARCHPLUGINSHORTINPUTWIDGET_H_2005_08_27
#define __APTSEARCHPLUGINSHORTINPUTWIDGET_H_2005_08_27

#include <qwidget.h>
#include <ui_aptsearchpluginshortinputwidget.h>

/**
@author Benjamin Mesing
*/
class AptSearchPluginShortInputWidget : public QWidget, public Ui::AptSearchPluginShortInputWidget
{
Q_OBJECT
public:
	AptSearchPluginShortInputWidget(QWidget *parent = 0, const char *name = 0);

	~AptSearchPluginShortInputWidget();
	void setClearButton( QPushButton * pButton, int index );
};

#endif	// __APTSEARCHPLUGINSHORTINPUTWIDGET_H_2005_08_27
