//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_INSTALLEDVERSIONPLUGIN_H_2005_03_25
#define __NPLUGIN_INSTALLEDVERSIONPLUGIN_H_2005_03_25

#include <QObject>

#include <shortinformationplugin.h>

using namespace std;

namespace NApt
{
	class IPackageDB;
}

namespace NPlugin {

/**
@author Benjamin Mesing
*/
class InstalledVersionPlugin : public QObject, public ShortInformationPlugin
{
	const QString _title;
	const QString _briefDescription;
	const QString _description;
	/** @brief Package database to be used. */
	NApt::IPackageDB* _pPackageDB;
public:
	static const QString PLUGIN_NAME;
	InstalledVersionPlugin(NApt::IPackageDB* pPackageDB);
	~InstalledVersionPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const { return _title; };
	virtual QString briefDescription() const { return _briefDescription; };
	virtual QString description() const { return _description; };
	//@}
	/** @name ShortInformationPlugin interface
	  * 
	  * Implementation of the ShortInformationPlugin interface
	  */
	//@{
	virtual uint shortInformationPriority() const	{ return 4; }
	virtual const QString shortInformationText(const string& package);
	virtual QString shortInformationCaption() const { return tr("Installed Version"); };
	// documented in base class
	virtual int preferredColumnWidth() const { return 6; }
	//@}

};

};

#endif	//  __NPLUGIN_INSTALLEDVERSIONPLUGIN_H_2005_03_25
