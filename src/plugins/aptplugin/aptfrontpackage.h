//
// C++ Interface: aptfrontpackage
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NAPT_APTFRONTPACKAGE_H_2005_12_26
#define __NAPT_APTFRONTPACKAGE_H_2005_12_26

#include <ipackage.h>


#include <helpers.h>

namespace ept {
class Aggregator;
namespace apt {
class Apt;
class PackageRecord;
}
}

namespace NApt 
{

/** @brief A package class using libept.
  *
  * Note that accessing the package remains valid only as long as
  * the underlying ept::apt::Apt objekt does not change.
  * It also holds a pointer to the corresponding ept::apt::PackageRecord
  * object which also must not become invalid.
  *
  * @author Benjamin Mesing
  */
class AptFrontPackage : public IPackage
{
	const ept::apt::Apt* _pApt;
	//typedef ept::configuration::apt::cache::Relation Relation;
	std::string _source;
	mutable ept::apt::PackageRecord* m_rec;

	const ept::apt::PackageRecord& rec() const;

public:
	AptFrontPackage(const ept::apt::Apt& pApt, const std::string& sourcePackage);
	~AptFrontPackage();
	
	virtual QString name() const;
	virtual QString essential() const;
	virtual QString priority() const;
	virtual QString section() const;
	virtual QString installedSize() const;
	virtual QString maintainer() const;
	virtual QString architecture() const;
	virtual QString source() const;
	virtual QString version() const;
	virtual QString replaces() const;
	virtual QString provides() const;
	virtual QString preDepends() const;
	virtual QString depends() const;
	virtual QString recommends() const;
	virtual QString suggests() const;
	virtual QString conflicts() const;
	virtual QString filename() const;
	virtual QString size() const;
	virtual QString md5sum() const;
	virtual QString conffiles() const;
	virtual QString description() const;
	virtual InstalledState installedState() const;
	virtual QString installedVersion() const;
	virtual QString shortDescription() const;
	virtual QString homepage() const;

	virtual uint getInstalledSize() const;
	virtual uint getSize() const;

	/** @brief Returns if the source apt-front package is valid. */
	bool isValid() const	{ return !_source.empty(); }

protected:
	/** @brief Returns a string representation for all relations of the handed type. */
	//QString getRelation(Relation::Type type) const;

private:
	static QString _emptyString;
};

}

#endif	// __NAPT_APTFRONTPACKAGE_H_2005_12_26
