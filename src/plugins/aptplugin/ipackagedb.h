#ifndef __IPACKAGEDB_H_2004_02_19
#define __IPACKAGEDB_H_2004_02_19

#include "ipackage.h"

namespace NUtil
{
	class IProgressObserver;
}

namespace NApt
{

/**
@brief This is the interface for all implementations of a package database.


@author Benjamin Mesing
*/
class IPackageDB
{
public:
	IPackageDB();
	virtual ~IPackageDB();
	 
 	/** @brief This gets the information about the given package from the database.
	  *
	  * @param pkg the name of the package the information should be retrieved
	  * @throws PackageNotFoundException if no package with such name was in the database
	  */
	virtual const IPackage& getPackageRecord(const QString& pkg) const = 0;
 	/** @brief This gets the information about the given package from the database.
	  *
	  * @param packageHandle the handle of the package the information should be retrieved
	  * @param linebreak the linebreak command (e.g. "\n" or "&lt;br&gt;")
	  * @throws PackageNotFoundException if no package with such name was in the database
	  */
	virtual const IPackage& getPackageRecord(const string& package) const = 0;
	/** @brief This gets the short description for the requested package.
	  *
	  * @param packageHandle the handle for the package the information should be retrieved for
	  * @throws PackageNotFoundException if no package with this handle was in the database
	  */
	virtual const QString getShortDescription(const string& package) const = 0;
	/** @brief Returns the installed state for the package referenced. 
	  *
	  * @param package the package the information should be retrieved for
	  * @throws PackageNotFoundException if no package with this handle was in the database
	  */
    virtual IPackage::InstalledState getState(const string& package) const = 0;
	/** @brief This reloads the package information. 
	  * @param pLoader the observer where to report the reload progress, 0 if no progress should be reported  
	  */
	virtual void reloadPackageInformation(NUtil::IProgressObserver* pObserver) = 0;
};

} // NApt

#endif	//  __IPACKAGEDB_H_2004_02_19
