//
// C++ Interface: extpackagedb
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __EXTPACKAGEDB_H_2004_03_31
#define __EXTPACKAGEDB_H_2004_03_31

#include <string>
#include <list>
#include <map>

#include <qstring.h>

#include <PackageDB.h>

#include "ipackagedb.h"

using namespace std;

namespace NApt
{

/** This class extents the PackageDB class in the way, that getPackageRecord() function
  * parses the string description it get an creates a struct for easy access.
  * @author Benjamin Mesing
  */
class ExtPackageDB : public IPackageDB, protected Debtags::PackageDB
{
public:
	/** @brief This gets the information about the given package from the database.
	  *
	  * @param pkg the name of the package the information should be retrieved
	  * @param linebreak the linebreak command (e.g. "\n" or "<br>"
	  * 
	  * It might throw a PackageDBException. See PacakgeDB::getPackageRecord(). */
	virtual Package getPackageRecord(const QString& pkg, const QString& linebreak="\n");
	/** @brief This gets the short description from requested package from the database.
	  *
	  * @param pkg the name of the package the information should be retrieved
	  * 
	  * It might throw a PackageDBException. See PacakgeDB::getPackageRecord(). */
	virtual QString getShortDescription(const string& pkg);
	// documented in base class
	virtual QString getShortDescription(int packageHandle);

	
	/** @warning this version does not work any more - returns always installed*/
	virtual Package::InstalledState getState(int packageHandle)
	{	
		// TODO verify this cast! (Probably not working any more!
//		return (Package::State) Debtags::PackageDB::getState(pkg);
		return Package::INSTALLED;
	}
	
	ExtPackageDB();
	virtual ~ExtPackageDB();
};

}	// namespace NApt

#endif	//  __EXTPACKAGEDB_H_2004_03_31
