//
// C++ Interface: aptsearchscorecalculationstrategy
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NAPT_APTSEARCHSCORECALCULATIONSTRATEGY_H_2005_07_31
#define __NAPT_APTSEARCHSCORECALCULATIONSTRATEGY_H_2005_07_31

#include <scorecalculationstrategybase.h>
#include <qstringlist.h>

namespace NApt {

/**
@author Benjamin Mesing
*/
class AptSearchScoreCalculationStrategy : public NPlugin::ScoreCalculationStrategyBase
{
protected:
	/** @brief Holds of the search was performed case sensitve.
	  *
	  * Default is false.
	  */
	bool _cs;
	QStringList _includePatterns;
public:
	AptSearchScoreCalculationStrategy();
	virtual ~AptSearchScoreCalculationStrategy();
	void setCaseSensitive(bool cs)	{ _cs = cs; }
	bool isCaseSensitive()	{ return _cs; }
	void setIncludePatterns(const QStringList& includePatterns)	{ _includePatterns = includePatterns; }
	
};

}

#endif	// __NAPT_APTSEARCHSCORECALCULATIONSTRATEGY_H_2005_07_31
