//
// C++ Implementation: aptfrontpackage
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "aptfrontpackage.h"

#include <QtDebug>

#include <ept/apt/packagerecord.h>
#include <ept/apt/apt.h>

#include <apt-pkg/pkgcache.h>
#include <apt-pkg/pkgrecords.h>

namespace NApt 
{

QString AptFrontPackage::_emptyString;


AptFrontPackage::AptFrontPackage(const ept::apt::Apt& pApt, const std::string& sourcePackage)
 : _pApt(&pApt), _source(sourcePackage), m_rec(0)
{
}

AptFrontPackage::~AptFrontPackage()
{
	delete m_rec;
}

const ept::apt::PackageRecord& AptFrontPackage::rec() const
{
	if (!m_rec)
		m_rec = new ept::apt::PackageRecord(_pApt->rawRecord(_source));
	return *m_rec;
}

QString AptFrontPackage::name() const
{
	return toQString(_source);;
}

QString AptFrontPackage::essential() const
{
	return _emptyString;
}

QString AptFrontPackage::priority() const
{
	return _emptyString;
}

QString AptFrontPackage::section() const
{
	return toQString(rec().section(string()));;
}

QString AptFrontPackage::installedSize() const
{
	return toQString(rec()["Installed-Size"]);
}


uint AptFrontPackage::getInstalledSize() const
{
	return rec().installedSize();
}

QString AptFrontPackage::maintainer() const
{
	return toQString(rec().maintainer(string()));
}

QString AptFrontPackage::architecture() const
{
	return toQString(rec().architecture(string()));
}

QString AptFrontPackage::source() const
{
	return toQString(rec().source(string()));;
}

QString AptFrontPackage::version() const
{
	ept::apt::Version ver = _pApt->candidateVersion(_source);
	// FIXME: I used candidateVersion, but there is also anyVersion
	//if (_source.candidateVersion().valid())
		//return toQString(_source.candidateVersion().versionString());
	return toQString(ver.version());
}

QString AptFrontPackage::replaces() const
{
	return toQString(rec().replaces(string()));;
	//return AptFrontPackage::getRelation(Relation::Replaces);
}

QString AptFrontPackage::provides() const
{
	return toQString(rec().provides(string()));;
}

QString AptFrontPackage::preDepends() const
{
	return toQString(rec().preDepends(string()));;
}

QString AptFrontPackage::depends() const
{
	return toQString(rec().depends(string()));;
}

QString AptFrontPackage::recommends() const
{
	return toQString(rec().recommends(string()));;
}

QString AptFrontPackage::suggests() const
{
	return toQString(rec().suggests(string()));;
}

QString AptFrontPackage::conflicts() const
{
	return toQString(rec().conflicts(string()));;
}

QString AptFrontPackage::filename() const
{
	return toQString(rec().filename(string()));;
}

QString AptFrontPackage::size() const
{
	return toQString(rec()["Size"]);
}

uint AptFrontPackage::getSize() const
{
	return rec().size();
}

QString AptFrontPackage::md5sum() const
{
	return toQString(rec().md5sum(string()));;
}

QString AptFrontPackage::conffiles() const
{
	return _emptyString;
}

QString AptFrontPackage::description() const
{
	// source code taken from libapt, and slightly adapted
	pkgCache* pPkgCache = const_cast<pkgCache*>(_pApt->aptPkgCache());
	pkgCache::PkgIterator pi = pPkgCache->FindPkg(toString(name()));
	if (pi.end()) 
		return QString();
	for (pkgCache::VerIterator vi = pi.VersionList(); !vi.end(); vi++)
	{
		const char* v = vi.VerStr();
		if (v == 0) continue;
		if (rec().version() != v) continue;

		// Get the translated record
		pkgRecords Recs(*pPkgCache);
		pkgCache::DescIterator Desc = vi.TranslatedDescription();
		pkgRecords::Parser &P = Recs.Lookup(Desc.FileList());
		// TODO currently the language code is not added here
		// this would break the packagerecord parsing algorithm, should be implemented
		// in the future though
		//result << "Description" << ( (strcmp(Desc.LanguageCode(),"") != 0) ? "-" : "" ) << Desc.LanguageCode() << ": " << P.LongDesc();
        QString longDescription = toQString(P.LongDesc());
        longDescription = longDescription.remove(0, longDescription.indexOf("\n"));
        return longDescription;
	}
	return QString();
}


AptFrontPackage::InstalledState AptFrontPackage::installedState() const
{
	ept::apt::PackageState state = _pApt->state(_source);
	if (state.upgradable())
		return UPGRADABLE;
	if (state.installed())
		return INSTALLED;
	return NOT_INSTALLED;
}

QString AptFrontPackage::installedVersion() const
{
	ept::apt::Version ver = _pApt->installedVersion(_source);
	return toQString(ver.version());
}

QString AptFrontPackage::shortDescription() const
{
	return toQString(rec().shortDescription(string()));;
}

QString AptFrontPackage::homepage() const 
{
	return toQString(rec().lookup("Homepage"));
}


}

