//
// C++ Interface: aptsettingswidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __APTSETTINGSWIDGET_H_20080605
#define __APTSETTINGSWIDGET_H_20080605

#include <QWidget>
#include "ui_aptsettingswidget.h"

#include "installationtool.h"

/**
	@author Benjamin Mesing <bensmail@gmx.net>
*/
class AptSettingsWidget : public QWidget, public Ui::AptSettingsWidget
{
public:
	
	AptSettingsWidget(QWidget* pParent);
	~AptSettingsWidget();
	/** Returns the installation tool selected by the user. */
	NApt::InstallationTool installationTool();
	void setInstallationTool(NApt::InstallationTool tool);
};

#endif	// __APTSETTINGSWIDGET_H_20080605
