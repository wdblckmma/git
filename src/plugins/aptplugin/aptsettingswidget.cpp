//
// C++ Implementation: aptsettingswidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "aptsettingswidget.h"

AptSettingsWidget::AptSettingsWidget(QWidget* pParent)
 : QWidget(pParent)
{
	setupUi(this);
}


AptSettingsWidget::~AptSettingsWidget()
{
}

NApt::InstallationTool AptSettingsWidget::installationTool() 
{
	if (_pAptButton->isChecked())
		return NApt::APT;
	return NApt::APTITUDE;
}

void AptSettingsWidget::setInstallationTool(NApt::InstallationTool tool) 
{
	switch (tool) {
		case NApt::APT:
			_pAptButton->setChecked(true);
			break;
		case NApt::APTITUDE:
			_pAptitudeButton->setChecked(true);
			break;
	}
}


