//
// C++ Interface: installationtool
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INSTALLATIONTOOL_20080605
#define __INSTALLATIONTOOL_20080605

#include <QString>

namespace NApt 
{

enum InstallationTool {
	APT,
	APTITUDE
};


/** @brief Returns the command to execute the given installation tool (e.g. /usr/bin/apt). */
QString getInstallationToolCommand(InstallationTool tool) ;

} // namespace NApt

#endif // __INSTALLATIONTOOL_20080605

