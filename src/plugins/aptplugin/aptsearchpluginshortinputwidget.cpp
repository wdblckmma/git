//
// C++ Implementation: aptsearchpluginshortinputwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "aptsearchpluginshortinputwidget.h"

AptSearchPluginShortInputWidget::AptSearchPluginShortInputWidget(QWidget *parent, const char *name)
 : QWidget(parent)
{
	setupUi(this);
	setObjectName(name);
}


AptSearchPluginShortInputWidget::~AptSearchPluginShortInputWidget()
{
}

void AptSearchPluginShortInputWidget::setClearButton( QPushButton * pButton, int index)
{
	delete(_pClearButton);
	_pClearButton = pButton;
	_pInputLayout->insertWidget(index,_pClearButton);
}

