//
// C++ Interface: aptactionplugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_APTACTIONPLUGIN_H_2005_08_31
#define __NPLUGIN_APTACTIONPLUGIN_H_2005_08_31

#include <QObject>
#include <actionplugin.h>

#include "installationtool.h"




namespace NPlugin {

class IAptMediator;

/** @brief Plugin offerings actions realted to apt.
  *
  * The actions offered are:
  * <ul>
  * 	<li>update database</li>
  * 	<li>reload package database</li>
  * 	<li>install package</li>
  * 	<li>remove package</li>
  * </ul>
  *
  * @author Benjamin Mesing
  */
class AptActionPlugin : public QObject, public NPlugin::ActionPlugin
{
Q_OBJECT
	const QString _title;
	const QString _briefDescription;
	const QString _description;
	/** @brief Action used to update the apt database. */
	Action* _pUpdateAction;
	/** @brief Action used to reload the apt database. */
	Action* _pReloadDbAction;
	Action* _pCreateInstallLineAction;
	/** @name Package Actions
	  * 
	  * @brief Actions for installing/removing packages.
	  */
	//@{
	Action* _pInstallAction;
	Action* _pRemoveAction;
	Action* _pPurgeAction;
	//@}
	Action* _pSeparatorAction;
	IProvider* _pProvider;
	IAptMediator* _pMediator;
protected Q_SLOTS:
	void onCreateInstallLineAction();
	void onInstallAction();
	void onRemoveAction();
	void onPurgeAction();
	void onUpdateAction();
	void onReloadAction();
protected:
	/** @brief Installs or removes the current package. 
	  *
	  * @param install if true, the package will be installed, if false it will be removed
	  * @param purge determines if the package shall be purged on removal (no effect for
	  * installing)
	  */
	void installOrRemove(bool install, bool purge = false);
public:
	static const QString PLUGIN_NAME;
	AptActionPlugin(IAptMediator* pMediator);
	~AptActionPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const { return _title; };
	virtual QString briefDescription() const { return _briefDescription; };
	virtual QString description() const { return _description; };
	//@}
	/** @brief Returns the QAction object for the #_pAptUpdateAction. */
	QAction* qAptUpdateAction() const { return _pUpdateAction->action(); }
	/** @brief Returns the QAction object for the #_pReloadDbAction. */
	QAction* qReloadDbAction() const { return _pReloadDbAction->action(); }

	/** @name ActionPlugin Interface
	  * 
	  * Implementation of the ActionPlugin Interface.
	  */
	//@{
	virtual vector<Action*> actions() const;
	//@}
	
private:
	/** @brief Shortcut for calling _aptMediator->installationToolCommand() */
	QString installationToolCommand();
};

}

#endif	// __NPLUGIN_APTACTIONPLUGIN_H_2005_08_31
