//
// C++ Interface: aptpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __APTPLUGINFACTORY_H_2004_09_11
#define __APTPLUGINFACTORY_H_2004_09_11

#include <ipluginfactory.h>

namespace NApt
{
	class DumpAvailPackageDB;
	class IAptSearch;
 	class IPackageDB;
}

namespace NPlugin 
{

class IProvider;
class IAptMediator;

/** @brief Factory for creating apt plugins.
  *
  * @author Benjamin Mesing
  */
class AptPluginFactory : public IPluginFactory
{
	 
	/** This holds the interface to access the information of the packages
	  * and for searching. */
	NApt::IPackageDB* _pPackageDB; 
	/** This holds the interface to access the information of the packages
	  * and for searching. */
	NApt::IAptSearch* _pAptSearch; 
	/** Holds the mediator object to allow the plugins to communicate with each other. */
	IAptMediator* _pMediator; 
public:
	AptPluginFactory(IProvider* pProvider, IAptMediator* pMediator, 
		NApt::IPackageDB* pPackageDB, NApt::IAptSearch* pAptSearch);
	virtual ~AptPluginFactory();
	/** @name IPluginFactory interface */
	//@{
	// documented in base class
	virtual Plugin* createPlugin(const string& name) const;
	//@}
};

};

#endif	// __APTPLUGINFACTORY_H_2004_09_11
