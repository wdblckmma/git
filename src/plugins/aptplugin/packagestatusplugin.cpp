#include <qcombobox.h>
#include <QMainWindow>

#include <iprovider.h>

#include <packagenotfoundexception.h>

#include "packagestatusplugin.h"

#include "installedfilterwidget.h"
#include "ipackagedb.h"

namespace NPlugin
{

const QString PackageStatusPlugin::PLUGIN_NAME = "PackageStatusPlugin";

PackageStatusPlugin::PackageStatusPlugin(NApt::IPackageDB* pPackageDB) :
	_title(tr("Package Status Plugin")),
	/// @todo implement descriptions here
	_briefDescription(tr("")),
	_description(tr("")),
	_installedFilter(ALL),
	_pPackageDB(pPackageDB)
{ 
	_pInstalledFilterWidget = 0;
    _stateToText[NApt::IPackage::INSTALLED] = "i";
    _stateToText[NApt::IPackage::UPGRADABLE] = "u";
    _stateToText[NApt::IPackage::NOT_INSTALLED] = "";
}

PackageStatusPlugin::~PackageStatusPlugin() 
{ 
	delete _pInstalledFilterWidget;
}

/////////////////////////////////////////////////////
// Plugin Interface
/////////////////////////////////////////////////////
 
void PackageStatusPlugin::init(IProvider* pProvider)
{
	QMainWindow* pWindow = pProvider->mainWindow();
	_pInstalledFilterWidget = new InstalledFilterWidget(pWindow);
	_pInstalledFilterWidget->setObjectName("InstalledFilterInput");
	_pInstalledFilterWidget->show();
	connect(_pInstalledFilterWidget->_pInstalledFilterInput, 
		SIGNAL(activated(int)), SLOT(onInstalledFilterChanged(int))
	);	
}

/////////////////////////////////////////////////////
// SearchPlugin Interface
/////////////////////////////////////////////////////

QWidget* PackageStatusPlugin::shortInputAndFeedbackWidget() const
{
	return _pInstalledFilterWidget;
}


bool PackageStatusPlugin::filterPackage(const string& package) const
{
	try 
	{
		NApt::IPackage::InstalledState state = getState(package);
		if (_installedFilter == INSTALLED)
			return state == NApt::IPackage::INSTALLED || state == NApt::IPackage::UPGRADABLE;
		else
			return _installedFilter == (InstalledState) state;	// does the state of the package match the on filtered by?
	}
	catch (const PackageNotFoundException& e)	// if the package is not in the database, it is not installed
	{
		return _installedFilter == NOT_INSTALLED;
	}
}

void PackageStatusPlugin::clearSearch()
{
	_pInstalledFilterWidget->_pInstalledFilterInput->setCurrentIndex(0);
	// this should not be neccessary, because setCurrentIndex(0) should trigger
	// onInstalledFilterChanged
	onInstalledFilterChanged(0);
}

bool PackageStatusPlugin::isInactive() const
{
	return _installedFilter == ALL;
}

/////////////////////////////////////////////////////
// ShortInformationPlugin Interface
/////////////////////////////////////////////////////

const QString PackageStatusPlugin::shortInformationText(const string& package)
{
	return _stateToText[getState(package)];	// returns the text for the state of the package
}

/////////////////////////////////////////////////////
// PackageStatusPlugin functions
/////////////////////////////////////////////////////

NApt::IPackage::InstalledState PackageStatusPlugin::getState(const string& package) const
{
	try 
	{
		return _pPackageDB->getState(package);
	}
	catch (const PackageNotFoundException& e)
	{
        return NApt::IPackage::NOT_INSTALLED;
	}
}

void PackageStatusPlugin::onInstalledFilterChanged(int activated)
{
	switch (activated)
	{
		case 0:
			_installedFilter = ALL;
			break;
		case 1:
			_installedFilter = INSTALLED;
			break;
		case 2:
			_installedFilter = UPGRADABLE;
			break;
		case 3:
			_installedFilter = NOT_INSTALLED;
			break;
	}
	emit searchChanged(this);
}


}	// namespace NPlugin

