#ifndef NPLUGIN_IAPTMEDIATOR_H_2005_03_06
#define NPLUGIN_IAPTMEDIATOR_H_2005_03_06

#include <qstringlist.h>

#include "installationtool.h"

namespace NPlugin
{

/** @brief This interface is used to allow the Apt-Plugins to communicate with
  * each other without having to know each other.
  *
  * Also defines configuration options, shared among all apt-plugins (e.g. the 
  * installation command configured).
  *
  * @author Benjamin Mesing
  */
class IAptMediator
{
public:
	virtual ~IAptMediator() {};
	/** @brief Returns the list of patterns currently searched for by the AptSearchPlugin.
	  *
	  * @returns the list of patterns currently searched for, an empty list if no searched
	  * is active or the plugin is not available
	  */
	virtual QStringList searchPatterns() = 0;
	
	/** @brief Sets the installation tool to be used. */
	virtual void setInstallationTool(NApt::InstallationTool tool) = 0;
	
	/** @brief Returns the command to execute the configured installation tool (e.g. /usr/bin/apt). */
	virtual QString installationToolCommand() = 0;
	
	/** @brief Requests an update of the apt-database. */
	virtual void updateAptDatabase() = 0;

	/** @brief Requests an update of the apt-database. */
	virtual void reloadAptDatabase() = 0;

	

};

}	// namespace NPlugin

#endif // NPLUGIN_IAPTMEDIATOR_H_2005_03_06

