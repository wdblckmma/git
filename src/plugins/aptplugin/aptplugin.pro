# File generated by kdevelop's qmake manager. 
# ------------------------------------------- 
# Subdir relative project main directory: ./aptplugin
# Target is a library:  

FORMS += aptsearchpluginshortinputwidget.ui \
         installedfilterwidget.ui  \
 aptsettingswidget.ui
HEADERS += aptplugincontainer.h \
           aptsearchplugin.h \
           packagedescriptionplugin.h \
           packagestatusplugin.h \
           aptpluginfactory.h \
           ipackagedb.h \
           iaptsearch.h \
           installedversionplugin.h \
           availableversionplugin.h \
           aptsearchscorecalculationstrategy.h \
           complexscorecalculationstrategy.h \
           installedfilterwidget.h \
           aptsearchpluginshortinputwidget.h \
           aptactionplugin.h \
           ipackage.h \
           aptfrontpackagedb.h \
           aptfrontpackage.h  \
            aptsettingswidget.h \
 	installationtool.h \
 iaptmediator.h

SOURCES += aptplugincontainer.cpp \
           aptsearchplugin.cpp \
           packagedescriptionplugin.cpp \
           packagestatusplugin.cpp \
           aptpluginfactory.cpp \
           ipackagedb.cpp \
           iaptsearch.cpp \
           installedversionplugin.cpp \
           availableversionplugin.cpp \
           aptsearchscorecalculationstrategy.cpp \
           complexscorecalculationstrategy.cpp \
           installedfilterwidget.cpp \
           aptsearchpluginshortinputwidget.cpp \
           aptactionplugin.cpp \
           ipackage.cpp \
           aptfrontpackagedb.cpp \
           aptfrontpackage.cpp \
 aptsettingswidget.cpp \
 installationtool.cpp
TRANSLATIONS = ../../../translations/aptplugin_de.ts \
	../../../translations/aptplugin_es.ts
TEMPLATE = lib
VERSION = 0.0.1
INCLUDEPATH = . \
../../

LIBS += -lapt-pkg

PKGCONFIG = libept
# Uncomment to see warnings for all unresolved symbols, to catch locations of
# missing template instantiations
#LIBS += -Wl,--unresolved-symbols=report-all

# this is required to remove the --no-undefined flag, which causes a linker error
QMAKE_LFLAGS -= -Wl,--no-undefined
DESTDIR = ../
CONFIG += warn_on \
	qt \
	thread \
	plugin \
	stl \
	link_pkgconfig

MOC_DIR = .moc
UI_DIR = .ui
QT += xml widgets

debug {
    message("generating debug version")
    OBJECTS_DIR = .obj_debug
    DEFINES += __DEBUG
    CONFIG -= release
}
else {
    message("generating release version")
    OBJECTS_DIR = .obj
}
# yeah, go for C++11!
QMAKE_CXXFLAGS += -std=c++11
# add standard Debian flags
# add standard Debian flags
QMAKE_CXXFLAGS_RELEASE += $$system(dpkg-buildflags --get CPPFLAGS)
QMAKE_CXXFLAGS_RELEASE += $$system(dpkg-buildflags --get CXXFLAGS)
QMAKE_LFLAGS += $$system(dpkg-buildflags --get LDFLAGS)


