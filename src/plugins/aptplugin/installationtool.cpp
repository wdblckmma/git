//
// C++ Implementation: installationtool
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "installationtool.h"


namespace NApt {

/** @brief Returns the command to execute the given installation tool (e.g. /usr/bin/apt). */
QString getInstallationToolCommand(InstallationTool tool) 
{
	switch (tool) 
	{
		case APT:
			return "/usr/bin/apt-get";
			break;
		case APTITUDE:
		default:
			return "/usr/bin/aptitude";
	}
}


}	// namespace NApt