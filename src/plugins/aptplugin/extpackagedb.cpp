//
// C++ Implementation: extpackagedb
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <map>
#include <iostream>

#include <qstringlist.h>
#include <qregexp.h>

#include <singlehandlemaker.h>

#include "extpackagedb.h"

#include "helpers.h"


namespace NApt
{


ExtPackageDB::ExtPackageDB()
 : PackageDB()
{
}


ExtPackageDB::~ExtPackageDB()
{
}

NApt::Package ExtPackageDB::getPackageRecord(const QString& pkg, const QString& linebreak)
{
	Package result;
	QString record(toQString( PackageDB::getPackageRecord(toString(pkg)) ));
	map<QString, QString*> sectionToString;
	{	// init the map
		sectionToString["Package"]=&result.package;
		sectionToString["Essential"]=&result.essential;
		sectionToString["Priority"]=&result.priority;
		sectionToString["Section"]=&result.section;
		sectionToString["Installed-Size"]=&result.installedSize;
		sectionToString["Maintainer"]=&result.maintainer;
		sectionToString["Architecture"]=&result.architecture;
		sectionToString["Source"]=&result.source;
		sectionToString["Version"]=&result.version;
		sectionToString["Replaces"]=&result.replaces;
		sectionToString["Provides"]=&result.provides;
		sectionToString["Pre-Depends"]=&result.preDepends;
		sectionToString["Depends"]=&result.depends;
		sectionToString["Recommends"]=&result.recommends;
		sectionToString["Suggests"]=&result.suggests;
		sectionToString["Conflicts"]=&result.conflicts;
		sectionToString["Filename"]=&result.filename;
		sectionToString["Size"]=&result.size;
		sectionToString["MD5sum"]=&result.md5sum;
		sectionToString["Conffiles"]=&result.conffiles;
		sectionToString["Description"]=&result.description;
	}	
	if (record.isEmpty()) return result;	// the default record
	QStringList lines = QStringList::split('\n', record);
	// search all lines for the keywords
	QString dummy;	// this string is used for the first pass of the loop, to avoid a special variable
		// first_pass_through variable
	QString* pCurrentSection=&dummy;
	QString currentText;
	for (QStringList::iterator it=lines.begin(); it!=lines.end(); ++it)
	{
		QRegExp reg("^([\\w\\-]*): (.*)");	// get the Section out of it
		if (reg.search(*it) != -1)	// we have probably found a new section
		{
			QStringList captured = reg.capturedTexts();
			const QString& key = captured[1];		// key becomes the part before the :
			const QString& value = captured[2];	// value the one after it
			map<QString, QString*>::iterator jt = sectionToString.find(key);
			// check if it really is a new section i.e. its key is known
			if ( jt != sectionToString.end() )
			{
				(*pCurrentSection) = currentText;
				pCurrentSection = jt->second;	// point to the next section
				currentText = value;
			}
			else
			{
				currentText += linebreak + (*it);
			}
		}
		else
		{
			currentText += linebreak + (*it);
		}
	}
	// the last section should be a description
	if (pCurrentSection != &result.description)
		cerr << "Error in ExtPackageDB::getPackageRecord(string) last section must be a description" << endl;
	else
	{
		result.description=currentText;
		result.shortDescription=currentText.section('\n',0,0);	// get the first line
	}
	return result;
}


QString ExtPackageDB::getShortDescription(const string& pkg)
{
	string record( PackageDB::getPackageRecord(pkg) );
	const int DESCRIPTION_STRING_LENGTH = 13;	// including the following space
	int first = record.find("Description:");
	first += DESCRIPTION_STRING_LENGTH;
	int second = record.find("\n", first);
	return toQString(record.substr(first, second - first));
}

QString ExtPackageDB::getShortDescription(int packageHandle)
{
	return  getShortDescription(SingleHandleMaker::instance()->getItem(packageHandle));
};

}	// namespace NApt

