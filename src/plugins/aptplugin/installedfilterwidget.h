//
// C++ Interface: installedfilterwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __INSTALLEDFILTERWIDGET_H_2005_08_27
#define __INSTALLEDFILTERWIDGET_H_2005_08_27

#include <ui_installedfilterwidget.h>

/**
  * @author Benjamin Mesing
  */
class InstalledFilterWidget : public QWidget, public Ui::InstalledFilterWidget
{
Q_OBJECT
public:
	InstalledFilterWidget(QWidget *parent = 0);
	~InstalledFilterWidget();
};

#endif	// __INSTALLEDFILTERWIDGET_H_2005_08_27
