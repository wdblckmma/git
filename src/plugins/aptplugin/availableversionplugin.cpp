//
// C++ Implementation: %{MODULE}
//
// Description:
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "availableversionplugin.h"

#include <packagenotfoundexception.h>

#include "ipackagedb.h"

namespace NPlugin {

const QString AvailableVersionPlugin::PLUGIN_NAME = "AvailableVersionPlugin";


AvailableVersionPlugin::AvailableVersionPlugin(NApt::IPackageDB* pPackageDB) :
	_title(tr("Available Version Plugin")),
	_briefDescription(tr("Shows the version for the package available for download")),
	_description(tr("Shows the version for the package available for download")),
	_pPackageDB(pPackageDB)
{
}


AvailableVersionPlugin::~AvailableVersionPlugin()
{
}


/////////////////////////////////////////////////////
// Plugin Interface
/////////////////////////////////////////////////////
 
void AvailableVersionPlugin::init(IProvider*)
{
}

/////////////////////////////////////////////////////
// ShortInformationPlugin Interface
/////////////////////////////////////////////////////

const QString AvailableVersionPlugin::shortInformationText(const string& package)
{
	try 
	{
		return QString(_pPackageDB->getPackageRecord(package).version());
	}
	catch (const PackageNotFoundException& e)
	{
		return _emptyString;
	}
}

/////////////////////////////////////////////////////
// AvailableVersionPlugin functions
/////////////////////////////////////////////////////


};
