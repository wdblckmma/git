//
// C++ Interface: aptfrontpackagedb
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NAPT_APTFRONTPACKAGEDB_H_2005_12_26
#define __NAPT_APTFRONTPACKAGEDB_H_2005_12_26

#include <set>

#include <ipackagedb.h>
#include <iaptsearch.h>

#include "aptfrontpackage.h"

namespace NPlugin
{
	class IProvider;
}

namespace NApt 
{

/**
@author Benjamin Mesing
*/
class AptFrontPackageDB : public IPackageDB, public IAptSearch
{
//	typedef ept::configuration::apt::cache::Package Package;

	/** @brief The provider instance offering the apt-front cache. */
	NPlugin::IProvider* _pProvider;
	/** @todo TODO currently we return a reference to the current package
	  * in getPackageRecord() -- which changes all the time
	  *
	  * Ensure a sane semantic here.
	  */
	mutable AptFrontPackage _currentPackage;
public:
	/** @brief Constructs a new apt-front database, using the apt-front cache of the provider.
	  *
	  * @param pProvider The provider instance offering the apt-front cache. 
	  */
	AptFrontPackageDB(NPlugin::IProvider* pProvider);
	~AptFrontPackageDB();
	/** @name IPackageDB Interface
	  * 
	  * Implementation of the IPackageDB
	  */
	//@{
	virtual const AptFrontPackage& getPackageRecord(const QString& pkg) const;
	virtual const AptFrontPackage& getPackageRecord(const string& package) const;
	virtual const QString getShortDescription(const string& package) const;
	virtual IPackage::InstalledState getState(const string& package) const;
	virtual void reloadPackageInformation(NUtil::IProgressObserver* pObserver);
	//@}

	
	/** @name IAptSearch Interface
	  * 
	  * Implementation of the IAptSearch
	  */
	//@{
	virtual bool search(std::set<string>& result, const QStringList& includePatterns, 
		const QStringList& excludePatterns, bool searchDescr) const;
	virtual bool search(std::set<string>& result, const QString& pattern, 
		bool searchDescr) const;
	//@}
	/** @brief Searches searchPattern within sourceString. 
	  *
	  * If searching for whole words only, any non alphabetic character is considered 
	  * to be a word boundary.
	  *	  
	  * @param text the string to search in
	  * @param searchPattern the string to search for
	  * @param caseSensitive if true, the search is case sensitive.
	  * @param wholeWords if true, searching only for whole words within 
	  * sourceString to match the search pattern. 
	  * 
	  * @returns true if the search was successful, else false
	  */
	/// @todo add test case
	static bool searchString(const QString& text, const QString& searchPattern, bool caseSensitive, bool wholeWords);
};

}

#endif	// 
