#ifndef __APTSEARCHPLUGIN_H_2004_06_21
#define __APTSEARCHPLUGIN_H_2004_06_21

#include <string>

#include <QObject>
#include <QStringList>


// NPlugin
#include <searchplugin.h>
#include <scoreplugin.h>

class QTimer;
class QStatusBar;

class AptSearchPluginShortInputWidget;

using namespace std;

namespace NApt
{
	class IAptSearch;
	class IPackageDB;
	class AptSearchScoreCalculationStrategy;
}

namespace NPlugin
{

// Class AptSearchPlugin
// 
// 
class AptSearchPlugin : public SearchPlugin, public ScorePlugin
{
	Q_OBJECT
	const QString _title;
	const QString _briefDescription;
	const QString _description;
	/** @brief This holds the result of the currently active search. */
	std::set<string> _searchResult;
	/** @brief This holds a link to the manager which manages the plugin. */
	IProvider* _pProvider;
	/** @brief Holds a pointer to the main widgets status bar. */
	QStatusBar* _pStatusBar;
	/** @brief This timer is used to delay the evaluation of the search a little 
	  * to avoid unneccessary operations e.g. on text input. */
	QTimer* _pDelayTimer;
	
	/** @brief Holds the strategy object used to calculate the scores. */
	NApt::AptSearchScoreCalculationStrategy* _pScoreCalculationStrategy;
	/** @brief This is the delay time in ms the delay timer waits for another input. */
	uint _delayTime;
	/** @brief The cache access data structure this plugin uses to access the apt database. */
//	const CacheAccess& _ca;
	/** @brief The short widget used for getting the user input. */
	AptSearchPluginShortInputWidget* _pShortInputWidget;
	
	/** @brief Holds the interface used for searching. */
	NApt::IAptSearch* _pAptSearch;
	/** @brief Holds the interface used to access the package database directly. */
	NApt::IPackageDB* _pPackageDb;
	/** @brief Holds the patterns that must be contained in the text to be searched. */
	QStringList _includePatterns;
	/** @brief Holds the patterns that must not be contained in the text to be searched. */
	QStringList _excludePatterns;
protected Q_SLOTS:
	/** @brief Evaluates the search as specified in the input widgets. */
	virtual void evaluateSearch();
	/** @brief Called whenever the text of the widget where to insert the 
	  * search pattern changes. */
	void onInputTextChanged(const QString&);
	/** @brief Slot that calls clearSearch() */
	void onClearSearch()	{ clearSearch(); };
protected:
	/** @brief Parses the handed search expression. 
	  *
	  * It fills #_includePatterns and #_excludePatterns;
	  */
	void parseSearchExpression(const QString& expression);
	
public:
	static const QString PLUGIN_NAME;
	AptSearchPlugin(NApt::IAptSearch* pAptSearch, NApt::IPackageDB* pPackageDb);
	virtual ~AptSearchPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	/**
	  * 
	  * @param pluginManager the search window this plugin is added to
	  */
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const { return _title; };
	virtual QString briefDescription() const { return _briefDescription; };
	virtual QString description() const { return _description; };
	//@}
	/** @name SearchPlugin interface
	  * 
	  * Implementation of the SearchPlugin interface
	  */
	//@{
	/** @brief This plugin offers no input widgets. */
	virtual QWidget* inputWidget() const { return 0; };
	/** @brief This plugin offers no input widgets.  */
	virtual QString inputWidgetTitle() const { return _emptyString; };
	virtual QWidget* shortInputAndFeedbackWidget() const;
	virtual void clearSearch();
	virtual bool usesFilterTechnique() const { return false; };
	virtual const std::set<string>& searchResult() const	{ return _searchResult; };
	virtual bool filterPackage(const string&) const	{ return true; };
	virtual bool isInactive() const;
	virtual uint searchPriority() const { return 1; };
	//@}
	
	/** @name ScorePlugin interface
	  * 
	  * Implementation of the ScorePlugin interface
	  */
	//@{
	virtual map<string, float> getScore(const set<string>& packages) const;
	virtual bool offersScore() const { return !_includePatterns.isEmpty(); }
	//@}

	
	/** @brief Returns the list of patterns currently searched for by the AptSearchPlugin.
	  *
	  * @returns the list of patterns currently searched for or an empty list if no searched
	  * is active
	  */
	QStringList searchPatterns();

};

}	// namespace NPlugin


#endif //	__APTSEARCHPLUGIN_H_2004_06_21

