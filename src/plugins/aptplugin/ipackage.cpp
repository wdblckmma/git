//
// C++ Implementation: ipackage
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipackage.h"

namespace NApt {

IPackage::IPackage()
{
}


IPackage::~IPackage()
{
}

IPackage::BorderList IPackage::getPackageList(const QString& s)
{
	BorderList result;
	if (s.isEmpty())
		return result;
	pair<uint, uint> currentPair;
	// inWord and inPar (in paranthesis) can never both be true
	bool inWord=true;
	uint inPar=0;
	// a simple parser...
	currentPair.first=0;
	for (int i=0; i<s.length(); ++i)
	{
		if (inWord)
		{
			if ( !s[i].isSpace() && s[i]!=',' && s[i]!='(')	// we are still in the word
				continue;
			// we have reached the first char after a word
			currentPair.second=i;
			result.push_back(currentPair);
			inWord=false;
			if (s[i]=='(')
				++inPar;
		}
		else
		{
			if (inPar != 0)
			{
				if (s[i]=='(')	// this should not happen cause we should not have nested parantheses
					++inPar;
				if (s[i]==')')
					--inPar;
			}
			else
			{
				if (s[i]=='(')
					++inPar;
				if (s[i].isLetterOrNumber() || s[i]=='-' || s[i]=='_')	// we have reached a new word
				{
					currentPair.first=i;
					inWord=true;
				}
			}
		}
	}
	// if we finished with a word
	if (inWord)
	{
		currentPair.second=s.length();
		result.push_back(currentPair);
	}
	return result;
}



}
