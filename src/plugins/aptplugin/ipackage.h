//
// C++ Interface: ipackage
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NAPT_IPACKAGE_H_2005_12_26
#define __NAPT_IPACKAGE_H_2005_12_26

#include <list>

#include <qstring.h>

using namespace std;

namespace NApt 
{

/**
@author Benjamin Mesing
*/
class IPackage
{


public:
	enum InstalledState
	{
		NOT_INSTALLED,
		UPGRADABLE,
		INSTALLED
	};
	IPackage();
	virtual ~IPackage();
	virtual QString name() const = 0;
	virtual QString essential() const = 0;
	virtual QString priority() const = 0;
	virtual QString section() const = 0;
	virtual QString installedSize() const = 0;
	virtual QString maintainer() const = 0;
	virtual QString architecture() const = 0;
	virtual QString source() const = 0;
	virtual QString version() const = 0;
	virtual QString replaces() const = 0;
	virtual QString provides() const = 0;
	virtual QString preDepends() const = 0;
	virtual QString depends() const = 0;
	virtual QString recommends() const = 0;
	virtual QString suggests() const = 0;
	virtual QString conflicts() const = 0;
	virtual QString filename() const = 0;
	virtual QString size() const = 0;
	virtual QString md5sum() const = 0;
	virtual QString conffiles() const = 0;
	virtual QString description() const = 0;
	virtual InstalledState installedState() const = 0;
	virtual QString installedVersion() const = 0;
	virtual QString homepage() const = 0;
	
	/** This should be filled with the first line of the description. */
	virtual QString shortDescription() const = 0;
	virtual uint getSize() const = 0;
	virtual uint getInstalledSize() const = 0;


	typedef list< pair<uint, uint> > BorderList;
	/** @brief This function returns a list of the packages contained in the string. 
		*
		* It is based on some simple parsing rules and might fail if the text is not
		* formatted as expected. See the source for details (the list is assumed to
		* have the form like the dependencies)
		* @returns a list of the packages in the current string. The pairs contain
		* the first char and the one after the last for the package.
		*/
	static BorderList getPackageList(const QString& s);


};

}

#endif	// __NAPT_IPACKAGE_H_2005_12_26
