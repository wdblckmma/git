//
// C++ Implementation: aptpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "aptpluginfactory.h"

#include <iprovider.h>
#include <iprogressobserver.h>

// plugins offered
#include "aptsearchplugin.h"
#include "aptactionplugin.h"
#include "packagestatusplugin.h"
#include "packagedescriptionplugin.h"
#include "installedversionplugin.h"
#include "availableversionplugin.h"

namespace NPlugin {

AptPluginFactory::AptPluginFactory(IProvider*, IAptMediator* pMediator, 
	NApt::IPackageDB* pPackageDB, NApt::IAptSearch* pAptSearch)
 : IPluginFactory()
{
	qDebug("Constructing AptPluginFactory");
	_pPackageDB = pPackageDB;
	_pAptSearch = pAptSearch;
	_pMediator = pMediator;
}


AptPluginFactory::~AptPluginFactory()
{
	qDebug("Deleting AptPluginFactory");
}


Plugin* AptPluginFactory::createPlugin(const string& name) const
{
	if (name=="AptSearchPlugin")
	{
		return new AptSearchPlugin(_pAptSearch, _pPackageDB); 
	}
	if (name=="AptActionPlugin")
	{
		return new AptActionPlugin(_pMediator);
	}
	if (name=="PackageStatusPlugin")
	{
		return new PackageStatusPlugin(_pPackageDB);
	}
	if (name=="PackageDescriptionPlugin")
	{
		return new PackageDescriptionPlugin(_pPackageDB, _pMediator);
	}
	if (name=="InstalledVersionPlugin")
	{
		return new InstalledVersionPlugin(_pPackageDB);
	}
	if (name=="AvailableVersionPlugin")
	{
		return new AvailableVersionPlugin(_pPackageDB);
	}
	return 0;
}

};
