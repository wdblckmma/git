#ifndef __PACKAGEDESCRIPTIONPLUGIN_H_2004_06_24
#define __PACKAGEDESCRIPTIONPLUGIN_H_2004_06_24

#include <utility>

#include <QObject>
#include <qstring.h>

#include <informationplugin.h>
#include <shortinformationplugin.h>

#include "ipackage.h"


class QTextBrowser;

using namespace std;

namespace NApt
{
	class IPackageDB;
}

namespace NPlugin
{

class IProvider;
class IAptMediator;

class PackageDescriptionPlugin : public QObject, public InformationPlugin, public ShortInformationPlugin
{
	Q_OBJECT
	QTextBrowser* _pDescriptionView;
	NApt::IPackageDB* _pPackageDB;
	IProvider* _pProvider;
	/** Holds the mediator object to allow the plugins to communicate with each other. */
	IAptMediator* _pMediator; 
	static const QString _emptyString;
public:
	static const QString PLUGIN_NAME;
	/**
	  * @param pPackageDB package database to be used
	  */
	PackageDescriptionPlugin(NApt::IPackageDB* pPackageDB, IAptMediator* pMediator);
	~PackageDescriptionPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	/** @returns &quot;PackageDescriptionPlugin&quot; */
	virtual QString title() const { return tr("PackageDescriptionPlugin"); };
	/// @todo to be implemented
	virtual QString briefDescription() const { return ""; };
	/// @todo to be implemented
	virtual QString description() const { return ""; };
	//@}
	
	/** @name InformationPlugin interface
	  * 
	  * Implementation of the InformationPlugin interface
	  */
	//@{
	virtual uint informationPriority() const	{ return 0; }
	/** @returns a widget which shows a description of this package. */
	virtual QWidget* informationWidget() const;
	/** @returns &quot;Description&quot; */
	virtual QString informationWidgetTitle() const;
	virtual void updateInformationWidget(const string& package);
	virtual void clearInformationWidget();
	/** This plugin offers an information text. */
	virtual bool offersInformationText() const { return true; };
	virtual QString informationText (const string& package);
	//@}
	
	/** @name ShortInformationPlugin interface
	  * 
	  * Implementation of the ShortInformationPlugin interface
	  */
	//@{
	virtual uint shortInformationPriority() const	{ return 7; };
	/** This returns a short description about the package.
	  * @param packageID a handle of the package to show information for */
	virtual const QString shortInformationText(const string& package);
	/** The caption for the short information is <b>Description</b>.  */
	virtual QString shortInformationCaption() const { return tr("Description"); };
	// documented in base class
	virtual int preferredColumnWidth() const { return 60; }
	//@}
	/** @brief This creates the link for the given BorderList and QString.
	  *
	  * It extracts all the names specified by the borderlist and for each checks 
	  * if they
	  * are really packages (i.e. in _pProvider->packages()) and if so creates
	  * a link for the package to itself (i.e. 
	  * <tt>&lt;a href=&quot;packagename&quot;&gt;packagename&lt;/a&gt;</tt>)
	  */
    QString createLinks( NApt::IPackage::BorderList packages, const QString & s);
};

}	// namespace NPlugin


#endif	// __PACKAGEDESCRIPTIONPLUGIN_H_2004_06_24

