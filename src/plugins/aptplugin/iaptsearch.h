//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NAPT_IAPTSEARCH_H_2005_02_20
#define __NAPT_IAPTSEARCH_H_2005_02_20

#include <string>
#include <vector>
#include <set>

using namespace std;

class QStringList;
class QString;

namespace NUtil
{
	class IProgressObserver;
}

namespace NApt {

/**
@author Benjamin Mesing
*/
class IAptSearch
{
public:
	IAptSearch();

	virtual ~IAptSearch();
	/** This exception is thrown if something is wrong with Apt.*/
 	class AptException
	{	
		/** This holds the error message. 
		  * @warning we can't use _error here because there is a MACRO in APT's "error.h" with this name!\n
		  * I <b>hate</b> Makros especially if they have such a common name!  */
		string _merror;
	public:
		AptException(const string& error)	{ _merror=error; }
		const string& error() const	{ return _merror; }
	};
	/** @brief Searches for the given patterns in the package database.
	  *
	  * For a package it is neccessary to match all expressions from the given pattern list.
	  * @param result the set where the search result should be inserted. The names of the matching packages 
	  * will be added to result.
	  * @param includePatterns list of patterns that must occur in the packages.
	  * @param excludePatterns list of patterns that must not occur in the packages.
	  * @param searchDescr defines if the description should be searched too, else only the
	  * names will be serached
	  * @returns if at least one package was found.
	  * @pre !patterns.empty()
	  */
	virtual bool search(std::set<string>& result, const QStringList& includePatterns, 
		const QStringList& excludePatterns, bool searchDescr) const = 0;
	/** @brief Searches for the given pattern in the package database.
	  * @param result the set where the search result should be inserted. The names of the matching packages 
	  * will be added to result.
	  * @param pattern the patters to be searched.
	  * @param searchDescr defines if the description should be searched too, else only the
	  * names will be serached
	  * @returns if at least one package was found.
	  */
	virtual bool search(std::set<string>& result, const QString& pattern, 
		bool searchDescr) const = 0;
		
	/** @brief This reloads the package information search upon. 
	  * @param pLoader the observer where to report the reload progress, 0 if no progress should be reported  
	  */
	virtual void reloadPackageInformation(NUtil::IProgressObserver* pObserver) = 0;
};

};

#endif	//  __NAPT_IAPTSEARCH_H_2005_02_20
