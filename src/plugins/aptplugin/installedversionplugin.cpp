//
// C++ Implementation: %{MODULE}
//
// Description:
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "installedversionplugin.h"

#include <packagenotfoundexception.h>

#include "ipackage.h"
#include "ipackagedb.h"

namespace NPlugin {

const QString InstalledVersionPlugin::PLUGIN_NAME = "InstalledVersionPlugin";


InstalledVersionPlugin::InstalledVersionPlugin(NApt::IPackageDB* pPackageDB) :
	_title(tr("Installed Version Plugin")),
	_briefDescription(tr("Shows the version of the installed package in the package list")),
	_description(tr("Shows the version of the installed package in the package list")),
	_pPackageDB(pPackageDB)
{
}


InstalledVersionPlugin::~InstalledVersionPlugin()
{
}


/////////////////////////////////////////////////////
// Plugin Interface
/////////////////////////////////////////////////////
 
void InstalledVersionPlugin::init(IProvider*)
{
}

/////////////////////////////////////////////////////
// ShortInformationPlugin Interface
/////////////////////////////////////////////////////

const QString InstalledVersionPlugin::shortInformationText(const string& package)
{
	try 
	{
		const NApt::IPackage& packageStruct(_pPackageDB->getPackageRecord(package));
        if (packageStruct.installedState() == NApt::IPackage::INSTALLED)
			return QString(packageStruct.installedVersion());
		else
			return packageStruct.installedVersion();
	}
	catch (const PackageNotFoundException& e)
	{
		return _emptyString;
	}
}

/////////////////////////////////////////////////////
// InstalledVersionPlugin functions
/////////////////////////////////////////////////////


};
