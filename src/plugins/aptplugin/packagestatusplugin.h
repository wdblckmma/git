#ifndef __PACKAGESTATUSPLUGIN_H_2004_06_21
#define __PACKAGESTATUSPLUGIN_H_2004_06_21

#include <vector>
#include <map>

#include <QObject>

#include <shortinformationplugin.h>
#include <searchplugin.h>

#include "ipackage.h"

using namespace std;

class InstalledFilterWidget;

namespace NApt
{
	class IPackageDB;
}


namespace NPlugin
{

/** @brief This Plugin offers information about the installed State of the package.
  *
  * It offers short information (an x if the package is installed and an empty string 
  * if not) and allows searching by installed status using the filter search technique.
  *
  * @author Benjamin Mesing
  */
// take care to keep the order of the subclasses (SearchPlugin must be first as it
// is the QT class
class PackageStatusPlugin : public SearchPlugin, public ShortInformationPlugin
{
	Q_OBJECT
	const QString _title;
	const QString _briefDescription;
	const QString _description;
	/** @brief This enum must always match NApt::Package::InstalledState except for ALL. */
	enum InstalledState 
	{ 
		NOT_INSTALLED = NApt::IPackage::NOT_INSTALLED, 
		UPGRADABLE = NApt::IPackage::UPGRADABLE, 
		INSTALLED = NApt::IPackage::INSTALLED, 
		ALL = 100 
	};
	/** This holds the currently selected installed state filter. */
	InstalledState _installedFilter;

	NApt::IPackageDB* _pPackageDB;
	/** The widget where to select which packages should be shown. */
	InstalledFilterWidget* _pInstalledFilterWidget;
	/** The text to mark an installed package ("x") */
	QString _installedText;
	/** The text to mark a not installed package (empty string) */
	QString _notInstalledText;
	std::set<string> _emptyOpSet;
	/** @brief This maps the installed state to how it shall be diplayed. 
	  *
	  * So _stateToText[Package::INSTALLED] returns "x" while _stateToText[Package::NOT_INSTALLED] returns an empty string
	  */
    map<NApt::IPackage::InstalledState, QString> _stateToText;

public:
	static const QString PLUGIN_NAME;
	/**
	  */
	PackageStatusPlugin(NApt::IPackageDB* pPackageDB);
	~PackageStatusPlugin() ;
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const { return _title; };
	virtual QString briefDescription() const { return _briefDescription; };
	virtual QString description() const { return _description; };
	//@}
	/** @name SearchPlugin interface
	  * 
	  * Implementation of the SearchPlugin interface
	  */
	//@{
	virtual uint searchPriority() const	{ return 2; }
	/** @brief This plugin offers no input widgets. */
	virtual QWidget* inputWidget() const { return 0; };
	/** @brief This plugin offers no input widgets.  */
	virtual QString inputWidgetTitle() const { return _emptyString; };
	virtual QWidget* shortInputAndFeedbackWidget() const;
	virtual void clearSearch();
	virtual bool usesFilterTechnique() const	{ return true; };
	virtual const std::set<string>& searchResult() const	{ return _emptyOpSet; };
	virtual bool filterPackage(const string& package) const;
	/** Returns true if ALL is selected as filter. */
	virtual bool isInactive() const;
	//@}
	
	/** @name ShortInformationPlugin interface
	  * 
	  * Implementation of the ShortInformationPlugin interface
	  */
	//@{
	virtual uint shortInformationPriority() const	{ return 2; }
	virtual const QString shortInformationText(const string& package);
	/** The caption for the short information is <b>I</b> for installed  */
	virtual QString shortInformationCaption() const { return QString("I"); };
	// documented in base class
	virtual int preferredColumnWidth() const { return 1; }
	//@}
protected:
	/** @brief Returns the state of the handed package. 
	  *
	  * Returns NApt::Package::NOT_INSTALLED if the package is not in the database. */
    NApt::IPackage::InstalledState getState(const string& package) const;
protected Q_SLOTS:
	/** Called whenever the user selects a new installed filter. 
	  * @param the item that was activated */
	void onInstalledFilterChanged(int activated);
};

}	// namespace NPlugin

#endif //	__PACKAGESTATUSPLUGIN_H_2004_06_21

