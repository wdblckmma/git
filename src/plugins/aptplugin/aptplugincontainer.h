#ifndef __APTPLUGINCONTAINER_H_2004_06_23
#define __APTPLUGINCONTAINER_H_2004_06_23

#include <qobject.h>

#include <baseplugincontainer.h>

#include "aptfrontpackagedb.h"

#include "iaptmediator.h"

#include "installationtool.h"


class QAction;

class ExtPackageDB;

using namespace std;

namespace NApplication
{
	class RunCommand;
}

namespace NApt
{
	class IAptSearch;
	class IPackageDB;
}

class AptSettingsWidget;

namespace NPlugin
{

class AptSearchPlugin;
class AptActionPlugin;
class PackageDescriptionPlugin;
class PackageStatusPlugin;
class InstalledVersionPlugin;
class AvailableVersionPlugin;

// Class AptPluginContainer
// 
// 
class AptPluginContainer : public QObject, public BasePluginContainer, public IAptMediator
{
	Q_OBJECT
	/** @brief The plugins in the container */
	//@{
	AptSearchPlugin* _pAptSearchPlugin;
	AptActionPlugin* _pAptActionPlugin;
	PackageDescriptionPlugin* _pPackageDescriptionPlugin;
	PackageStatusPlugin* _pPackageStatusPlugin;
	InstalledVersionPlugin* _pInstalledVersionPlugin;
	AvailableVersionPlugin* _pAvailableVersionPlugin;
	//@}
	/** @brief This pointer is used to run the <tt>apt-get update</tt> command. */
	NApplication::RunCommand* _pCommand;
	/** @brief This holds the interface to access the information of the packages.
	  */
	NApt::IPackageDB* _pPackageDB; 
	/** @brief This holds the interface to search the packages.
	  */
	NApt::IAptSearch* _pAptSearch; 
	/** @brief This holds a pointer to the settings widget currently active.
	  * 
	  * This pointer must not be deleted and should only be used in applySettings(). 
	  */
	AptSettingsWidget* _pSettingsWidget;
	/** @brief Holds the installation tool to be used for installation (APT). */
	NApt::InstallationTool _installationTool;
public:
	/**
	  * Default Constructor
	  */
	AptPluginContainer();
	virtual ~AptPluginContainer();
	/** @name PluginContainer Interface
	  *
	  * These functions implement the PluginContainer interface.
	  */
	//@{
	// documened in base class
	virtual bool init(IProvider* pProvider);
	/** @brief This gets a plugin with the given name. The plugin is not 
	  * initialized so call Plugin::init() for it.
	  *
	  * Accepted names are: 
	  * \li AptSearchPlugin
	  * \li PackageDescriptionPlugin
	  * \li PackageStatusPlugin
	  *
	  * @see PluginContainer::requestPlugin()
	  */
//	virtual Plugin* requestPlugin(const QString& name);
	/** @returns "aptplugin" */
	virtual string name() const {	return "aptplugin"; };
	virtual QString title() const	{ return QString("Apt Plugins"); };
	virtual QWidget* getSettingsWidget(QWidget* pParent);
	virtual void applySettings();
	//@}
	
	/** @name BasePluginContainer Overloads
	  *
	  * These functions implement saving and loading of the container settings
	  * as described in BasePluginContainer.
	  */
	//@{
	virtual QDomElement loadContainerSettings(const QDomElement source);
	virtual void saveContainerSettings(NXml::XmlData& outData, QDomElement parent) const;
	//@}
	
	/** @name IAptMediator Overloads
	  *
	  */
	// documened in base class
	//@{
	virtual void setInstallationTool(NApt::InstallationTool tool);
	virtual QString installationToolCommand();
	virtual QStringList searchPatterns();
	virtual void updateAptDatabase();
	virtual void reloadAptDatabase();
	//@}
protected Q_SLOTS:
	/** @brief This function will be called if the update finished. 
	  *
	  * It will update the database used. 
	  */
	void onAptUpdateFinished();
};

}	// namespace NPlugin

#endif //	__APTPLUGINCONTAINER_H_2004_06_23

