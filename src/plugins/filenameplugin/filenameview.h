//
// C++ Interface: filenameview
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __FILENAMEVIEW_H_2005_08_26
#define __FILENAMEVIEW_H_2005_08_26

//Added by qt3to4:

#include <qwidget.h>
#include <ui_filenameview.h>

#include <processcontainer.h>

class QWidget;
class QMouseEvent;

namespace NPlugin
{
	class IProvider;
}


/** @brief This subclasses the FilenameView to allow easy extension.
  *
  * It is used to display a set of files to the user, or alternatively an error message.
  * Use the addEntry() function to add a new file to the list of files
  * and the setErrorMessage() to set an error message. If an error message is displayed,
  * the entries will not be shown. 
  *
  * Additionally a context menu can be shown which allows the user to copy the 
  * file path or to view the file using the <tt>see</tt> command. For <tt>see</tt>
  * the function viewFile(QString) is called.
  *
  * The FilenameView is capable of filtering the items according to the input in a textbox
  * on top of the filelist.
  * 
  * @author Benjamin Mesing
  */
class FilenameView : public QWidget, public Ui::FilenameView
{
Q_OBJECT
	static QString _seeCommand;
	NApplication::ProcessContainer _processContainer;
	NPlugin::IProvider* _pProvider;
	bool _filterTextEmpty;
	QString _errorMessage;
	QStringList _entries;
	
	/** @brief This map contains all the <tt>see</tt> commands currently running. 
	  *
	  * It maps them to the file which they should display and if an attempt was already made,
	  * to view the file as text/plain.
	  */
	map<QProcess*, pair<QString, bool> > _seeCommands;
public:
	FilenameView(QWidget *parent, const char *name, NPlugin::IProvider* pProvider);
	~FilenameView();
	/** @brief Clears the content of the widget excluding the filter.
	  *
	  * The file list and the error message will be cleared.
	  */
	void clear();
	/** @brief Sets the search pattern for the filter. 
	* 
	* The view will be updated accordingly. */
	void setFilterText( const QString & pattern );
	/** Adds an entry to the widget. */
	void addEntry( const QString & entry );
	/** @brief Returns if the filelist has at least one entry. */
	bool hasEntry() { return !_entries.empty(); }
	/** Shows the entries which shall be shown according to the filter currently active. */
	void updateView();
	/** @brief Call this function if you want to indicate an error.
	* 
	* The error message will be shown and not affected by the filter.
	* The error state will cleared at next call of the clear() function. */
	void setErrorMessage( const QString & errorMessage );
	/** @brief This adds an item to the filename view.
	  *
	  * The item is displayed only if it matches the active filter. 
	  */
	void insertItem( const QString & entry );
	/** Returns the text of all visible items in a string list.
	*
	* Each string will contain one line, the first item in the stringlist corresponds to the first in
	* the listview.
	*
	* If the error state is set, an emtpy list will be returned.
	*/
	QStringList getAllVisibleItems();
	/** @brief Returns if the file is viewable (i.e. no directory and readable). */
	bool isFileViewable(QString filename);
	/** @brief Views the file using the <tt>see</tt> command. 
	 * 
	 * Reports an error to the provider if the viewing fails.
	 */
	virtual void viewFile(QString filename);
	/** @brief Enables or disables the "Show" button. */
	void setShowButtonEnabled(bool enabled);
protected Q_SLOTS:
	/** @brief Called whenever one of the processes (executing <tt>see</tt>) exited.
	  *
	  * It tries to detect if the process exited successfully and if not it 
	  * tries to relaunch see handing the mime-type text/plain as fallback.
	  * 
	  * If the process exited successfully or a try with text/plain was already
	  * attempted, the process will be deleted.
	  */
	virtual void onProcessExited(QProcess* pProcess);
	void on__pFilenameView_itemDoubleClicked(QListWidgetItem* pItem);
	void on__pFilenameView_customContextMenuRequested(const QPoint & pos);
	/** @brief Filters the view by the given pattern. 
	*
	* Called whenever the text in the filterinput changed. */
	void on__pFilterInput_textChanged( const QString & pattern );
signals:
	void showRequested();

};

#endif	// __FILENAMEVIEW_H_2005_08_26
