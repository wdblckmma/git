//
// C++ Interface: filenamepluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __FILENAMEPLUGINFACTORY_H_2004_09_10
#define __FILENAMEPLUGINFACTORY_H_2004_09_10

#include <ipluginfactory.h>

namespace NPlugin {

/**
  * @author Benjamin Mesing
  */
class FilenamePluginFactory : public IPluginFactory
{
private:
	FilenamePluginFactory();
	static FilenamePluginFactory* _pInstance;
public:
	~FilenamePluginFactory();
	/** @name IPluginFactory interface */
	//@{
	virtual Plugin* createPlugin(const string& name) const;
	//@}
	/** @brief Returns the single instance of this class. */
	static FilenamePluginFactory* getInstance();
};

};

#endif	// __FILENAMEPLUGINFACTORY_H_2004_09_10
