//
// C++ Interface: filenameactionplugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_FILENAMEACTIONPLUGIN_H_2005_09_01
#define __NPLUGIN_FILENAMEACTIONPLUGIN_H_2005_09_01

#include <QObject>

#include <actionplugin.h>

namespace NPlugin {

/**
@brief Offers the actions related to the filename plugin

Currently the only action offered is used to perform apt-file update.
 * @author Benjamin Mesing
  */
class FilenameActionPlugin : public QObject, public ActionPlugin
{
	const QString _title;
	const QString _briefDescription;
	const QString _description;
	/** @brief Action used to update the debtags database. */
	Action* _pAptFileUpdateAction;
	Action* _pSeparatorAction;
public:
	static const QString PLUGIN_NAME;
	FilenameActionPlugin();
	~FilenameActionPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider*) {};
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const { return _title; };
	virtual QString briefDescription() const { return _briefDescription; };
	virtual QString description() const { return _description; };
	//@}
	/** @brief Returns the QAction object for the #_pAptFileUpdateAction. */
	QAction* qAptFileUpdateAction() const { return _pAptFileUpdateAction->action(); }

	/** @name ActionPlugin Interface
	  * 
	  * Implementation of the ActionPlugin Interface.
	  */
	//@{
	virtual vector<Action*> actions() const;
	//@}

};

}

#endif	// __NPLUGIN_FILENAMEACTIONPLUGIN_H_2005_09_01
