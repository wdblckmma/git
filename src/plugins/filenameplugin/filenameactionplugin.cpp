//
// C++ Implementation: filenameactionplugin
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "filenameactionplugin.h"

#include <QAction>

namespace NPlugin 
{

const QString FilenameActionPlugin::PLUGIN_NAME = "FilenameActionPlugin";


FilenameActionPlugin::FilenameActionPlugin() :
 	_title("Filename-Action Plugin"),
	_briefDescription("Offers the menu and toolbar entries"),
	_description("This plugin offers the menu and toolbar entries for the Filename plugin. "
		"Currently this is only the debtags update entry.")
{
	QAction* pQAptFileUpdateAction = new QAction(QObject::tr("Update File Database"), this);
	pQAptFileUpdateAction->setStatusTip(tr("Calls \"apt-file update\" updating the file database"));
	_pAptFileUpdateAction = new Action(pQAptFileUpdateAction, false, "System");
	
	QAction* pQSeparatorAction = new QAction(this);
	pQSeparatorAction->setSeparator(true);
	_pSeparatorAction = new Action(pQSeparatorAction, false, "System");
}


FilenameActionPlugin::~FilenameActionPlugin()
{
	delete _pAptFileUpdateAction;
	delete _pSeparatorAction;
}

vector<Action*> FilenameActionPlugin::actions() const
{
	vector<Action*> actions;
	actions.push_back(_pSeparatorAction);
	actions.push_back(_pAptFileUpdateAction);
	return actions;
}



}
