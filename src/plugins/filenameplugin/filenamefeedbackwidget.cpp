//
// C++ Implementation: filenamefeedbackwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "filenamefeedbackwidget.h"

FilenameFeedbackWidget::FilenameFeedbackWidget(QWidget *parent, const char *name)
 : QWidget(parent), Ui::FilenameFeedbackWidget()
{
	setObjectName(name);
	setupUi(this);
}



FilenameFeedbackWidget::~FilenameFeedbackWidget()
{
}


void FilenameFeedbackWidget::setClearButton(QPushButton* pButton, int index)
{
	delete(_pClearButton);
	_pClearButton = pButton;
	_pInputLayout->insertWidget(index,_pClearButton);
}

