//
// C++ Implementation: filenameplugincontainer
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "filenameplugincontainer.h"

#include <qaction.h>
#include <QMainWindow>
#include <qmessagebox.h>
#include <qobject.h>



// NApplication
#include <applicationfactory.h>
#include <runcommand.h>

// NPluign
#include <iprovider.h>
#include "filenameactionplugin.h"
#include "filenameplugin.h"

#include <helpers.h>
#include <globals.h>

using namespace std;

extern "C" 
{ 
	NPlugin::PluginContainer* new_filenameplugin() 
	{
		return new NPlugin::FilenamePluginContainer;; 
	} 
	
	NPlugin::PluginInformation get_pluginInformation()
	{
		return NPlugin::PluginInformation("filenameplugin", toString(NPackageSearch::VERSION), "Benjamin Mesing");
	} 
}


namespace NPlugin 
{

FilenamePluginContainer::FilenamePluginContainer()
{
	addPlugin("FilenamePlugin");
	addPlugin("FilenameActionPlugin");
	_pCommand = 0;
}


FilenamePluginContainer::~FilenamePluginContainer()
{
	unloadAllPlugins();
	delete _pCommand;
}


/////////////////////////////////////////////////////
// Plugin Container Interface
/////////////////////////////////////////////////////

bool FilenamePluginContainer::init(IProvider* pProvider)
{
	BasePluginContainer::init(pProvider, FilenamePluginFactory::getInstance());
	requestPlugin("FilenamePlugin");
	_pFilenameActionPlugin = dynamic_cast<FilenameActionPlugin*>(requestPlugin("FilenameActionPlugin"));
	connect( _pFilenameActionPlugin->qAptFileUpdateAction(), SIGNAL(triggered(bool)), SLOT(onAptFileUpdate()) );
	return true;
}

QString FilenamePluginContainer::title() const
{
	return QObject::tr("Filename Plugins");
}

/////////////////////////////////////////////////////
// Helper Methods
/////////////////////////////////////////////////////


void FilenamePluginContainer::onAptFileUpdate()
{
	provider()->setEnabled(false);
	NApplication::ApplicationFactory* pFac = NApplication::ApplicationFactory::getInstance();
	_pCommand = pFac->getRunCommand("AptFileUpdateProcess");	// get a suitable user interface for the update
	connect(_pCommand, SIGNAL(quit()), SLOT(onAptFileUpdateFinished()) );
	_pCommand->addArgument("/usr/bin/apt-file");
	_pCommand->addArgument("update");
	try 
	{
		if ( !_pCommand->startAsRoot() )
		{
			provider()->reportError( tr("Command not executed"), tr("For an unknwon reason, the command could "
				"not be executed.") );
			delete _pCommand;
			_pCommand = 0;
			provider()->setEnabled(true);
		}
	}
	catch (const NException::RuntimeException& e)
	{
		provider()->reportError(tr("Command not executed"), toQString(e.description()));
		delete _pCommand;
		_pCommand = 0;
		provider()->setEnabled(true);
	}
}

void FilenamePluginContainer::onAptFileUpdateFinished()
{
	if (!_pCommand->processExitedSuccessful())	// if the command was aborted
	{
		provider()->reportWarning(
			tr("Update not successfully completed"), 
			tr("The apt-file update was not completed successfully.<br>"
			"The database might be broken, rerun <tt>apt-file update</tt> to fix this.")
		);
	}
	delete _pCommand;
	_pCommand = 0;
	provider()->setEnabled(true);
}


};
