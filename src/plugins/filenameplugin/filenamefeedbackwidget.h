//
// C++ Interface: filenamefeedbackwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __FILENAMEFEEDBACKWIDGET_H_2005_07_26
#define __FILENAMEFEEDBACKWIDGET_H_2005_07_26

#include <qwidget.h>
#include <ui_filenamefeedbackwidget.h>

/**
@author Benjamin Mesing
*/
class FilenameFeedbackWidget : public QWidget, public Ui::FilenameFeedbackWidget
{
Q_OBJECT
public:
	FilenameFeedbackWidget(QWidget *parent = 0, const char *name = 0);

	~FilenameFeedbackWidget();
	/** Sets the push button used in this dialog.
	  *
	  * @param index the position where the button is placed (0 is before the filename display, 
	  * 1 is behind it). 
	  */
	void setClearButton(QPushButton* pButton, int index);
};

#endif	// __FILENAMEFEEDBACKWIDGET_H_2005_07_26
