//
// C++ Implementation: filenamepluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "filenamepluginfactory.h"

#include "filenameactionplugin.h"
#include "filenameplugin.h"

namespace NPlugin 
{

FilenamePluginFactory* FilenamePluginFactory::_pInstance = 0;

FilenamePluginFactory::FilenamePluginFactory()
{
}


FilenamePluginFactory::~FilenamePluginFactory()
{
}


Plugin* FilenamePluginFactory::createPlugin(const string& name) const
{
	if (name=="FilenamePlugin")
	{
		return new FilenamePlugin();
	}
	if (name=="FilenameActionPlugin")
	{
		return new FilenameActionPlugin();
	}
	else
		return 0;
}


FilenamePluginFactory* FilenamePluginFactory::getInstance()
{
	if (_pInstance == 0)
		_pInstance = new FilenamePluginFactory;
	return _pInstance;
}


};
