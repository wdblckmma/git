//
// C++ Interface: filenameplugincontainer
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __FILENAMEPLUGINCONTAINER_H_2004_07_17
#define __FILENAMEPLUGINCONTAINER_H_2004_07_17

#include <qobject.h>

#include <baseplugincontainer.h>

#include "filenamepluginfactory.h"

class QAction;

namespace NApplication
{
	class RunCommand;
}


namespace NPlugin 
{

class FilenamePlugin;
class FilenameActionPlugin;


/**
  * @author Benjamin Mesing
  */
class FilenamePluginContainer :  public QObject, public BasePluginContainer
{
	Q_OBJECT
public:
	FilenamePluginContainer();
	virtual ~FilenamePluginContainer();
	/** @name PluginContainer Interface
	  *
	  * These functions implement the PluginContainer interface.
	  */
	//@{
	virtual bool init(IProvider* pProvider);
	/** This gets a plugin with the given name.
	  *
	  * Accepted names are: 
	  * \li "FilenamePlugin"
	  * @see PluginContainer::requestPlugin()
	  */
	/** @returns "filenameplugin" */
	virtual string name() const {	return "filenameplugin"; };
	virtual QString title() const;
	//@}
protected slots:
	/** Launches the apt-file update command. 
	  * @pre _pCommand == 0 */
	void onAptFileUpdate();
	/** @brief This function will be called if the update finished. 
	  * @post _pCommand == 0 */
	void onAptFileUpdateFinished();
private:
	/** This pointer is used to run the <tt>apt-file update</tt> command. */
	NApplication::RunCommand* _pCommand;
	FilenameActionPlugin* _pFilenameActionPlugin;
};

};

#endif	// __FILENAMEPLUGINCONTAINER_H_2004_07_17
