//
// C++ Interface: filenamesearchinput
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __FILENAMESEARCHINPUT_H_
#define __FILENAMESEARCHINPUT_H_

#include <qwidget.h>
#include "ui_filenamesearchinput.h"

/**
@author Benjamin Mesing
*/
class FilenameSearchInput : public QWidget, public Ui::FilenameSearchInput
{
Q_OBJECT
public:
	FilenameSearchInput(QWidget *parent = 0);
	~FilenameSearchInput();
};

#endif	// 
