//
// C++ Interface: choosentagsdisplay
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __CHOOSENTAGSDISPLAY_H_
#define __CHOOSENTAGSDISPLAY_H_

#include <qwidget.h>
#include <ui_choosentagsdisplay.h>

/**
@author Benjamin Mesing
*/
class ChoosenTagsDisplay : public QWidget, public Ui::ChoosenTagsDisplay
{
Q_OBJECT
public:
    ChoosenTagsDisplay(QWidget *parent = 0, const char *name = 0);

    ~ChoosenTagsDisplay();

};

#endif	// 
