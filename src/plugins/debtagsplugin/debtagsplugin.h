#ifndef __DEBTAGSPLUGIN_H_2004_06_21
#define __DEBTAGSPLUGIN_H_2004_06_21

#include <QObject>
#include <QString>

#include <string>

#include <searchplugin.h>
#include <informationplugin.h>
#include <debtagsplugincontainer.h>


#include "tagchooserwidget.h"

class QAbstractItemView;
class QPoint;

namespace NWidgets 
{
	class SelectionInputAndDisplay;
}

namespace NTagModel
{
	class VocabularyModel;
}

using namespace std;

namespace NPlugin
{

/** This plugin offers search by tags  
  *
  * @author Benjamin Mesing
  */
class DebtagsPlugin : public SearchPlugin, public InformationPlugin 
{
	Q_OBJECT

	TagChooserWidget* _pChooserWidget;
	
	NWidgets::SelectionInputAndDisplay* _pTagSelection;
	
	/** @brief This holds a link to the manager which manages the plugin.
	  *
	  * Acquaintance relation */
	IProvider* _pProvider;
	/** This manages the include widgets (but not the labels belonging to them. */
// 	NWidgets::SelectionInputAndDisplay* _pIncludeSelection;
	/** The container which holds this plugin. */
	const DebtagsPluginContainer& _container;
	/** Holds if the search is currently inactive. */
	bool _isInactive;
	/** This holds the search result for the current search */
	std::set<string> _searchResult;
public:
	static const QString PLUGIN_NAME;
	/** Create this plugin as plugin in container.
	  *
	  * @param container the container which contains this plugin. 
	  */
	DebtagsPlugin(const DebtagsPluginContainer& container);;
	virtual ~DebtagsPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface 
	  */
	//@{
	/** @brief Initializes the plugin. 
	  * 
	  * It does not load the debtags vocabulary if _pColl or _pPackageCollection are 0.*/
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const;
	virtual QString briefDescription() const;
	virtual QString description() const;
	//@}
	///@todo try to remove malformed descriptions
	/** @name SearchPlugin interface
	  * 
	  * Implementation of the SearchPlugin interface
	  */
	//@{
	virtual uint searchPriority() const { return 5; };
	/** @brief Returns a widget where you can select the debtags you want to in- and exclude. */
	virtual QWidget* inputWidget() const;	
	/** @brief Returns &quot;Debtags&quot;.  */
	virtual QString inputWidgetTitle() const	{ return "Debtags"; };
	virtual QWidget* shortInputAndFeedbackWidget() const;
	virtual void clearSearch();
	virtual bool usesFilterTechnique() const	{ return false; };
	virtual const std::set<string>& searchResult() const;
	/** This plugin does not use the filter technique. */
	virtual bool filterPackage(const string&) const	{ return true; };
	virtual bool isInactive() const	{ return _isInactive; };
	//@}
	
	/** @name InformationPlugin interface
	  * 
	  * Implementation of the InformationPlugin interface
	  */
	//@{
	virtual uint informationPriority() const	{ return 5; };
	/** This plugin does not offer a separate information widget. */
	virtual QWidget* informationWidget() const	{ return 0; };
	/** This plugin does not offer a separate information widget. */
	virtual QString informationWidgetTitle() const	{ return _emptyString; };
	/** This plugin does not offer a separate information widget. */
	virtual void updateInformationWidget(const string&) {};
	/** This plugin does not offer a separate information widget. */
	virtual void clearInformationWidget() {};
	/** This plugin offers an information text. */
	virtual bool offersInformationText() const	{ return true; };
	/** Returns a string which lists the tags for the requested package. */
	virtual QString informationText (const string& package);
	//@}
protected Q_SLOTS: 
	/** @brief This shows or hides the exclude widgets.
	  *
	  * @param display decides if to show (true) or hide (false) the elements. */
	void showExcludeWidgets(bool display);
	/** This evaluates the current search */
	void evaluateSearch();
	/** @brief This enables/ disables the visible widgets. */
	void setWidgetsEnabled(bool enabled);

	/** @brief Called whenever the selection of tags was changed. 
	  *
	  * If no tags are selected any more, the widget displaying the selected 
	  * tags will be hidden.
	  */
	void onTagSelectionChanged();

	/** @brief This shows or hides the exclude display elements.
	  *
	  * @param display decides if to show (true) or hide (false) the elements. */
//	void showExcludeDisplay(bool display);
	/** @brief This shows or hides the exclude input elements.
	  *
	  * @param display decides if to show (true) or hide (false) the elements. */
//	void showExcludeInput(bool display);
protected:
	/** @brief Returns a pointer to the VocabularyModel instance used. */
	NTagModel::VocabularyModel* vocabularyModel();
};

}	// namespace NPlugin

#endif //	__DEBTAGSPLUGIN_H_2004_06_21

