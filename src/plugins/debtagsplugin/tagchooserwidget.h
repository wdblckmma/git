//
// C++ Interface: tagchooserwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __TAGCHOOSERWIDGET_H_
#define __TAGCHOOSERWIDGET_H_

#include <qwidget.h>
#include <ui_tagchooserwidget.h>

/**
@author Benjamin Mesing
*/
class TagChooserWidget : public QWidget, public Ui::TagChooserWidget
{
Q_OBJECT
public:
    TagChooserWidget(QWidget *parent = 0, const char *name = 0);

    ~TagChooserWidget();

};

#endif	// 
