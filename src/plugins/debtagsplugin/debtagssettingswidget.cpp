//
// C++ Implementation: debtagssettingswidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "debtagssettingswidget.h"

#include <wibble/operators.h>
/*
#include <ept/configuration/apt.h>
#include <ept/cache/apt/packages.h>
#include <ept/cache/debtags/vocabulary.h>
*/

// NTagModel
#include "vocabularymodel.h"
#include "vocabularymodelrole.h"
#include "filterhiddenproxymodel.h"

#include <helpers.h>

#include "exception.h"

DebtagsSettingsWidget::DebtagsSettingsWidget(NTagModel::VocabularyModel* pModel, 
	QWidget *parent, const char *name)
 : QWidget(parent), Ui::DebtagsSettingsWidget()
{
	using namespace wibble::operators;
	typedef NTagModel::FilterHiddenProxyModel FilterHiddenProxyModel;

	if (name)
		setObjectName(name);
	setupUi(this);
	_pModel = pModel;
	
	// will be garbage collected
	_pShownModel = new FilterHiddenProxyModel(false, this);
	_pHiddenModel = new FilterHiddenProxyModel(true, this);

	_pShownModel->setSourceModel(_pModel);
	_pHiddenModel->setSourceModel(_pModel);
 	_pShownModel->setDynamicSortFilter(true);
 	_pHiddenModel->setDynamicSortFilter(true);

	_pShownFacetsList->setModel(_pShownModel);
	_pHiddenFacetsList->setModel(_pHiddenModel);
	
}


DebtagsSettingsWidget::~DebtagsSettingsWidget()
{
}




void DebtagsSettingsWidget::on__pAddButton_clicked()
{
	QModelIndexList selected = _pShownFacetsList->selectionModel()->selectedIndexes();
	QModelIndex proxyIndex;
	foreach(proxyIndex, selected)
	{
		QModelIndex index = _pShownModel->mapToSource(proxyIndex);
		_pModel->setData(index, true, NTagModel::HiddenRole);
	}
}

void DebtagsSettingsWidget::on__pRemoveButton_clicked()
{
	QModelIndexList selected = _pHiddenFacetsList->selectionModel()->selectedIndexes();
	QModelIndex proxyIndex;
	foreach(proxyIndex, selected)
	{
		QModelIndex index = _pHiddenModel->mapToSource(proxyIndex);
		_pModel->setData(index, false, NTagModel::HiddenRole);
	}
}


