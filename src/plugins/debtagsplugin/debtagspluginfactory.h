//
// C++ Interface: debtagspluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGINDEBTAGSPLUGINFACTORY_H_
#define __NPLUGINDEBTAGSPLUGINFACTORY_H_

#include <ipluginfactory.h>

namespace NPlugin 
{

class DebtagsPluginContainer;

/** @brief This creates the debtags plugins.
  *
  * 
  * @author Benjamin Mesing
  */
class DebtagsPluginFactory : public IPluginFactory
{
	DebtagsPluginContainer* _pContainer;
	DebtagsPluginFactory();
	static DebtagsPluginFactory* _pInstance;
public:
	~DebtagsPluginFactory();
	void setContainer(DebtagsPluginContainer* pContainer) { _pContainer = pContainer; };
	/** @name IPluginFactory interface */
	//@{
	/** @brief This creates a plugin for the given name.
	  *
	  * Accepted names are: 
	  * \li "DebtagsPlugin"
	  * \li "RelatedPlugin"
	  *
	  * @pre setContainer() must have been called or 0 will be returned.
	  */
	virtual Plugin* createPlugin(const string& name) const;
	//@}
	static DebtagsPluginFactory* getInstance();
};

};

#endif	//  __NPLUGINDEBTAGSPLUGINFACTORY_H_
