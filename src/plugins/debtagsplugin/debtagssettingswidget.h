//
// C++ Interface: debtagssettingswidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __DEBTAGSSETTINGSWIDGET_H_2005_08_28
#define __DEBTAGSSETTINGSWIDGET_H_2005_08_28

#include <set>


#include <qwidget.h>

#include <ui_debtagssettingswidget.h>

using namespace std;

namespace NTagModel
{
	class VocabularyModel;
	class FilterHiddenProxyModel;
}


/** @brief Widget used to let the user select facets to be hidden and shown.
  *
  * @author Benjamin Mesing
  */
class DebtagsSettingsWidget : public QWidget, public Ui::DebtagsSettingsWidget
{
Q_OBJECT
	typedef NTagModel::FilterHiddenProxyModel FilterHiddenProxyModel;
	NTagModel::VocabularyModel* _pModel;
	FilterHiddenProxyModel* _pShownModel;
	FilterHiddenProxyModel* _pHiddenModel;
public:
	/**
	  * @param hiddenFacets the facets which are selected to be hidden	
	  */
	DebtagsSettingsWidget(NTagModel::VocabularyModel* pModel, QWidget *parent = 0, const char *name = 0);
	~DebtagsSettingsWidget();
protected Q_SLOTS:
	/** @brief The button to add facets to the hidden facets was clicked. */
	void on__pAddButton_clicked();
	/** @brief The button to remove facets from the hidden facets was clicked. */
	void on__pRemoveButton_clicked();
signals:
	/** @brief Emitted whenever the user hid a facet. */
	void facetHidden(const string& facet);
	/** @brief Emitted whenever the user showed a facet. */
	void facetShown(const string& facet);
};

#endif	// __DEBTAGSSETTINGSWIDGET_H_2005_08_28
