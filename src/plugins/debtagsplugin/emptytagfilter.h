//
// C++ Interface: emptytagfilter
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NTAGMODEL_EMPTYTAGFILTER_H_2007_04_06
#define __NTAGMODEL_EMPTYTAGFILTER_H_2007_04_06

#include <set>

#include <QSortFilterProxyModel>

using namespace std;

namespace NPlugin
{
	class DebtagsPluginContainer;
}

namespace Xapian 
{
	class Database;
}

namespace NTagModel {

class VocabularyModel;

/** Filters out all tags, that would lead to an empty search result.
  *
  * To filter out the tags, the set of currently selected tags (_selectedTags) is considered
  * and only tags which are assigned to at least one of packages selected by that tag set are shown.
  *
  * @author Benjamin Mesing <bensmail@gmx.net>
  */
class EmptyTagFilter : public QSortFilterProxyModel
{
	Q_OBJECT
	
	typedef std::string Tag;


	/** The xapian database. */
	const Xapian::Database& _xapian;

	/** The model holding the information about the selected tags.
	  *
	  * This information is required, to be able to access the selected tags.
	  * The model is also used to access the Debtags object.
	  */
	const VocabularyModel* _pVocabularyModel;
	/** @brief This indicates if the tag selection has changed since the last calculation of the companion tags.
	  */
	mutable bool _tagSelectionHasChanged;
public:
	EmptyTagFilter(const VocabularyModel* pModel, const Xapian::Database& xapian, QObject* pParent = 0);

	virtual ~EmptyTagFilter();
	virtual bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const;

public Q_SLOTS:
	void tagSelectionChanged();

};

}



#endif
