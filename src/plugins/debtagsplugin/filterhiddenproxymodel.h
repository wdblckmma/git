//
// C++ Interface: filterhiddenproxymodel
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NTAGMODEL_FILTERHIDDENPROXYMODEL_H_2007_03_30
#define __NTAGMODEL_FILTERHIDDENPROXYMODEL_H_2007_03_30

#include <QSortFilterProxyModel>

namespace NTagModel {

/**
 *  Filters model depending on wether HiddenRole is set.
	@author Benjamin Mesing <bensmail@gmx.net>
*/
class FilterHiddenProxyModel : public QSortFilterProxyModel
{
Q_OBJECT
	/** @brief Controls whether to display only hidden items or only unhidden items.
	  *
	  * If true only hidden items will be displayed, else only unhidden.
	  */
	bool _displayHidden;
public:
	/** Creates an instance.
	  * 
	  * @param displayHidden used to set _displayHidden
	  */
	FilterHiddenProxyModel(bool displayHidden, QObject * pParent = 0);
	~FilterHiddenProxyModel();
	/** @brief Sets _displayHidden. */
	void setDisplayHidden(bool displayHidden)	{ _displayHidden = displayHidden; }
	bool filterAcceptsRow ( int sourceRow, const QModelIndex & sourceParent ) const;
};

}

#endif
