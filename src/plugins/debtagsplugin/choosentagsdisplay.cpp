//
// C++ Implementation: choosentagsdisplay
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "choosentagsdisplay.h"

ChoosenTagsDisplay::ChoosenTagsDisplay(QWidget *parent, const char *name)
 : QWidget(parent)
{
	if (name)
		setObjectName(name);
	setupUi(this);
}	


ChoosenTagsDisplay::~ChoosenTagsDisplay()
{
}


