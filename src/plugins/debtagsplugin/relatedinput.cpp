//
// C++ Implementation: relatedinput
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "relatedinput.h"

RelatedInput::RelatedInput(QWidget *parent, const char *name)
 : QWidget(parent)
{
	if (name)
		setObjectName(name);
	setupUi(this);
}	


RelatedInput::~RelatedInput()
{
}


