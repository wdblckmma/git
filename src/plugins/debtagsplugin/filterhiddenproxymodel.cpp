//
// C++ Implementation: filterhiddenproxymodel
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "filterhiddenproxymodel.h"

// NTagModel
#include "vocabularymodelrole.h"


namespace NTagModel {

FilterHiddenProxyModel::FilterHiddenProxyModel(bool displayHidden, QObject * pParent)
 : QSortFilterProxyModel(pParent)
{
	_displayHidden = displayHidden;
}


FilterHiddenProxyModel::~FilterHiddenProxyModel()
{
}


bool FilterHiddenProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
{
	QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
	if (sourceModel()->data(index, TypeRole).toInt() == TagTypeItem)
		return true;
	bool isHidden = sourceModel()->data(index, HiddenRole).toBool();
	return isHidden == _displayHidden;
}


}
