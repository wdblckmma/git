//
// C++ Interface: relatedinput
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __RELATEDINPUT_H_2005_08_28
#define __RELATEDINPUT_H_2005_08_28

#include <qwidget.h>
#include <ui_relatedinput.h>

/**
@author Benjamin Mesing
*/
class RelatedInput : public QWidget, public Ui::RelatedInput
{
Q_OBJECT
public:
	RelatedInput(QWidget *parent = 0, const char *name = 0);

	~RelatedInput();

};

#endif	// 
