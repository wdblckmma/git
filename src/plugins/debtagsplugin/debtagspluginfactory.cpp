//
// C++ Implementation: debtagspluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "debtagspluginfactory.h"

#include "debtagsplugin.h"
#include "relatedplugin.h"

namespace NPlugin {

DebtagsPluginFactory* DebtagsPluginFactory::_pInstance = 0;

DebtagsPluginFactory::DebtagsPluginFactory()
{
}


DebtagsPluginFactory::~DebtagsPluginFactory()
{
}

Plugin* DebtagsPluginFactory::createPlugin(const string& name) const
{
	if (name=="DebtagsPlugin")
	{
		return new DebtagsPlugin(*_pContainer);
	}
	if (name=="RelatedPlugin")
	{
		return new RelatedPlugin(*_pContainer);
	}
	return 0;
}

DebtagsPluginFactory* DebtagsPluginFactory::getInstance()
{
	if (_pInstance == 0)
		_pInstance = new DebtagsPluginFactory;
	return _pInstance;
}



};
