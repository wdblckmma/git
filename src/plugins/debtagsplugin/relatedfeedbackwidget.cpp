//
// C++ Implementation: relatedfeedbackwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "relatedfeedbackwidget.h"

RelatedFeedbackWidget::RelatedFeedbackWidget(QWidget *parent, const char *name)
 : QWidget(parent), Ui::RelatedFeedbackWidget()
{
	if (name)
		setObjectName(name);
	setupUi(this);
}


RelatedFeedbackWidget::~RelatedFeedbackWidget()
{
}

void RelatedFeedbackWidget::setClearButton( QPushButton * pButton, int index )
{
	delete(_pClearButton);
	_pClearButton = pButton;
	_pFeedbackLayout->insertWidget(index, _pClearButton);
}
