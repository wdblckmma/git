//
// C++ Interface: selectedtagsview
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NTAGMODEL_SELECTEDTAGSVIEW_2007_03_25
#define __NTAGMODEL_SELECTEDTAGSVIEW_2007_03_25

#include <QListView>

// NTagModel
#include "filterselectedproxymodel.h"

class QAbstractProxyModel;
class QContextMenuEvent;

class TreeFilterModel;

namespace NTagModel
{

class VocabularyModel;
class TagListProxyModel;

/** Shows the selected tags in a simple list
  *
  * Expects a VocabularyModel as source model.
  * @author Benjamin Mesing <bensmail@gmx.net>
  */
class SelectedTagsView : public QListView
{
Q_OBJECT
	/** @brief The number of tags currently displayed. */
// 	int _displayedTagsNum;
	NTagModel::FilterSelectedProxyModel _selectedProxyModel;
	TagListProxyModel* _pTagListProxyModel;
	VocabularyModel* _pModel;
	TreeFilterModel* _pTreeFilterModel;
public:
	SelectedTagsView(QWidget * parent = 0);
	~SelectedTagsView();
	/** @brief Sets the vocabulary mode to be displayed. 
	  * 
	  * @param pModel must be a VocabularyModel.
	  */
	virtual void setModel(QAbstractItemModel* pModel);
protected Q_SLOTS:
	virtual void onItemDoubleClicked(const QModelIndex& index);
	virtual void contextMenuEvent(QContextMenuEvent* pEvent);
protected:
	VocabularyModel* vocabularyModel()	{ return _pModel; };
};

}

#endif // __NTAGMODEL_SELECTEDTAGSVIEW_2007_03_25
