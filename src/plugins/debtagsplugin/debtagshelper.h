//
// C++ Interface: debtagshelper
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __NUTIL_DEBTAGSHELPER_H_2005_09_24
#define __NUTIL_DEBTAGSHELPER_H_2005_09_24

#include <string>
#include <set>


#include <ept/apt/apt.h>





using namespace std;

namespace Xapian {
	class Database;
}

namespace NUtil
{

/** Returns the names of all tags of the given package. 
  *
  * @param package the name of the package
  * @param xapian the xapian database to use (this is the apt-xapian database)
  * @returns the tag names or an empty set if no such package was found.
  */
set<string> tagsForPackage(const string& package, const Xapian::Database& xapian);

/** @brief Calculates the tags that produces a non-empty search result when used for
  * searching together with <em>tags</em>.
  * @param tags the tags to be used
  * @param xapian the xapian database to be used as source, it is assumed to be a
  * Debian-Package database, with tags having the prefix XT
  */
set<string> companionTags(const set<string>& tags, const Xapian::Database& xapian);


}

#endif //  __NUTIL_DEBTAGSHELPER_H_2005_09_24
