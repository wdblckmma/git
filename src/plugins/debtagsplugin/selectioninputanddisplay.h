//
// C++ Interface: selectioninputanddisplay
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __SELECTIONINPUTANDDISPLAY_H_2004_06_28
#define __SELECTIONINPUTANDDISPLAY_H_2004_06_28

#include <QObject>
#include <set>

class QAbstractItemView;
class QLabel;
class QWidget;
class QModelIndex;

class TagItem;

namespace NTagModel
{
	class VocabularyModel;
	class UnselectedTagsView;
}

namespace NPlugin
{
	class DebtagsPluginContainer;
}

using namespace std;

namespace NWidgets {

/** @brief This class manages a widget to selected tags and one to display
  * the selected tags
  * @author Benjamin Mesing
  *
  */
class SelectionInputAndDisplay : public QObject
{
Q_OBJECT
	/** Widget to display the selected tags. 
	  *
	  * @see _pTagSelector
	  */
	QAbstractItemView* _pSelectedTagDisplay;
	/** Widget to selected the tags to search. 
	  *
	  * @see _pSelectedTagDisplay
	  */
	NTagModel::UnselectedTagsView* _pTagSelector;
	/** Contains all widgets used for selecting tags.
	  *
	  * This currently includes an AbstractItemView for displaying the tags and a 
	  * LineEdit for typing a filter.
	  */
	QWidget* _pTagSelectorWidget;
	
	/** Holds the model for the debtags vocabulary. */
	NTagModel::VocabularyModel* _pModel;


	typedef std::string Facet;
	typedef std::string Tag;

	/** @brief The label shown above #_pTagView. */
	QLabel* _pViewLabel;
public:
	/** By default, the selectedTagDisplay will be hidden.
	  * @param pParent the parent of this class
	  */
	SelectionInputAndDisplay(const NPlugin::DebtagsPluginContainer* pContainer, NTagModel::VocabularyModel* pModel, QObject* pParent);
	~SelectionInputAndDisplay();
	
	QWidget* tagSelectorWidget() const	{ return _pTagSelectorWidget; };
	QAbstractItemView* tagDisplayWidget() const	{ return _pSelectedTagDisplay; };
public Q_SLOTS:
	/** @brief Shows or hides the widget displaying the selected tags.
	  *
	  * By default it is shown.
	  */
	void setSelectedTagDisplayShown(bool shown);
	/** @brief Enables/disables all widgets managed by this class. */
	void setEnabled(bool enabled);

signals:
	/** @brief The signal will be emitted, whenever the set of selected tags is changed.
	  *
	  * @param tags the tags that were selected by the user.
	  */
	void tagItemsSelected(std::set<Tag> tags);
	
};

}	// namespace NWidgets

#endif	//  __SELECTIONINPUTANDDISPLAY_H_2004_06_28
