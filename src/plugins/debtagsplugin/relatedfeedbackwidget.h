//
// C++ Interface: relatedfeedbackwidget
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __RELATEDFEEDBACKWIDGET_H_2005_08_28
#define __RELATEDFEEDBACKWIDGET_H_2005_08_28

#include <qwidget.h>
#include <ui_relatedfeedbackwidget.h>

class QPushButton;

/**
@author Benjamin Mesing
*/
class RelatedFeedbackWidget : public QWidget, public Ui::RelatedFeedbackWidget
{
Q_OBJECT
public:
	RelatedFeedbackWidget(QWidget *parent = 0, const char *name = 0);
	~RelatedFeedbackWidget();
	void setClearButton(QPushButton* pButton, int index );
};

#endif	// __RELATEDFEEDBACKWIDGET_H_2005_08_28
