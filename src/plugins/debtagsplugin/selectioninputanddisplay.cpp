//
// C++ Implementation: selectioninputanddisplay
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "selectioninputanddisplay.h"

#include <cassert>

#include <QLayout>
#include <QLineEdit>

#include <wibble/operators.h>


// NTagModel
#include "vocabularymodel.h"
#include "vocabularymodelrole.h"
#include "unselectedtagsview.h"
#include "selectedtagsview.h"
#include "emptytagfilter.h"

#include "helpers.h"

using namespace wibble::operators;


namespace NWidgets 
{

typedef std::string Facet;
typedef std::string Tag;


SelectionInputAndDisplay::SelectionInputAndDisplay(const NPlugin::DebtagsPluginContainer* pContainer, NTagModel::VocabularyModel* pModel, QObject* pParent)
 : QObject(pParent), _pModel(pModel)
{
	_pTagSelectorWidget = new QWidget();
	QLayout* pLayout = new QVBoxLayout(_pTagSelectorWidget);
	QLineEdit* pFilterInput = new QLineEdit(_pTagSelectorWidget);
	pFilterInput->setToolTip(tr("filter string to filter tags"));
	pFilterInput->setWhatsThis(tr("Filters the tag names by the string in realtime."));

	// will be garbage collected once DebtagsPlugin is destroyed
	_pTagSelector = new NTagModel::UnselectedTagsView(pContainer, _pTagSelectorWidget);
	_pTagSelector->setModel(_pModel);
	
	_pTagSelector->show();
	pLayout->addWidget(pFilterInput);
	pLayout->addWidget(_pTagSelector);
	
 	connect(pFilterInput, SIGNAL(textChanged(const QString&)), 
 		_pTagSelector, SLOT(setFilterFixedString(const QString&)));
	
 	_pSelectedTagDisplay = new NTagModel::SelectedTagsView();
 	_pSelectedTagDisplay->setModel(_pModel);
	
}


SelectionInputAndDisplay::~SelectionInputAndDisplay()
{
	delete _pSelectedTagDisplay;
	delete _pTagSelector;
	delete _pTagSelectorWidget;
}

void SelectionInputAndDisplay::setSelectedTagDisplayShown(bool shown)
{
	_pSelectedTagDisplay->setVisible(shown);
}

void SelectionInputAndDisplay::setEnabled(bool enabled)
{
	_pTagSelector->setEnabled(enabled);
	_pSelectedTagDisplay->setEnabled(enabled);
}


}	// namespace NWidgets 


#undef emit
//#include <ept/cache/debtags/vocabulary.tcc>
