#ifndef __DEBTAGSPLUGINCONTAINER_H_2004_06_23
#define __DEBTAGSPLUGINCONTAINER_H_2004_06_23

#include <string>

#include <QObject>
#include <QString>

#include <ept/debtags/vocabulary.h>
//#include <ept/configuration/apt.h>

#include <baseplugincontainer.h>

using namespace std;

namespace Xapian 
{
	class Database;
}


namespace NApplication
{
	class RunCommand;
}

namespace NTagModel
{
	class VocabularyModel;
}

class DebtagsSettingsWidget;

namespace NPlugin
{

class DebtagsPlugin;
class RelatedPlugin;

/** @brief This class provides plugins using the debtags system.
  * 
  * It manages the shared data and offers the possibility to update the debtags database.
  *
  * @author Benjamin Mesing
  */
class DebtagsPluginContainer : public QObject, public BasePluginContainer 
{
	Q_OBJECT
	typedef std::string Facet;
	typedef ept::debtags::Vocabulary Vocabulary;

	Vocabulary _vocabulary;
	NTagModel::VocabularyModel* _pVocabularyModel;

	/** @brief This holds if the debtags operations are currently enabled. 
	  * 
	  * This will be set to false if the reading of the tag database failed. 
	  */
	bool _debtagsEnabled;

	/** Holds the debtags plugin created. */
	DebtagsPlugin* _pDebtagsPlugin;
	/** Holds the related plugin created. */
	RelatedPlugin* _pRelatedPlugin;
	/** This pointer is used to run the <tt>debtags update</tt> command. */
	NApplication::RunCommand* _pCommand;
	/** @brief This holds a pointer to the settings widget currently active.
	  * 
	  * This pointer must not be deleted and should only be used in applySettings(). 
	  */
	DebtagsSettingsWidget* _pSettingsWidget;
public:
	/** Empty Constructor  */
	DebtagsPluginContainer();
	~DebtagsPluginContainer();
	/** @name PluginContainer Interface
	  *
	  * These functions implement the PluginContainer interface.
	  */
	//@{
	virtual bool init(IProvider* pProvider);
	/** @returns "debtagsplugin" */
	virtual string name() const {	return "debtagsplugin"; };
	virtual QString title() const	{ return tr("Debtags Plugins"); };
	/** @brief This returns a list with one entry which is the "Debtags Update" entry
	  *
	  * The QString specifies the menu the action should be added to, the action is
	  * a QAction which is connected to the operation to perform.\n
	  * The default implementation returns an empty vector.
	  */
	vector< pair<QString, QAction*> > actions();
	virtual QWidget* getSettingsWidget(QWidget* pParent);
	virtual void applySettings();
	//@}
	
	/** @name BasePluginContainer Overloads
	  *
	  * These functions implement saving and loading of the container settings
	  * as described in BasePluginContainer.
	  */
	//@{
	virtual QDomElement loadContainerSettings(const QDomElement source);
	virtual void saveContainerSettings(NXml::XmlData& outData, QDomElement parent) const;
	//@}
	
	/** @brief Returns the #_debtagsEnabled property. */
	bool debtagsEnabled()	{ return _debtagsEnabled; };
	/** @brief Returns the facets from the vocabulary. */
	const std::set<Facet> facets() const;
	/** @brief Returns a pointer to the model used to store the information about the vocabulary
	  *
	  * This information includes selected tags, hidden facets and possibly more.
	  */
	NTagModel::VocabularyModel* vocabularyModel() const { return _pVocabularyModel; };
	
	/** The xapian database. */
	const Xapian::Database& xapian()  const;

	
	const Vocabulary& vocabulary() const { return _vocabulary; }
protected:
	/** @brief This sets the debtags operations to be enabled/ disabled 
	  *
	  * If set to false all widgets will be disabled, else they will be enabled.
	  * @see #_debtagsEnabled 
	  */
	void setDebtagsEnabled(bool enabled);
};

}	// namespace NPlugin

#endif //	__DEBTAGSPLUGINCONTAINER_H_2004_06_23

