//
// C++ Interface: filterselectedproxymodel
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NTAGMODEL_FILTERSELECTEDPROXYMODEL_H_2007_03_28
#define __NTAGMODEL_FILTERSELECTEDPROXYMODEL_H_2007_03_28

#include <QSortFilterProxyModel>

namespace NTagModel {

class VocabularyModel;

/** @brief Proxy model that filters items according to whether the items are selected or not.
  *
  * _includeSelected controls which items to be displayed.
  *
  * To determine if an item is selected, the data for the SelectedRole is queried
  * (which should return a boolean). Selection as meant here has nothing to do with
  * traditional selection (e.g. by clicking on items).
  *
  * @author Benjamin Mesing <bensmail@gmx.net>
  */
class FilterSelectedProxyModel : public QSortFilterProxyModel
{
Q_OBJECT
	/** @brief Controls which items to be displayed, if true only selected items will be
	  * diplayed, else only unselected items.
	  */
	bool _includeSelected;
public:
	/** Creates an instance.
	  * 
	  * @param includeSelected used to set _includeSelected
	  */
	FilterSelectedProxyModel(bool includeSelected, QObject * pParent = 0);
	~FilterSelectedProxyModel();
	/** @brief Sets _includeSelected. */
	void setIncludeSelected(bool includeSelected)	{ _includeSelected = includeSelected; }
	bool filterAcceptsRow ( int sourceRow, const QModelIndex & sourceParent ) const;
};

}

#endif
