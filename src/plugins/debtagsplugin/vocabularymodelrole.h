//
// C++ Interface: vocabularymodelrole
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

namespace NTagModel
{


enum VocabularyModelRole
{
	SelectedRole = Qt::UserRole,
	HiddenRole,
	/// role that decides wether the item is a facet or a tag
	TypeRole,
	// @pre TypeRole must be TagTypeItem 
	// -> data() returns that Tag instance
	TagRole,
	/** Display role to be used, when tag names should be displayed along with the facet name.
	  *
	  * This is usually the case, when the tags are diplayed without a context.
	  */
	FullDisplayRole	
};

enum ItemType
{
	FacetTypeItem,
	TagTypeItem
};


}
