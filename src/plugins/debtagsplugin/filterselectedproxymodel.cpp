//
// C++ Implementation: filterselectedproxymodel
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "filterselectedproxymodel.h"

#include <QDebug>

// NTagModel
#include "vocabularymodelrole.h"

namespace NTagModel {

FilterSelectedProxyModel::FilterSelectedProxyModel(bool includeSelected, QObject * pParent)
 : QSortFilterProxyModel(pParent)
{
	_includeSelected = includeSelected;
}


FilterSelectedProxyModel::~FilterSelectedProxyModel()
{
}


bool FilterSelectedProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
{
	QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
	if (sourceModel()->data(index, TypeRole).toInt() == FacetTypeItem) 
	{
		return true;
	}
	bool isSelected = sourceModel()->data(index, SelectedRole).toBool();
	return isSelected == _includeSelected;
}

}
