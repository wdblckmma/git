//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __TREEFILTERMODEL_H_2012_05_13
#define __TREEFILTERMODEL_H_2012_05_13

#include <QSortFilterProxyModel>

/** @brief A filter which hides top level nodes which do not have any child nodes.
  *
  * All child nodes will be accepted.
  *
  * @author Benjamin Mesing
  */
class TreeFilterModel : public QSortFilterProxyModel
{ 
public:
	TreeFilterModel(QObject * pParent = 0) : QSortFilterProxyModel(pParent)
	{}
	virtual bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
	{
		if (sourceParent.isValid())
			return true;
		QModelIndex current = sourceModel()->index(sourceRow, 0, sourceParent);
// 		qStrDebug("TreeSortFilter:item: " + data(current).toString());
		if (sourceModel()->rowCount(current) > 0)
		{
// 			qDebug("has children: %d ", sourceModel()->rowCount(current));
			return true;
		}
		return false;
	}
};

#endif // __TREEFILTERMODEL_H_2012_05_13