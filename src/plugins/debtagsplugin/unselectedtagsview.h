//
// C++ Interface: unselectedtagsview
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NTAGMODEL_UNSELECTEDTAGSVIEW_2007_03_25
#define __NTAGMODEL_UNSELECTEDTAGSVIEW_2007_03_25

#include <vector>

using namespace std;

#include <QTreeView>

// NTagModel
#include "filterselectedproxymodel.h"
#include "filterhiddenproxymodel.h"

class QAbstractProxyModel;
class QContextMenuEvent;

namespace NPlugin
{
	class DebtagsPluginContainer;
}

class TreeFilterModel;

namespace NTagModel
{

class EmptyTagFilter;

/** @brief This class can be used to display and modify VocabularyModels.
  *
  * It may only be initialised with VocabularyModels.
  * It allows to choose an item by double clicking on it.
  *
  * Model chain: _pTreeFilter -> TextFilter -> _filterSelectedProxyModel -> _pTagFilter -> _hiddenFilterProxyModel -> sourceModel
  * -> denotes the uses realtion
  */
class UnselectedTagsView : public QTreeView
{
Q_OBJECT
	FilterSelectedProxyModel _filterSelectedProxyModel;
	FilterHiddenProxyModel _hiddenFilterProxyModel;
	EmptyTagFilter* _pTagFilter;
	/** @brief Filters the facets according to whether a subtags is shown.
	  */
	TreeFilterModel* _pTreeFilter;
	/** Filters the tags according to a match-string.
	  *
	  * @see setFilterFixedString()
	  */
	QSortFilterProxyModel* _pTextFilter;
	
	/** This is the model holding the actual data. */
	QAbstractItemModel* _pDataModel;
public:
	UnselectedTagsView(const NPlugin::DebtagsPluginContainer* pContainer, QWidget * parent = 0);
	~UnselectedTagsView();
	virtual void setModel(QAbstractItemModel* pModel);
	/** @brief Returns the filter responsible for filtering out tags which should not be shown since
	  * they would lead to an empty result set. */
	EmptyTagFilter* emptyTagFilter()	{ return _pTagFilter; };
	
	
public Q_SLOTS:
	/** Allows to set a string for which the tags shall be filtered.
	  *
	  * Tags will be shown if they match the the given string (case insensitive).
	  * Facets will be shown if they match the given string or any of the containing
	  * tags matches the string.
	  */
	void setFilterFixedString(const QString& string);
protected Q_SLOTS:
	virtual void onItemDoubleClicked(const QModelIndex& index);
	virtual void contextMenuEvent(QContextMenuEvent* event);
private:
	/** Debug. */
	vector<QAbstractProxyModel*> _filterChain;
};

}

#endif	// __NTAGMODEL_UNSELECTEDTAGSVIEW_2007_03_25
