#ifndef __POPCONPLUGIN_H_2010_02_28
#define __POPCONPLUGIN_H_2010_02_28

#include <QString>

#include <string>

#include <scoreplugin.h>
#include <informationplugin.h>
#include <popconplugincontainer.h>

class QAbstractItemView;
class QPoint;

namespace NTagModel
{
	class VocabularyModel;
}

using namespace std;

namespace NPlugin
{

/** This plugin offers search by tags  
  *
  * @author Benjamin Mesing
  */
class PopconPlugin : public ScorePlugin, public InformationPlugin 
{
	/** @brief This holds a link to the manager which manages the plugin.
	  *
	  * Acquaintance relation */
	IProvider* _pProvider;
	/** The container which holds this plugin. */
	const PopconPluginContainer& _container;
public:
	static const QString PLUGIN_NAME;
	/** Create this plugin as plugin in container.
	  *
	  * @param container the container which contains this plugin. 
	  */
	PopconPlugin(const PopconPluginContainer& container);;
	virtual ~PopconPlugin();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface 
	  */
	//@{
	/** @brief Initializes the plugin. 
	  */
	virtual void init(IProvider* pProvider);
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return PLUGIN_NAME; }
	virtual QString title() const;
	virtual QString briefDescription() const;
	virtual QString description() const;
	//@}
	///@todo try to remove malformed descriptions
	/** @name ScorePlugin interface
	  * 
	  * Implementation of the SearchPlugin interface
	  */
	//@{
	/** @brief Returns the score of the handed packages for the currently active search.
	  *
	  * The scores are in [0..1] where 0.0f means the lowest and 1.0f the highest score.
	  * The scores are always calculated for a whole set of packages, because scores
	  * may be relative to each other (i.e. there is no absolute criterion to score package
	  * but we can say that package1 matches better than package2)
	  *
	  * @pre offersScore() == true
	  * @post returnedValue.keys() == packages<br>
	  * i.e. all the handed <tt>packages</tt> must be contained in the returned map
	  * @returns a map mapping each package to its scores
	  * @see offersScore()
	  */
	virtual map<string, float> getScore(const set<string>& packages) const ;
	/** @brief Returns if the plugin is currently offering scores.
	  * 
	  * This could return <tt>false</tt> e.g. if scores are only calculated for an active search 
	  * (like e.g. a fulltext search), but no search is currently active.
	  * @see getScore()
	  */
	virtual bool offersScore() const;
	//@}
	
	/** @name InformationPlugin interface
	  * 
	  * Implementation of the InformationPlugin interface
	  */
	//@{
	virtual uint informationPriority() const	{ return 5; };
	/** This plugin does not offer a separate information widget. */
	virtual QWidget* informationWidget() const	{ return 0; };
	/** This plugin does not offer a separate information widget. */
	virtual QString informationWidgetTitle() const	{ return _emptyString; };
	/** This plugin does not offer a separate information widget. */
	virtual void updateInformationWidget(const string&) {};
	/** This plugin does not offer a separate information widget. */
	virtual void clearInformationWidget() {};
	/** This plugin offers an information text. */
	virtual bool offersInformationText() const	{ return true; };
	/** Returns a string which lists the tags for the requested package. */
	virtual QString informationText (const string& package);
	//@}
	/** @brief This can be used to notify the plugin that the popcon data in the 
	  * plugin container (i.e. the tag collection and the vocabulary) has changed. 
	  *
	  * It behaves sound if the values are not set in the container. */
	void popconDataChanged();
};

}	// namespace NPlugin

#endif //	__POPCONPLUGIN_H_2010_02_28

