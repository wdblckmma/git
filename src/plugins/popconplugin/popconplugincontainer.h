#ifndef __POPCONPLUGINCONTAINER_H_2010_02_28
#define __POPCONPLUGINCONTAINER_H_2010_02_28

#include <string>

#include <QObject>
#include <QString>

//#include <ept/configuration/apt.h>

#include <baseplugincontainer.h>

using namespace std;




class PopconSettingsWidget;

namespace NPlugin
{

class PopconPlugin;

/** @brief This class provides plugins using the popcon system.
  * 
  * It manages the shared data and offers the possibility to update the popcon database.
  *
  * @author Benjamin Mesing
  */
class PopconPluginContainer : public QObject, public BasePluginContainer 
{
	Q_OBJECT
	
	/** @brief This holds if the popcon operations are currently enabled. 
	  * 
	  * This will be set to false if the reading of the tag database failed. 
	  */
	bool _popconEnabled;

	/** Holds the popcon plugin created. */
	PopconPlugin* _pPopconPlugin;
public:
	/** Empty Constructor  */
	PopconPluginContainer();
	~PopconPluginContainer();
	/** @name PluginContainer Interface
	  *
	  * These functions implement the PluginContainer interface.
	  */
	//@{
	virtual bool init(IProvider* pProvider);
	/** @returns "popconplugin" */
	virtual string name() const {	return "popconplugin"; };
	virtual QString title() const	{ return tr("Popcon Plugins"); };
	/** @brief This returns a list with one entry which is the "Popcon Update" entry
	  *
	  * The QString specifies the menu the action should be added to, the action is
	  * a QAction which is connected to the operation to perform.\n
	  * The default implementation returns an empty vector.
	  */
	vector< pair<QString, QAction*> > actions();
	//@}
	
	/** @brief Returns the #_popconEnabled property. */
	bool popconEnabled()	{ return _popconEnabled; };
protected:
	/** @brief This sets the popcon operations to be enabled/ disabled 
	  *
	  * If set to false all widgets will be disabled, else they will be enabled.
	  * @see #_popconEnabled 
	  */
	void setPopconEnabled(bool enabled);
};

}	// namespace NPlugin

#endif //	__POPCONPLUGINCONTAINER_H_2010_02_28

