//
// C++ Interface: popconpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGINPOPCONPLUGINFACTORY_H_2010_02_28
#define __NPLUGINPOPCONPLUGINFACTORY_H_2010_02_28

#include <ipluginfactory.h>

namespace NPlugin 
{

class PopconPluginContainer;

/** @brief This creates the popcon plugins.
  *
  * 
  * @author Benjamin Mesing
  */
class PopconPluginFactory : public IPluginFactory
{
	PopconPluginContainer* _pContainer;
	PopconPluginFactory();
	static PopconPluginFactory* _pInstance;
public:
	~PopconPluginFactory();
	void setContainer(PopconPluginContainer* pContainer) { _pContainer = pContainer; };
	/** @name IPluginFactory interface */
	//@{
	/** @brief This creates a plugin for the given name.
	  *
	  * Accepted names are: 
	  * \li "PopconPlugin"
	  *
	  * @pre setContainer() must have been called or 0 will be returned.
	  */
	virtual Plugin* createPlugin(const string& name) const;
	//@}
	static PopconPluginFactory* getInstance();
};

};

#endif	//  __NPLUGINPOPCONPLUGINFACTORY_H_
