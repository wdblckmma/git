#include <sstream>
#include <iostream>

#include <qcheckbox.h>
#include <qlayout.h>
#include <qlabel.h>
#include <QMainWindow>
#include <qmessagebox.h>
#include <qpoint.h>
#include <QAbstractItemView>
#include <QVBoxLayout>

//#include <ept/configuration/apt.h>
//#include <ept/cache/popcon/tagmap.h>


#include <helpers.h>
#include <exception.h>

// NPlugin
#include <packagenotfoundexception.h>
#include <iprovider.h>

#include "popconplugin.h"

using namespace std;

namespace NPlugin
{

const QString PopconPlugin::PLUGIN_NAME = "PopconPlugin";


/////////////////////////////////////////////////////
// Constructors/ Destructors
/////////////////////////////////////////////////////

PopconPlugin::PopconPlugin(const PopconPluginContainer& container) :
	_container(container)
{ 
	_pProvider = 0;
}

PopconPlugin::~PopconPlugin() 
{ 
}

/////////////////////////////////////////////////////
// Plugin Interface
/////////////////////////////////////////////////////

void PopconPlugin::init(IProvider* pProvider)
{
	_pProvider = pProvider;
}


QString PopconPlugin::title() const 
{ 
	return QString("Popcon Plugin"); 
}

QString PopconPlugin::briefDescription() const 
{ 
	return QString("Makes popcon data available and uses it for score calucation"); 
}

QString PopconPlugin::description() const 
{ 
	return QString("This plugin makes the popcon information available."
	 "Popcon scores are used to calculate the scores for a package and "
	 "displayed in the package details."); 
}

/////////////////////////////////////////////////////
// Search Plugin Interface 
/////////////////////////////////////////////////////

map<string, float> PopconPlugin::getScore(const set<string>& packages) const
{
	qDebug("PopconPlugin::getScore not implemented");
	return map<string,float>();
}

bool PopconPlugin::offersScore() const 
{
	return false;
}

/////////////////////////////////////////////////////
// Information Plugin Interface 
/////////////////////////////////////////////////////

QString PopconPlugin::informationText(const string& package)
{
	{	// add the tags of the package to the description
// 		float score = _container.popcon().scoreByName(package);
		QString detailsString = "<b>Score:</b> " ;
// 		+ QString::number(score);
		return detailsString+"<br>";
	}
}

/////////////////////////////////////////////////////
// Helper Methods
/////////////////////////////////////////////////////





}	// namespace NPlugin

#undef emit
/*
#include <ept/cache/apt/packages.tcc>
#include <ept/cache/popcon/vocabulary.tcc>
#include <ept/cache/tag.tcc>
#include <tagcoll/coll/base.tcc>
*/
