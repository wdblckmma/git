#include <cassert>

#include <qmessagebox.h>
#include <qaction.h>
#include <QMainWindow>
#include <QMutex>

#include <unistd.h>		// for getuid and geteuid
#include <sys/types.h>	//

#include <wibble/operators.h>

#include "popconplugincontainer.h"

// NUtil
#include "helpers.h"

// NApplication
#include "applicationfactory.h"
#include "runcommand.h"

// NPlugin
#include <iprovider.h>
#include <plugincontainer.h>
#include <iprogressobserver.h>
#include "popconplugin.h"
#include "popconpluginfactory.h"


#include <globals.h>

extern "C" 
{ 
	NPlugin::PluginContainer* new_popconplugin() 
	{
		return new NPlugin::PopconPluginContainer;
	} 

	NPlugin::PluginInformation get_pluginInformation()
	{
		return NPlugin::PluginInformation("popconplugin", toString(NPackageSearch::VERSION), "Benjamin Mesing");
	} 
}

/** Initialize the plugin. */
__attribute__ ((constructor)) void init() 
{
}
      
      
// __attribute__ ((destructor)) void fini() 
// {
//   /* code here is executed just before dlclose() unloads the module */
// } 


namespace NPlugin
{

PopconPluginContainer::PopconPluginContainer()
{
	// this assumes, that only one instance of the PopconPluginContainer is created
	// (no two versions of libapt-front may be opened at any time)
	// This is ok for our purpose - though usually there should be a singleton ensuring
	// this constraint...
	PopconPluginFactory::getInstance()->setContainer(this);
	_pPopconPlugin = 0;
	addPlugin("PopconPlugin");
	
	_popconEnabled=false;
}
 
PopconPluginContainer::~PopconPluginContainer()
{
	unloadAllPlugins();
}

/////////////////////////////////////////////////////
// PluginContainer Interface
/////////////////////////////////////////////////////


bool PopconPluginContainer::init(IProvider* pProvider)
{
	BasePluginContainer::init(pProvider, PopconPluginFactory::getInstance());
	
	if (popconEnabled())
	{
		// use dynamic cast here because of the virtual base class 
		// (static_cast is not allowed there)
		_pPopconPlugin = dynamic_cast<PopconPlugin*>(requestPlugin("PopconPlugin"));
	}
	else
	{
		provider()->reportError(
			tr("Popcon data not available" ),
			tr(
				"<p>There is no popcon data available!</p>"
				"<p>"
				"The popcon plugin has been disabled."
				"</p>")
		);
		return false;
	}
	return popconEnabled();
}


/////////////////////////////////////////////////////
// BasePluginContainer Interface
/////////////////////////////////////////////////////

vector< pair<QString, QAction*> > PopconPluginContainer::actions()
{
	vector< pair<QString, QAction*> > result;
	return result;
}



/////////////////////////////////////////////////////
// Helper Methods
/////////////////////////////////////////////////////

void PopconPluginContainer::setPopconEnabled(bool enabled)
{
	_popconEnabled = enabled;
}


}	// namespace NPlugin

