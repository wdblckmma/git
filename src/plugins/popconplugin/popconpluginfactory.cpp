//
// C++ Implementation: popconpluginfactory
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "popconpluginfactory.h"

#include "popconplugin.h"

namespace NPlugin {

PopconPluginFactory* PopconPluginFactory::_pInstance = 0;

PopconPluginFactory::PopconPluginFactory()
{
}


PopconPluginFactory::~PopconPluginFactory()
{
}

Plugin* PopconPluginFactory::createPlugin(const string& name) const
{
	if (name=="PopconPlugin")
	{
		return new PopconPlugin(*_pContainer);
	}
	return 0;
}

PopconPluginFactory* PopconPluginFactory::getInstance()
{
	if (_pInstance == 0)
		_pInstance = new PopconPluginFactory;
	return _pInstance;
}



};
