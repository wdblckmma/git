//
// C++ Implementation: columncontrol
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "columncontroldlg.h"

ColumnControlDlg::ColumnControlDlg(QWidget* parent)
 : QDialog(parent)
{
	setupUi(this);
}


ColumnControlDlg::~ColumnControlDlg()
{
}


void ColumnControlDlg::setContent(const QStringList & shown, const QStringList & hidden)
{
   _pShownList->addItems(shown);
   _pHiddenList->addItems(hidden);
}



QStringList ColumnControlDlg::shownColumns()
{
   QStringList result;
   for (int i=0; i<_pShownList->count(); ++i)
      result.push_back(_pShownList->item(i)->text());
   return result;
}


QStringList ColumnControlDlg::hiddenColumns()
{
   QStringList result;
   for (int i=0; i<_pHiddenList->count(); ++i)
      result.push_back(_pHiddenList->item(i)->text());
   return result;
}

void ColumnControlDlg::on__pHideButton_clicked()
{
 	if (_pShownList->selectedItems().size() == 0)
		return;
	QListWidgetItem* pItem = _pShownList->selectedItems().first();
	int row = _pShownList->row(pItem);
	pItem = _pShownList->takeItem(row);
	_pHiddenList->insertItem(_pHiddenList->count(), pItem);
}

void ColumnControlDlg::on__pShowButton_clicked()
{
 	if (_pHiddenList->selectedItems().size() == 0)
		return;
	QListWidgetItem* pItem = _pHiddenList->selectedItems().first();
	int row = _pHiddenList->row(pItem);
	pItem = _pHiddenList->takeItem(row);
	_pShownList->insertItem(_pShownList->count(), pItem);
}

