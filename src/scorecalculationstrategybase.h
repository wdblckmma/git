//
// C++ Interface: scorecalculationstrategybase
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NPLUGIN_SCORECALCULATIONSTRATEGYBASE_H_2005_07_30
#define __NPLUGIN_SCORECALCULATIONSTRATEGYBASE_H_2005_07_30

#include <string>

#include <iscorecalculationstrategy.h>

using namespace std;

namespace NPlugin {

/** @brief Provides a base implementation for a ScoreCalculationStrategy.
  *
  * In your strategies you only need to implement 
  * IScoreCalculationStrategy::calculateScore(set&lt;int&gt;) when inheriting 
  * this function. There you must call setScore(string, float) for each
  * of the packages handed.
  * 
  * @author Benjamin Mesing
  */
class ScoreCalculationStrategyBase : public IScoreCalculationStrategy
{
	map<string, float> _packagesToScore;
protected:
	/** @brief Sets the scores for the handed package. */
	void setScore(const string package, float score)
	{
		_packagesToScore[package] = score;
	}
public:
	ScoreCalculationStrategyBase();
	virtual ~ScoreCalculationStrategyBase();

	/** @name IScoreCalculationStrategy interface
	  * 
	  * Implementation of the IScoreCalculationStrategy interface
	  */
	//@{
	virtual float getScore(const string& package) const;
	virtual const map<string, float>& getScore() const;
	virtual void clear();
	//@}
};

}

#endif	// __NPLUGIN_SCORECALCULATIONSTRATEGYBASE_H_2005_07_30
