//
// Author: Benjamin Mesing <bensmail@gmx.net>
//
// Copyright: See COPYING file that comes with this distribution
//
//
// This file was generated on Wed Aug 31 2005 at 10:43:44
//

#ifndef __NPLUGIN_ACTION_H_2005_08_31
#define __NPLUGIN_ACTION_H_2005_08_31

#include <string>

#include <QString>

class QAction;

/**
 * Namespace
 */
namespace NPlugin 
{ 

/** @brief Class that holds a QAction object and some details about it.
  *
  * The details are the #_menu and #_toolBar where the action should be added to,
  * and if it is this action acts on selected packages (#_packageAction).
  *
  * The QAction object will not be deleted by this class. Normally it should be 
  * autodeleted by its parent object. Otherwise, you have to delete it manually.
  */

class Action 
{
public:
	Action(QAction* pAction, bool packageAction=false, QString menu="", QString toolBar="");
	
	/** @brief Returns the QAction object used for this action. */
	QAction* action () const 
	{
		return _pAction;
	}
	/** @brief Sets the QAction object used for this action. */
	void setAction (QAction* value ) 
	{
		_pAction = value;
	}
	/** @brief Returns the #_menu property. */
	QString menu() const {
		return _menu;
	}
	/** @brief Sets the _menu property. */
	void setMenu (QString value ) {
		_menu = value;
	}
	/** @brief Returns the #_toolBar property. */
	QString toolBar() const {
		return _toolBar;
	}
	/** @brief Sets the #_toolBar property. */
	void setToolBar(QString value ) {
		_toolBar = value;
	}
	/** @brief Returns the #_packageAction property. */
	bool packageAction() const {
		return _packageAction;
	}
	/** @brief Sets the #_packageAction property. */
	void setPackageAction (bool value ) {
		_packageAction = value;
	}
private:
	/** @brief Holds which menu this action belongs to.
	  *
	  * If it is empty, the action will be added to no menu.
	  * 
	  * <b>Default:</b> empty
	  */
	QString _menu;
	/** @brief Holds which toolBar this action should be added to.
	  *
	  * If it is empty, the action will not be added to a toolBar.
	  * 
	  * <b>Default:</b> empty
	  */
	QString _toolBar;
	/** @brief Holds if this action is performed to packages. 
	  *
	  * Setting this to true, results in the action being disabled if
	  * no package is selected.
	  *
	  * <b>Default:</b> false 
	  */
	bool _packageAction;
	/** @brief The QAction for this action. */
	QAction* _pAction;
};
} 
#endif // __NPLUGIN_ACTION_H_2005_08_31

