//
// C++ Implementation: plugininformer
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2004
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <algorithm>
#include <functional>

#include "plugininformer.h"

#include "ipluginuser.h"

namespace NPlugin 
{

PluginInformer::PluginInformer()
{
}


PluginInformer::~PluginInformer()
{
}

void PluginInformer::removePluginUser(IPluginUser* pUser)
{
	_users.erase(pUser);
}

void PluginInformer::informAddPlugin(Plugin* pPlugin)
{
	for_each(_users.begin(), _users.end(), 
		bind2nd(mem_fun(&IPluginUser::addPlugin), pPlugin) );
}

void PluginInformer::informRemovePlugin(Plugin* pPlugin)
{
	for_each(_users.begin(), _users.end(), 
		bind2nd(mem_fun(&IPluginUser::removePlugin), pPlugin) );
}




};
