//
// C++ Implementation: informationpluginmockup
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "informationpluginmockup.h"

namespace NTest {

InformationPluginMockup::InformationPluginMockup()
 : NPlugin::InformationPlugin(), NPlugin::ShortInformationPlugin(), _shortInformationPriority(0), _shortInformationCaption("Caption")
{
}


InformationPluginMockup::~InformationPluginMockup()
{
}


}
