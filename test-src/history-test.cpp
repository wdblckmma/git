//
// C++ Implementation: history-test
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifdef __UNIT_TEST_PP

#include <UnitTest++.h>
#include <iostream>
#include <string>

#include "history.h"

using namespace std;

SUITE(History)
{
	
	// Equivalence classes
	// "History()"
	// Hi
	// "append(T)"
	// Ap1 empty, Ap2 not_empty
	// Ap3 current is invalid, Ap4 current is not the last element, Ap5 current is the last element
	// "empty()"
	// Em1 empty, Em2 not empty
	// "const T& current()"
	// Cu
	// "const T& back()"
	// Ba1 last element is current, Ba2 middle element is current
	// "const T& forward()"
	// Fo1 first element is current, Fo2 middle element is current
	// "const T& backPossible()"
	// Bp1 first element is current, Bp2 middle element is current, Bp3 last element is current
	// Bp4 empty
	// "const T& forwardPossible()"
	// Fp1 first element is current, Fp2 middle element is current, Fp3 last element is current
	// Fp4 empty
	// "clear()"
	// Cl1 empty, Cl2 not empty
	
	TEST(History_append)
	{
		// Hi
		NBrowser::History<string> h;
		// Em1
		CHECK(h.empty());
		// Fp4
		CHECK(!h.forwardPossible());
		// Bp4
		CHECK(!h.backPossible());
		// Cl1
		h.clear();
		CHECK(h.empty());
		// Ap1, Ap3
		h.append("first");
		// Em2
		CHECK(!h.empty());
		// Cu
		CHECK_EQUAL(h.current(), "first");
		// Ap2, Ap5
		h.append("second");
		CHECK_EQUAL(h.current(), "second");
		// Bp3
		CHECK( h.backPossible());
		// Fp3
		CHECK(!h.forwardPossible());
		// go back one entry
		// Ba1
		CHECK_EQUAL(h.back(), "first");
		// Bp1
		CHECK(!h.backPossible());
		// Fp1
		CHECK(h.forwardPossible());
		// Fo1
		CHECK_EQUAL(h.forward(), "second");
		h.append("third");
		h.back();	// second is current
		// Bp2
		CHECK(h.backPossible());
		// Fp2
		CHECK(h.forwardPossible());
		// Ba2
		CHECK_EQUAL(h.back(), "first");
		h.forward();	// second
		// Fo2
		CHECK_EQUAL(h.forward(), "third");
		h.back();	// second
		// Ap4
		h.append("fourth");
		CHECK(!h.forwardPossible());
		CHECK( h.backPossible());
		CHECK_EQUAL(h.back(), "second");
		// Cl2
		h.clear();
		CHECK(h.empty());
	}

}

#endif	// UNITTEST_PP

