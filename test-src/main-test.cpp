#ifdef __UNIT_TEST_PP

#include <UnitTest++.h>

#include <QApplication>


int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	return UnitTest::RunAllTests();
// 	return app.exec();
}

#endif