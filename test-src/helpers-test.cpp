/** Tests for functions in helpers.h */

#include <string>
#include <helpers.h>

using namespace std;

#ifdef __QT_TEST


#include <QTest>


class HelpersTest: public QObject
{
	Q_OBJECT
private slots:
	void testMakeLower()
	{
		string myString = "aBC";
		string& result = makeLower(myString);
		QCOMPARE(myString, string("abc"));
		// result and myString should point to the same object
		QCOMPARE(&result, &myString);
		
		// test German umlauts
		myString = "ÜÖÄ";
		string result1 = makeLower(myString);
		QCOMPARE(myString, string("ÜÖÄ"));

		
	}
	void testToLower()
	{
		string myString = "aBC";
		string result = toLower(myString);
		QCOMPARE(result, string("abc"));
		// no side effects expected
		QCOMPARE(myString, string("aBC"));
		// result and myString should not point to the same object
		QVERIFY(&result != &myString);
	}

};

QTEST_MAIN(HelpersTest)
#include "helpers-test.moc"

#endif	// __QT_TEST



#ifdef __UNIT_TEST_PP

#include <UnitTest++.h>
#include <QString>
#include <iostream>

using namespace std;

// std::string& makeLower(std::string& trans)
TEST(makeLower)
{
	string myString = "aBC";
	string& result = makeLower(myString);
	CHECK_EQUAL(myString, string("abc"));
	// result and myString should point to the same object
	CHECK_EQUAL(&result, &myString);
	
	// test German umlauts
// 	myString = "ÜÖÄ";
// 	string result1 = makeLower(myString);
// 	QCOMPARE(myString, string("ÜÖÄ"));

	
}

// inline std::string toLower(const std::string& in)
TEST(toLower)
{
	string myString = "aBC";
	string result = toLower(myString);
	CHECK_EQUAL(result, string("abc"));
	// no side effects expected
	CHECK_EQUAL(myString, string("aBC"));
	// result and myString should not point to the same object
	CHECK(&result != &myString);
}

#endif	// __UNIT_TEST_PP

