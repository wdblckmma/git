//
// C++ Implementation: packagedisplaywidget-test
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef __UNIT_TEST_PP

#include <UnitTest++.h>

#include <QHeaderView>
#include <QMainWindow>

#include "packagedisplaywidget.h"

#include "informationpluginmockup.h"

using namespace NPackageSearch;

struct TheDialog
{
	TheDialog() : argc(0), widget(*(new PackageDisplayWidget())) { }
	~TheDialog() { /* some teardown */ }
	int argc;

	PackageDisplayWidget& widget;
};

SUITE(PackageDisplayWidget_Test)
{
	
	TEST_FIXTURE(TheDialog, ctor) 
	{
		CHECK_EQUAL(0, widget.rowCount());
		CHECK_EQUAL(0, widget.columnCount());
		CHECK(widget.selectedPackage().isNull());
		CHECK(!widget.scoreColumnVisible());
	}	
	
	// Equivalence classes SP-setPackages():
	// SP1 empty package list (empty/uninitialised list before)
	// SP2 one package in list
	// SP3 multiple packages in list
	// SP4 empty package list (non-empty list before)
	// SP5 modifying package list after calling setPackages (should have no effect)
	TEST_FIXTURE(TheDialog, setPackages) 
	{
		set<string> packages;
		widget.setPackages(packages);
		CHECK_EQUAL((long) packages.size(), widget.rowCount());	// SP1
		packages.insert("packagesearch");
		widget.setPackages(packages);
		CHECK_EQUAL((long) packages.size(), widget.rowCount());	// SP2
		packages.insert("apt");
		packages.insert("libept");
		widget.setPackages(packages);
		CHECK_EQUAL((long) packages.size(), widget.rowCount());	// SP3
		long listSize = packages.size();
		packages.clear();
		CHECK_EQUAL(listSize, widget.rowCount());	// SP5
		widget.setPackages(set<string>());
		CHECK_EQUAL(0, widget.rowCount());	// SP4
	}

	// Equivalence classes AP-addPackages():
	// xAP1 no short information plugin before
	// xAP2 one or more information plugin before
	// xAP3 short information plugin
	// AP4 no short information plugin
	// xAP5 plugin has valid shortInformationCaption
	// (untested) AP6 plugin has invalid shortInformationCaption
	// Not tested/specified: adding the same plugin twice or more
	// Column ordering
	// AP10 two plugins have the same priority
	// xAP11 added plugin has higher priority value than a plugin already added
	// xAP12 added plugin has lower  priority value than a plugin already added
	// xAP13 added plugin has higher priority value than multiple plugins already added
	// xAP14 added plugin has lower  priority value than multiple plugins already added
	// xAP15 plugin has highest priority value
	// xAP16 plugin has lowest  priority value
	TEST_FIXTURE(TheDialog, addPlugin) 
	{	
		// DEBUG Begin
// 		QMainWindow& wnd = *(new QMainWindow());
// 		wnd.setCentralWidget(&widget);
// 		wnd.show();
		// DEBUG End
		
		QHeaderView* pHeader = widget.horizontalHeader();
		// default priority 0
		NTest::InformationPluginMockup plugin1;
		plugin1.setShortInformationPriority(10);
		plugin1.setShortInformationCaption("Plugin1");
		widget.addPlugin(&plugin1);	// AP1, AP3, AP5
		CHECK_EQUAL(1, widget.columnCount());
		CHECK_EQUAL(plugin1.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(0))->text());
		
		NTest::InformationPluginMockup plugin2;
		plugin2.setShortInformationPriority(5);
		plugin2.setShortInformationCaption("Plugin2");
		widget.addPlugin(&plugin2);	// AP2, AP12, AP16
		CHECK_EQUAL(2, widget.columnCount());
		CHECK_EQUAL(plugin2.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(0))->text());
		CHECK_EQUAL(plugin1.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(1))->text());

		NTest::InformationPluginMockup plugin3;
		plugin3.setShortInformationPriority(15);
		plugin3.setShortInformationCaption("Plugin3");
		widget.addPlugin(&plugin3);	// AP11, AP13, AP15
		CHECK_EQUAL(plugin2.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(0))->text());
		CHECK_EQUAL(plugin1.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(1))->text());
		CHECK_EQUAL(plugin3.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(2))->text());

		NTest::InformationPluginMockup plugin4;
		plugin4.setShortInformationPriority(3);
		plugin4.setShortInformationCaption("Plugin4");
		widget.addPlugin(&plugin4);	// AP14
		CHECK_EQUAL(plugin4.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(0))->text());
		CHECK_EQUAL(plugin2.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(1))->text());
		CHECK_EQUAL(plugin1.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(2))->text());
		CHECK_EQUAL(plugin3.shortInformationCaption(), widget.horizontalHeaderItem(pHeader->logicalIndex(3))->text());
	}
}

#endif	// __UNIT_TEST_PP
