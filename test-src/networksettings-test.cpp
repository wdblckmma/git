/** Tests for functions in networksettings.h */

#include <string>
#include <networksettings.h>

#include <stdlib.h>

using namespace std;


#ifdef __UNIT_TEST_PP

#include <UnitTest++.h>
#include <QString>
#include <iostream>

using namespace std;
using namespace NUtil;

void helper_setProxyEnv() {
	setenv("http_proxy", "http://localhost:8080", 1);
}

void helper_validateProxyEnv(const NetworkSettings& settings, NetworkSettings::ProxyType type, 
									  string host="", int port=-1, bool useProxy=false) 
{
	CHECK_EQUAL(host, settings.host());
	CHECK_EQUAL(port, settings.port());
	CHECK_EQUAL(type, settings.proxyType());
	CHECK_EQUAL(useProxy, settings.useProxy());
}


SUITE(NetworkSettings_Test)
{

	TEST(ctorEmpty) 
	{	
		NetworkSettings settings;
		helper_validateProxyEnv(settings, NetworkSettings::NO_PROXY, "", -1);
	}
	TEST(ctor2)
	{
		NetworkSettings settings(NetworkSettings::NO_PROXY, "my_proxy", 80);
		helper_validateProxyEnv(settings, NetworkSettings::NO_PROXY, "my_proxy", 80, false);
	}
	TEST(ctor3)
	{
		NetworkSettings settings(NetworkSettings::CUSTOM_PROXY, "my_proxy", 80);
		helper_validateProxyEnv(settings, NetworkSettings::CUSTOM_PROXY, "my_proxy", 80, true);
	}
	TEST(ctor4)
	{
		helper_setProxyEnv();
		// system proxy -> host and port
		NetworkSettings settings(NetworkSettings::SYSTEM_PROXY, "my_proxy", 0);
		helper_validateProxyEnv(settings, NetworkSettings::SYSTEM_PROXY, "localhost", 8080, true);
	}
	TEST(ctor5)
	{
		unsetenv("http_proxy");
		// system proxy not set -> host and port should be stored as handed
		NetworkSettings settings(NetworkSettings::SYSTEM_PROXY, "my_proxy", 80);
		helper_validateProxyEnv(settings, NetworkSettings::SYSTEM_PROXY, "my_proxy", 80, false);
	}
		


}	// SUITE

#endif	// __UNIT_TEST_PP


