//
// C++ Interface: informationpluginmockup
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NTEST_INFORMATIONPLUGINMOCKUP_H_20080912
#define __NTEST_INFORMATIONPLUGINMOCKUP_H_20080912

#include <QObject>

#include <informationplugin.h>
#include <shortinformationplugin.h>

#include <helpers.h>

namespace NPlugin {
	class IProvider;
}

namespace NTest {

using namespace NPlugin;

/**
	@author Benjamin Mesing <bensmail@gmx.net>
*/
class InformationPluginMockup : public QObject, public NPlugin::InformationPlugin, public NPlugin::ShortInformationPlugin
{
	int _shortInformationPriority;
	QString _shortInformationCaption;
public:
	InformationPluginMockup();

	~InformationPluginMockup();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider* /*pProvider*/) {};
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return "InformationPluginMockup"; }
	/** @returns &quot;PackageDescriptionPlugin&quot; */
	virtual QString title() const { return tr("InformationPluginMockup"); };
	/// @todo to be implemented
	virtual QString briefDescription() const { return ""; };
	/// @todo to be implemented
	virtual QString description() const { return ""; };
	//@}
	/** @name InformationPlugin interface
	  * 
	  * Implementation of the InformationPlugin interface
	  */
	//@{
	virtual uint informationPriority() const	{ return 0; }
	/** @returns a widget which shows a description of this package. */
	virtual QWidget* informationWidget() const { return 0; };
	/** @returns &quot;Description&quot; */
	virtual QString informationWidgetTitle() const { return "InformationPluginMockup"; };
	virtual void updateInformationWidget(const string& /*package*/) {};
	virtual void clearInformationWidget() {};
	/** This plugin offers an information text. */
	virtual bool offersInformationText() const { return false; };
	virtual QString informationText (const string& package) {	return toQString(package); };
	//@}
	
	/** @name ShortInformationPlugin interface
	  * 
	  * Implementation of the ShortInformationPlugin interface
	  */
	//@{
	virtual uint shortInformationPriority() const	{ return _shortInformationPriority; };
	/** This returns a short description about the package.
	  * @param packageID a handle of the package to show information for */
	virtual const QString shortInformationText(const string& package) { return toQString(package); };
	/** The caption for the short information is <b>Description</b>.  */
	virtual QString shortInformationCaption() const { return _shortInformationCaption; };
	// documented in base class
	virtual int preferredColumnWidth() const { return 10; }
	//@}
	
	void setShortInformationPriority(int priority) { _shortInformationPriority=priority; };

	void setShortInformationCaption ( const QString& theValue )
	{
		_shortInformationCaption = theValue;
	}
	

};

}

#endif
