//
// C++ Interface: actionpluginmockup
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef __NTEST_ACTIONPLUGINMOCKUP_H_2008_09_12
#define __NTEST_ACTIONPLUGINMOCKUP_H_2008_09_12

#include <actionplugin.h>

namespace NPlugin {
	class IProvider;
}

using namespace NPlugin;

namespace NTest {

/**
	@author Benjamin Mesing <bensmail@gmx.net>
*/
class ActionPluginMockup : public NPlugin::ActionPlugin
{

public:
	ActionPluginMockup();

	~ActionPluginMockup();
	/** @name Plugin Interface
	  * 
	  * Implementation of the PluginInterface
	  */
	//@{
	virtual void init(IProvider* /*pProvider*/) {};
	/// @todo not yet implemented
	virtual void setEnabled(bool)	{};
	/// @todo not yet implemented
	virtual void setVisible(bool)	{};
	virtual QString name() const { return "ActionPluginMockup"; }
	virtual QString title() const { return "Action Plugin Mockup"; };
	virtual QString briefDescription() const { return "Action Plugin Mockup for Testing"; };
	virtual QString description() const { return "Action Plugin Mockup for Testing"; };
	//@}
	/** @name ActionPlugin Interface
	  * 
	  * Implementation of the ActionPlugin Interface.
	  */
	//@{
	virtual vector<Action*> actions() const;
	//@}

};

}

#endif
