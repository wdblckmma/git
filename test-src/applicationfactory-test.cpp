//
// C++ Implementation: applicationfactory-test
//
// Description: 
//
//
// Author: Benjamin Mesing <bensmail@gmx.net>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef __UNIT_TEST_PP

#include <UnitTest++.h>


#include "applicationfactory.h"

#include "runcommand.h"

using namespace NApplication;


SUITE(ApplicationFactory_Test)
{
	
	// Equivalence classes:
	// GI1 getInstance() without it being called before
	// GI2 getInstance() after it was called before
	// SI1 setInstance()
	// GR getRunCommand
	
	TEST(getInstance)
	{
		// GI1
		ApplicationFactory* pFac1 = ApplicationFactory::getInstance();
		CHECK(pFac1 != 0);
		// GR
		pFac1->getRunCommand("test");	// everything should be fine here
		// GI2
		ApplicationFactory* pFac2 = ApplicationFactory::getInstance();
		CHECK_EQUAL(pFac1, pFac2);
	}

// 	TEST(setInstance)
// 	{
		// untested
// 	}
	
	TEST(getRunCommand)
	{
		ApplicationFactory* pFac1 = ApplicationFactory::getInstance();
		// GR
		RunCommand* pR = pFac1->getRunCommand("test");	// everything should be fine here
		CHECK(pR != 0);		
		// cleanup
		delete pR;
	}


}

#endif	// __UNIT_TEST_PP
